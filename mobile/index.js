import React from 'react';
import {AppRegistry, StatusBar, Text} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import { Provider as PaperProvider } from 'react-native-paper';
import { MyTheme } from '@assets/MyStyle';
import useActivityIndicator from "@hooks/useActivityIndicator";
import useCustomDialog from '@hooks/useCustomDialog';
import useDropdownAlert from '@hooks/useDropdownAlert';
import useWatchInternetConnection from '@hooks/useWatchInternetConnection';
import Constants from '@helpers/Constants';


console.disableYellowBox = true;


const AppTheme = () => {
    const { toggleActivityIndicator, renderActivityIndicator } = useActivityIndicator();    
    const { showCustomDialog, renderCustomDialog } = useCustomDialog();
    const { showDropdownAlert, renderDropdownAlert } = useDropdownAlert();
    const { renderConnectedToInternetInfo, isConnectedToInternet } = useWatchInternetConnection();
    
    renderStagingDisclaimer = () =>{
        if(Constants.API_SERVER_URL == Constants.STAGING_API_SERVER_URL){
            return(
                <Text>STAGING</Text>
            );
        }
    }

    return (
        <GlobalContext.Provider value={{ toggleActivityIndicator, renderActivityIndicator, showDropdownAlert, renderDropdownAlert, showCustomDialog, renderCustomDialog, renderConnectedToInternetInfo }}>
            <PaperProvider theme={MyTheme}>
                <StatusBar backgroundColor="#003300" barStyle="light-content"/>
                {renderStagingDisclaimer()}
                <App/>
            </PaperProvider>
            {/* { renderConnectedToInternetInfo() } */}
            { renderDropdownAlert() }
        </GlobalContext.Provider>
    );
};

AppRegistry.registerComponent(appName, () => AppTheme);
//import ScreenTest from '@screens/ScreenTest';
//AppRegistry.registerComponent(appName, () => ScreenTest);

