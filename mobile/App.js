import React from 'react';
import { View, Text, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import ScreenLoading from './src/screens/ScreenLoading';
import ScreenLanding from '@screens/ScreenLanding';
import ScreenLogin from '@screens/ScreenLogin';
import ScreenSignUp from '@screens/ScreenSignUp';
//me tab
import ScreenMe from '@screens/ScreenMe';
import ScreenJourney from '@screens/ScreenJourney';
import ScreenJourneyDetail from '@screens/ScreenJourneyDetail';
import ScreenNotification from '@screens/ScreenNotification';
import ScreenNotificationDetail from '@screens/ScreenNotificationDetail';
import ScreenSetting from '@screens/ScreenSetting';
import ScreenMeEditProfile from '@screens/ScreenMeEditProfile';
import ScreenMeChangePassword from '@screens/ScreenMeChangePassword';
import ScreenTutorial from '@screens/ScreenTutorial';
//community tab
import ScreenCommunity from '@screens/ScreenCommunity';
import ScreenCommunityDetail from '@screens/ScreenCommunityDetail';
import ScreenCommunityViewAll from '@screens/ScreenCommunityViewAll';
//explore tab
import ScreenExplore from '@screens/ScreenExplore';
import ScreenCameraPreview from '@screens/ScreenCameraPreview';
import ScreenQuiz from '@screens/ScreenQuiz';
import ScreenCamera from '@screens/ScreenCamera';
import ScreenCameraHiddenFruit from '@screens/ScreenCameraHiddenFruit';
import ScreenWikitude from '@screens/ScreenWikitude';
//info tab
import ScreenInfo from '@screens/ScreenInfo';
import ScreenInfoPoiList from '@screens/ScreenInfoPoiList';
import ScreenInfoPoiDetail from '@screens/ScreenInfoPoiDetail';
import ScreenInfoTenantDetail from '@screens/ScreenInfoTenantDetail';
//reward tab
import ScreenReward from '@screens/ScreenReward';
import ScreenRewardInfo from '@screens/ScreenRewardInfo';
import ScreenRewardDetail from '@screens/ScreenRewardDetail';
import ScreenEventInfo from '@screens/ScreenEventInfo';
import ScreenEvent from '@screens/ScreenEvent';

import { StyleConstant, ShareStyle } from '@assets/MyStyle';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//#############################################
const LandingStack = createStackNavigator({
  /*
    landing: {screen: ScreenLanding, navigationOptions: {
      header: null,
    }},
    */
    landing: ScreenLanding,
    login: ScreenLogin,
    signUp: ScreenSignUp
  }, {
    initialRouteName: 'landing',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: StyleConstant.primaryColorDark,
      },
      headerBackTitle: null,
      headerTintColor: StyleConstant.secondaryColorDark,
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      /**/
    },
  }
);
//#############################################

const MeStack = createStackNavigator({
  me: ScreenMe,
  journey: ScreenJourney,
  journeyDetail: ScreenJourneyDetail,
  notification: ScreenNotification,
  notificationDetail: ScreenNotificationDetail,
  setting: ScreenSetting,
  editProfile: ScreenMeEditProfile,
  changePw: ScreenMeChangePassword,
  myPhotoDetail: ScreenCommunityDetail,
}, { initialRouteName: 'me', defaultNavigationOptions:{headerBackTitle: null, headerTintColor: 'white', headerStyle: {backgroundColor: StyleConstant.primaryColor}} });


//#############################################

const InfoStack = createStackNavigator({
  info: ScreenInfo,
  poiList: ScreenInfoPoiList,
  tenantDetail: ScreenInfoTenantDetail,
  poiDetail: ScreenInfoPoiDetail,  
}, { initialRouteName: 'info', defaultNavigationOptions:{headerBackTitle: null, headerTintColor: 'white', headerStyle: {backgroundColor: StyleConstant.primaryColor}} });


//#############################################

const RewardStack = createStackNavigator({
  reward: ScreenReward,
  infoReward: ScreenRewardInfo,
  detailReward: ScreenRewardDetail
}, { initialRouteName: 'reward', defaultNavigationOptions:{headerBackTitle: null, headerTintColor: 'white', headerStyle: {backgroundColor: StyleConstant.primaryColor}} });

RewardStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  for (let i = 0; i < navigation.state.routes.length; i++) {
    if (navigation.state.routes[i].routeName == "infoReward" || navigation.state.routes[i].routeName == "detailReward") {
      tabBarVisible = false;
    }
  }
  return {
    tabBarVisible
  };
};

//#############################################

const CommunityStack = createStackNavigator({
  community: ScreenCommunity,
  communityDetail: ScreenCommunityDetail,
  communityViewAll: ScreenCommunityViewAll
}, { initialRouteName: 'community', defaultNavigationOptions:{headerBackTitle: null, headerTintColor: 'white', headerStyle: {backgroundColor: StyleConstant.primaryColor}} });

//#############################################

const ExploreStack = createStackNavigator({
  explore: ScreenExplore,
  camera: ScreenCamera,
  preview: ScreenCameraPreview,
  quiz: ScreenQuiz,
  tutorial: ScreenTutorial,
  exploreCommunityDetail: ScreenCommunityDetail,
  exploreCommunityViewAll: ScreenCommunityViewAll,
  hiddenFruit: ScreenCameraHiddenFruit,
  wikitude: ScreenWikitude,
  exploreTenantDetail: ScreenInfoTenantDetail,
  explorePoiDetail: ScreenInfoPoiDetail, 
  exploreEventInfo: ScreenEventInfo,
  exploreEvent: ScreenEvent,
}, { initialRouteName: 'explore', defaultNavigationOptions:{headerBackTitle: null, headerTintColor: 'white', headerStyle: {backgroundColor: StyleConstant.primaryColor}} });

ExploreStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  for (let i = 0; i < navigation.state.routes.length; i++) {
    if (navigation.state.routes[i].routeName == "wikitude" || navigation.state.routes[i].routeName == "camera" || navigation.state.routes[i].routeName == "quiz" || navigation.state.routes[i].routeName == "tutorial" || navigation.state.routes[i].routeName == "hiddenFruit" || navigation.state.routes[i].routeName == "exploreEventInfo" || navigation.state.routes[i].routeName == "exploreEvent") {
      tabBarVisible = false;
    }
  }
  return {
    tabBarVisible
  };
};
//#############################################

  
propsAccordingToIosModel = () => {
  if(Platform.OS == 'ios'){
    if( DeviceInfo.getDeviceId().includes('iPhone13,1') ){
      return {
        paddingBottom:35,
        height:46
      }
    } else if( DeviceInfo.getDeviceId().includes('iPhone13') ){
      return {
        paddingBottom:25,
        height:70
      }
    }
  }
  return {};
}

const MainBottomTab= createBottomTabNavigator({
  meStack: {
    screen: MeStack,
    navigationOptions:{
      tabBarLabel:'Me',
      tabBarOptions: { activeTintColor: StyleConstant.primaryColor, inactiveTintColor: StyleConstant.mutedTextColor },
      tabBarIcon: ({ focused, tintcolor }) =>(
        <View>
          <Icon name="account" size={24} color={ focused? StyleConstant.primaryColor : StyleConstant.mutedTextColor }/>
        </View>
      )
    }
  },
  community: {
    screen: CommunityStack,
    navigationOptions:{
      tabBarLabel:'Community',
      tabBarOptions: { activeTintColor: StyleConstant.primaryColor, inactiveTintColor: StyleConstant.mutedTextColor },
      tabBarIcon: ({ focused, tintcolor }) =>(
        <View>
          <Icon name="account-group" size={24} color={ focused? StyleConstant.primaryColor : StyleConstant.mutedTextColor }/>
        </View>
      )
    }
  },
  explore: {
    screen: ExploreStack,
    navigationOptions:{
      tabBarLabel:'Explore',
      tabBarOptions: { activeTintColor: StyleConstant.primaryColor, inactiveTintColor: StyleConstant.mutedTextColor },
      tabBarIcon: ({ focused, tintcolor }) =>(
        <Icon name="map-marker" size={24} color={ focused? StyleConstant.primaryColor : StyleConstant.mutedTextColor }/>
      ),
    }
  },
  info: {
    screen: InfoStack,
    navigationOptions:{
      tabBarLabel:'Info',
      tabBarOptions: { activeTintColor: StyleConstant.primaryColor, inactiveTintColor: StyleConstant.mutedTextColor },
      tabBarIcon: ({ focused, tintcolor }) =>(
        <View>
          <Icon name="information-variant" size={24} color={ focused? StyleConstant.primaryColor : StyleConstant.mutedTextColor }/>
        </View>
      )
    }
  },
  reward: {
    screen: RewardStack,
    navigationOptions:{
      tabBarLabel:'Reward',
      tabBarOptions: { activeTintColor: StyleConstant.primaryColor, inactiveTintColor: StyleConstant.mutedTextColor },
      tabBarIcon: ({ focused, tintcolor }) =>(
        <View>
          <Icon name="trophy" size={24} color={ focused? StyleConstant.primaryColor : StyleConstant.mutedTextColor }/>
        </View>
      )
    }
  },
}, { initialRouteName: 'explore', resetOnBlur:true, tabBarOptions: {showLabel: false, style: {height: 60, ...propsAccordingToIosModel()}} })


const RootSwitch = createSwitchNavigator({
  loading: ScreenLoading,
  landingStack: LandingStack,
  mainBottomTab: MainBottomTab,
}, { initialRouteName: 'loading' });


export default createAppContainer(RootSwitch);