//
//  CounterView.swift
//  C2C
//
//  Created by loy on 22/8/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

class ArchitectView: UIView, WTArchitectViewDelegate {
  
  @objc var path: NSString = "Assets/wikitude/station/index.html"
  @objc var path_resource: NSString = "index"
  @objc var path_ext: NSString = "html"
  @objc var path_dir: NSString = "Assets/wikitude/station"
  @objc var serial: NSString = "xxx"
  
  fileprivate var architectView:WTArchitectView?
  fileprivate var architectWorldNavigation:WTNavigation?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor.blue
    //self.addSubview(button)
    //createArView called from react useEffect since path & serial need to be passed over
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func createArView() {
    print("path: " + (path_dir as String) + " " + (path_resource as String) + " " + (path_ext as String))
    print("serial: " + (serial as String))
    architectView = WTArchitectView(frame: frame)
    architectView?.requiredFeatures = WTFeatures.imageTracking
    architectView?.delegate = self
    architectView?.setLicenseKey((serial as String))
    //architectView?.loadArchitectWorld(from: Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "Assets/wikitude/station")!)
    architectView?.loadArchitectWorld(from: Bundle.main.url(forResource: (path_resource as String), withExtension: (path_ext as String), subdirectory: (path_dir as String))!)
    
    self.addSubview(architectView!)
    startWikitudeSDKRendering()
  }

  
  
  override func layoutSubviews() {
    architectView?.frame = reactContentFrame
  }
  
  override func removeReactSubview(_ subview: UIView!) {
    stopWikitudeSDKRendering()
  }
  
  func startWikitudeSDKRendering(){
    if self.architectView?.isRunning == false{
      self.architectView?.start({ configuration in
        configuration.captureDevicePosition = AVCaptureDevice.Position.back
        configuration.captureDeviceResolution = WTCaptureDeviceResolution.AUTO
      }, completion: {isRunning, error in
        if !isRunning{
          print("WTArchitectView could not be started. Reason: \(error.localizedDescription)")
        }
      })
    }
  }
  
  func stopWikitudeSDKRendering(){
    if self.architectView?.isRunning == true{
      self.architectView?.stop()
    }
    
  }
  func architectView(_ architectView: WTArchitectView, invokedURL URL: Foundation.URL) {
  }
  
  func architectView(_ architectView: WTArchitectView, didFinishLoadArchitectWorldNavigation navigation: WTNavigation) {
    //    /* Architect World did finish loading */
  }
  func architectView(_ architectView: WTArchitectView, didFailToLoadArchitectWorldNavigation navigation: WTNavigation, withError error: Error) {
    print("Architect World from URL \(navigation.originalURL) could not be loaded. Reason: \(error.localizedDescription)");
  }
  func architectView(_ architectView: WTArchitectView!, didEncounterInternalError error: NSError!) {
    print("WTArchitectView encountered an internal error \(error.localizedDescription)");
  }
  
  @objc var onArEvent: RCTDirectEventBlock?
  
  func architectView(_ architectView: WTArchitectView, receivedJSONObject jsonObject: [AnyHashable : Any]) {
    print(jsonObject)
    if let dict = jsonObject as? Dictionary<String, String> {
      let action = String(dict["action"]!)
      let param = String(dict["param"]!)
      print("action: " + action)
      print("param: " + param)
      
      if onArEvent != nil {
        onArEvent!(["action":action, "param":param])
      }
    }
  }
  
  
  @objc func update(value: NSNumber) {
    print("update from manager")
    /*
     architectView?.frame = CGRect(x: 0,y: 0,width: 1500,height: 1500)
     architectView?.setNeedsLayout()
     architectView?.layoutIfNeeded()
     */
  }

  @objc func initFromReact(path_resource: NSString, path_ext: NSString, path_dir: NSString, serial: NSString) {
    self.path_resource = path_resource
    self.path_ext = path_ext
    self.path_dir = path_dir
    self.serial = serial
    createArView()
  }
  @objc func stopWikitude() {
    stopWikitudeSDKRendering()
  }
  
  
  /*
   @objc var count: NSNumber = 0 {
   didSet {
   button.setTitle(String(describing: count), for: .normal)
   }
   
   } lazy var button: UIButton = {
 let b = UIButton.init(type: UIButton.ButtonType.system)
 b.titleLabel?.font = UIFont.systemFont(ofSize: 50)
 b.autoresizingMask = [.flexibleWidth, .flexibleHeight]
 b.addTarget(
 self,
 action: #selector(increment),
 for: .touchUpInside
 )
 return b
 }()
 @objc func increment() {
 count = count.intValue + 1 as NSNumber
 }
 */

}
