//
//  ArchitectViewManager.swift
//  mobile
//
//  Created by Eddie Lim on 5/9/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

//import Foundation

// ArchitectViewManager.swift
@objc(HiddenLocationViewManager)
class HiddenLocationViewManager: RCTViewManager {
  
  override func view() -> UIView! {
    /*
    let label = UILabel()
    label.text = "Swift Counter"
    label.textAlignment = .center
    return label
    */
    return HiddenLocationView()
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
/*
  @objc func updateFromManager(_ node: NSNumber, count: NSNumber) {
    
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
        ) as! ArchitectView
      component.update(value: count)
    }
  }
 */
  @objc func initFromReact(_ node: NSNumber, imgUrl: NSString, rare_flag: NSString) {
    
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
        ) as! HiddenLocationView
      component.initFromReact(imgUrl: imgUrl, rare_flag: rare_flag)
    }
  }
}
