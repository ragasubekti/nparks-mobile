//
//  ArchitectViewManager.swift
//  mobile
//
//  Created by Eddie Lim on 5/9/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

//import Foundation

// ArchitectViewManager.swift
@objc(ArchitectViewManager)

class ArchitectViewManager: RCTViewManager {
  
  override func view() -> UIView! {
    /*
    let label = UILabel()
    label.text = "Swift Counter"
    label.textAlignment = .center
    return label
    */
    return ArchitectView()
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }

  @objc func updateFromManager(_ node: NSNumber, count: NSNumber) {
    
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
        ) as! ArchitectView
      component.update(value: count)
    }
  }
  
  @objc func initFromReact(_ node: NSNumber, path_resource: NSString, path_ext: NSString, path_dir: NSString, serial: NSString) {
    
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
        ) as! ArchitectView
      component.initFromReact(path_resource: path_resource, path_ext: path_ext, path_dir: path_dir, serial: serial)
    }
  }
  @objc func stopWikitude(_ node: NSNumber) {
    
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
        ) as! ArchitectView
      component.stopWikitude()
    }
  }
}
