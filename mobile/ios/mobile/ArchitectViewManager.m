//
//  ArchitectViewManager.m
//  mobile
//
//  Created by loy on 22/8/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTViewManager.h"

@interface RCT_EXTERN_MODULE(ArchitectViewManager, RCTViewManager)

/*
RCT_EXPORT_VIEW_PROPERTY(count, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(path, NSString)
RCT_EXPORT_VIEW_PROPERTY(serial, NSString)

RCT_EXTERN_METHOD(
  updateFromManager:(nonnull NSNumber *)node
  count:(nonnull NSNumber *)count
)
*/

RCT_EXPORT_VIEW_PROPERTY(onArEvent, RCTDirectEventBlock)
RCT_EXTERN_METHOD(
  initFromReact:(nonnull NSNumber *)node
  path_resource:(nonnull NSString *)path_resource
  path_ext:(nonnull NSString *)path_ext
  path_dir:(nonnull NSString *)path_dir
  serial:(nonnull NSString *)serial
)
RCT_EXTERN_METHOD(
  stopWikitude:(nonnull NSNumber *)node
)

@end
