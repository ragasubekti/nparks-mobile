//
//  ArchitectViewManager.m
//  mobile
//
//  Created by loy on 22/8/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTViewManager.h"

@interface RCT_EXTERN_MODULE(HiddenLocationViewManager, RCTViewManager)

/*
RCT_EXPORT_VIEW_PROPERTY(count, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(path, NSString)
RCT_EXPORT_VIEW_PROPERTY(serial, NSString)

RCT_EXTERN_METHOD(
  updateFromManager:(nonnull NSNumber *)node
  count:(nonnull NSNumber *)count
)
*/

RCT_EXPORT_VIEW_PROPERTY(onTouchHiddenFruit, RCTDirectEventBlock)
RCT_EXTERN_METHOD(
    initFromReact:(nonnull NSNumber *)node
    imgUrl:(nonnull NSString *)imgUrl
    rare_flag:(nonnull NSString *)rare_flag
)

@end
