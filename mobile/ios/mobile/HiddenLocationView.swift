//
//  CounterView.swift
//  C2C
//
//  Created by loy on 22/8/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation
import AVFoundation

class HiddenLocationView: UIView, WTArchitectViewDelegate {
  
  @objc var imgUrl: NSString = "testImg"
  @objc var rare_flag: NSString = "true"
  
  fileprivate var architectView:WTArchitectView?
  fileprivate var architectWorldNavigation:WTNavigation?
  
  fileprivate var button:UIButton?
  fileprivate var button2:UIButton?
  fileprivate var timer:Timer?
  fileprivate var onTouchFruit:UITapGestureRecognizer?

  fileprivate var motionManager:CMMotionManager?
  fileprivate var locationManager:CLLocationManager?
  fileprivate var captureSession:AVCaptureSession?
  fileprivate var videoPreviewLayer:AVCaptureVideoPreviewLayer?
  
  @objc var xpos: double_t = 0.0
  @objc var ypos: double_t = 0.0
  @objc var xRandomAngle: double_t = 0.0
  
  @objc var hasTriggered: NSString = "false"


  override init(frame: CGRect) {
    super.init(frame: frame)
    //setup camera
    captureSession = AVCaptureSession()
    captureSession?.sessionPreset = .medium
    
    do { if #available(iOS 10.2, *) {
      let input = try AVCaptureDeviceInput(device: AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)!)
      captureSession?.addInput(input)
    } else {
      // Fallback on earlier versions
      } }
    catch let err{
      print("Error")
    }
    
    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
    videoPreviewLayer?.videoGravity = .resizeAspectFill
    videoPreviewLayer?.connection?.videoOrientation = .portrait
    self.layer.addSublayer(videoPreviewLayer!)
    
    DispatchQueue.global(qos: .userInitiated).async {
      self.captureSession?.startRunning()
      DispatchQueue.main.async {
        self.videoPreviewLayer?.frame = self.bounds
      }
    }
    
    //setup camera
    self.backgroundColor = UIColor.clear
    //if rare fruit 1.5x
    button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
    button?.backgroundColor = .clear
    button?.contentMode = UIView.ContentMode.scaleAspectFit
    
    self.addSubview(button!)
    //createArView called from react useEffect since path & serial need to be passed over
    motionManager = CMMotionManager()
    motionManager?.startAccelerometerUpdates()
    
    locationManager = CLLocationManager()
    locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    locationManager?.requestAlwaysAuthorization()
    locationManager?.headingFilter = kCLHeadingFilterNone
    locationManager?.headingOrientation = CLDeviceOrientation.portrait
    locationManager?.startUpdatingLocation()
    locationManager?.startUpdatingHeading()
    
    Timer.scheduledTimer(timeInterval: 0.016, target: self,   selector: (#selector(self.loopTimer)), userInfo: nil, repeats: true)
    
    let array = [40.0, 70.0, 120.0, 250.0, 300.0]
    let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
    xRandomAngle = array[randomIndex]
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  @objc var onTouchHiddenFruit: RCTDirectEventBlock?
 
  @objc func buttonClicked() {
      if onTouchHiddenFruit != nil {
          if(hasTriggered == "false"){
            self.hasTriggered = "true"
            onTouchHiddenFruit!(["status":"found"])
          }
        }
  }
  
  @objc func loopTimer(){
    self.setNeedsDisplay()
  }
  
  override func draw(_ rect: CGRect) {
    let a:String = String(format:"%f", motionManager?.accelerometerData?.acceleration.z ?? 0.0)
    button2?.setTitle(a, for: .normal)
    self.setYPos();
    self.setXPos();
    if(rare_flag == "true"){
      button?.frame = CGRect(x: xpos, y: ypos, width: 237, height: 300)
    } else {
      button?.frame = CGRect(x: xpos, y: ypos, width: 158, height: 200)
    }
    
    button?.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
  }
  
  func setXPos(){
        var heading = locationManager?.heading?.trueHeading
        //calculate xpos
        var xtargetAngle = xRandomAngle
        var xviewAngle = 60.00;
            var h = heading;
        var xpercent = 0.00;
        var xrangeLower = 0.00;

        //Note:: bitmap disappear in > 340 range
        //target angle range falls lesser than 360
        if (xtargetAngle < xviewAngle) {
          if (double_t(heading ?? 0.0) > 360.00 - xviewAngle / 2) {
            h = (heading ?? 0.0) - 360;
            xpercent = (double_t(h ?? 0.0) + xviewAngle / 2) / xviewAngle;
        } else {
            xpercent = (double_t(h ?? 0.0) + xviewAngle / 2) / xviewAngle;
        }
        } else {
            xrangeLower = xtargetAngle - xviewAngle / 2;
            xpercent = (double_t(heading ?? 0.0) - xrangeLower) / xviewAngle;
        }
        var x = CGFloat(1 - xpercent) * self.bounds.size.width;
        var v = lowpass(input: x, output: CGFloat(xpos))
        xpos = double_t(v)
  }
  
  func setYPos(){
    var yappearAngle = 0.1;
    var yviewAngle = 0.4;
    var tilt = (motionManager?.accelerometerData?.acceleration.z ?? 0.0) * -10
    var tiltPercent = self.prepareTilt(value: tilt)
    var ypercent = (tiltPercent - yappearAngle) / yviewAngle;
    var y = CGFloat(ypercent) * self.bounds.size.height;
    var v = lowpass(input: y, output: CGFloat(ypos))
    ypos = double_t(v)
  }
  
  func prepareTilt(value: double_t) -> double_t{
    var v = 0.0
    if (value < 0) { v = 10 - value;}
    else { v = -value + 10;}
    return v / 20;
  }
  
  func lowpass(input: CGFloat, output: CGFloat) -> CGFloat{
    var alpha = 0.05;
    if (output == 0.0){ return input; }
    else{
      var o = output + CGFloat(alpha) * (input - output);
      return o;
    }
  }

  @objc func initFromReact(imgUrl: NSString, rare_flag: NSString) {
    self.imgUrl = imgUrl
    self.rare_flag = rare_flag
    
    let url = URL(string: imgUrl as String)
    do{
      let urlData = try Data(contentsOf: url!)
      let image = UIImage(data: urlData)
      button?.setImage(image!, for: .normal)
    } catch let err{
      print("Error")
    }
  }

}
