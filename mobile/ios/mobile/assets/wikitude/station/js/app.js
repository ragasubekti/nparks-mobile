var model;
var actionAnimation;
var idleAnimation;
var talkAnimation;
var flag_hasTarget = false;


var World = {
    loaded: false,

    init: function initFn() {
        this.createOverlays();
    },

    createOverlays: function () {
        var targetCollectionResource = new AR.TargetCollectionResource("assets/tracker.wtc");

        var tracker = new AR.ImageTracker(targetCollectionResource, {
            maximumNumberOfConcurrentlyTrackableTargets: 1,
            extendedRangeRecognition: AR.CONST.IMAGE_RECOGNITION_RANGE_EXTENSION.OFF,
            onTargetsLoaded: this.worldLoaded,
            onError: function (errorMessage) {
                alert(errorMessage);
            }
        });


        new AR.ImageTrackable(tracker, "*", {
            onImageRecognized: function (target) {
                flag_hasTarget = true;

                World.showLoading();
                var url = "https://api.coast-to-coast.nparks.gov.sg/model/" + target.name + ".wt3";
                console.log("loading: " + url);

                model = new AR.Model(url, {
                    onInitialized: World.showLoading(),
                    //onLoaded: World.showTapInstructions(),
                    //onInitialized: alert("init"),
                    //onLoaded: alert("loaded"),
                    scale: 0.03,
                    rotate: {
                        x: -30,
                        //y: 30
                    }
                });

                idleAnimation = new AR.ModelAnimation(model, "idle");
                idleAnimation.onStart = World.showTapInstructions();
                idleAnimation.onFinish = idleAnimation.start;         

                talkAnimation = new AR.ModelAnimation(model, "talk");
                talkAnimation.onFinish = function () {
                    idleAnimation.start();
                }
                actionAnimation = new AR.ModelAnimation(model, "action");
                actionAnimation.onFinish = function () {
                    idleAnimation.start();
                }

                idleAnimation.start();
                AR.platform.sendJSONObject({ action: "scan_station", param: target.name});
                
                model.onClick = function () {
                    AR.platform.sendJSONObject({ action: "touch_model", param: target.name});
					World.removeLoadingBar();
                    doActionAnimation();
                }
                /**/
                
                this.addImageTargetCamDrawables(target, model);
                
            },
            onImageLost: function () {
                flag_hasTarget = false;
				World.showScanInstructions();
            },
            onError: function (errorMessage) {
                //alert(errorMessage);
            }
        });
    },
    showScanInstructions: function () {
        if (!World.loaded) {
            var e = document.getElementById('loadingMessage');
            e.innerHTML = "Scan the animal for a surprise!";
        }
    },
    showLoading: function () {
        if (!World.loaded) {
            var e = document.getElementById('loadingMessage');
            e.innerHTML = "Loading ...";
        }
    },	
    showTapInstructions: function () {
        if (!World.loaded) {
            var e = document.getElementById('loadingMessage');
            e.innerHTML = "Tap on the animal to learn more!";
        }
    },    
    removeLoadingBar: function () {
        if (!World.loaded) {
            var e = document.getElementById('loadingMessage');
            e.parentElement.removeChild(e);
            World.loaded = true;
        }
    },
    worldLoaded: function worldLoadedFn() {
        World.showScanInstructions();
    }
};

World.init();

/*
AR.context.onScreenClick = function () {
    idleAnimation.stop();
    talkAnimation.stop();
    actionAnimation.start();
};
*/


function doActionAnimation() {
    if (flag_hasTarget) {
        idleAnimation.pause();
        talkAnimation.pause();
        actionAnimation.pause()
        actionAnimation.start();
    } else {
        //alert("No target!");
    }
}
function doIdleAnimation() {
    if (flag_hasTarget) {
        idleAnimation.pause();
        talkAnimation.pause();
        actionAnimation.pause()
        IdleAnimation.start();
    } else {
        //alert("No target!");
    }
}

function doTalkAnimation() {
    if (flag_hasTarget) {
        idleAnimation.pause();
        talkAnimation.pause();
        actionAnimation.pause();
        talkAnimation.start();
    } else {
        //alert("No target!");
    }
}