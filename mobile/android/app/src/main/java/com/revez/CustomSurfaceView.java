package com.revez;

import android.hardware.Camera;
import android.util.Log;
import android.view.Gravity;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.View;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.content.Context;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;
import android.content.*;
import android.graphics.*;
import java.io.InputStream;
import java.net.URL;
import android.R.drawable;
import java.net.HttpURLConnection;
import java.io.IOException;
import android.os.StrictMode;
import com.facebook.react.bridge.ReactContext;
import java.util.Random;



public class CustomSurfaceView extends SurfaceView implements SensorEventListener, SurfaceHolder.Callback{

    protected Context _context;

    private Camera mCamera;
    private CameraPreview mPreview;

    private SensorManager sensorManager;
    private float[] mValuesAccel       = new float[3];
    private float[] mValuesOrientation = new float[3];
    private float[] mRotationMatrix    = new float[9];
    private float xRandomAngle;
    private String imgUrl;
    private String[] msg = new String[]{"a","a"};
    private float xpos= (float) 0.0;
    private float ypos = (float)0.0;
    private ImageView imageView;
    private Bitmap bmp;
    private SurfaceHolder holder;
    private int x = 0;
    private TextView textView;
    private CustomCanvas customCanvas;

    private boolean hasTriggered = false;

    public CustomSurfaceView(Context context, String imgUrl, String rare_flag) {
        super(context);
        _context = context;

        sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url = new URL(imgUrl);
            bmp = (BitmapFactory.decodeStream((InputStream) url.getContent()));
            if(rare_flag.equals("true")){
                //rare
                bmp = Bitmap.createScaledBitmap(bmp, 592, 750, false);
            } else {
                //normal
                bmp = Bitmap.createScaledBitmap(bmp, 395, 500, false);
            }
        } catch (IOException e) {
            Log.e("123", e.getMessage());
        }
        holder = getHolder();
        holder.setFormat(PixelFormat.TRANSPARENT);
        this.setWillNotDraw(false);

        holder.addCallback(this);
        customCanvas = new CustomCanvas(context);
        customCanvas.setDefaultValues(xpos, ypos, bmp);

        float[] floatArray = new float[]{(float)40.0, (float)70.0, (float)120.0, (float)250.0, (float)300.0};
        int rnd = new Random().nextInt(floatArray.length);
        xRandomAngle = floatArray[rnd];
        this.invalidate();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) { }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas canvas = holder.lockCanvas();
        if (canvas != null) {
            this.draw(canvas);
            holder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(Color.TRANSPARENT);
        customCanvas.setNewXposYpos(xpos,ypos);
        customCanvas.draw(canvas);
    }

    public void initFromReact(String imgUrl){
        this.imgUrl = imgUrl;
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            //Log.d(e);
            return null;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        float x = event.getX();  // or getRawX();
        float y = event.getY();

        switch(action){
            case MotionEvent.ACTION_DOWN:
                if(!hasTriggered){
                    if (x >=  xpos&& x < (xpos+ bmp.getWidth())
                            && y >= ypos && y < (ypos+ bmp.getHeight())) {
                                hasTriggered = true;
                                MyHiddenLocationView.sendEvent((ReactContext) _context);
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            getAccelerometer(event);
        }
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            getHeading(event);
        }
        this.postInvalidate();
        this.invalidate();
    }

    private void getHeading(SensorEvent event){
        SensorManager.getRotationMatrixFromVector(mRotationMatrix , event.values);
        SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);
        float heading = (float)Math.toDegrees(mValuesOrientation[0]) + (float)180.0;
        float xtargetAngle = xRandomAngle;
        float xviewAngle = (float)60.0;
        float h = heading;
        float xpercent = (float)0.0;
        float xrangeLower = (float)0.0;

        //Note:: bitmap disappear in > 340 range
        //target angle range falls lesser than 360
        if (xtargetAngle < xviewAngle) {
            if (heading > (float)360.0 - xviewAngle / 2) {
                h = heading - (float)360.0;
                xpercent = (h + xviewAngle / 2) / xviewAngle;
            } else {
                xpercent = (h + xviewAngle / 2) / xviewAngle;
            }
        } else {
            xrangeLower = xtargetAngle - (xviewAngle / 2);
            xpercent = (heading - xrangeLower) / xviewAngle;
        }
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        float x = ((float)1.0 - xpercent) * (float)width;
        float v = lowpass(x, xpos);
        xpos = v;
    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float yappearAngle = (float)0.1;
        float yviewAngle = (float)0.4;
        float tiltPercent = prepareTilt(z);
        float ypercent = (tiltPercent - yappearAngle) / yviewAngle;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        float yValue = ypercent * (float)height;
        float v = lowpass(yValue, ypos);
        ypos = v;
    }

    private float prepareTilt(float input){
        float v = (float)0;
        if(input < 0) { v = (float)10 - input;}
        else { v = -input+ (float)10;}
        return v / (float)20;
    }

    private float lowpass(float input, float output){
        float alpha = (float)0.05;
        if (output == 0.0){ return input; }
        else{
            float o = output + alpha * (input - output);
            return o;
        }
    }


}