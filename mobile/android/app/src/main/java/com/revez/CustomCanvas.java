package com.revez;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class CustomCanvas extends View {
    private float xpos= (float) 0.0;
    private float ypos = (float)0.0;
    private Bitmap bmp;

    public CustomCanvas(Context context){
        super(context);
    }

    public void setDefaultValues(float xpos, float ypos, Bitmap bmp){
        this.xpos = xpos;
        this.ypos = ypos;
        this.bmp = bmp;
    }

    public void setNewXposYpos(float xpos, float ypos){
        this.xpos = xpos;
        this.ypos = ypos;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(Color.TRANSPARENT);
        Paint paint = new Paint();
        if(xpos != 0 || ypos != 0){
            canvas.drawBitmap(bmp, xpos, ypos, paint);
        }
    }
}
