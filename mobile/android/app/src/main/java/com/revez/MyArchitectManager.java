package com.revez;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import java.util.Map;

public class MyArchitectManager extends SimpleViewManager<MyArchitectView> {
  protected ReactApplicationContext _reactAppContext;

  private static final int COMMAND_INIT = 1;

  public MyArchitectManager(ReactApplicationContext reactContext) {
    _reactAppContext = reactContext;
  }


  public static final String REACT_CLASS = "ArchitectView";
  private MyArchitectView myArchitectView;

  @Override
  public String getName() {
    return REACT_CLASS;
  }

  @Override
  public MyArchitectView createViewInstance(ThemedReactContext context) {
      myArchitectView = new MyArchitectView(context, _reactAppContext);
      return myArchitectView;
  }

  @Override
  public Map<String,Integer> getCommandsMap() {
      return MapBuilder.of("initFromReact", COMMAND_INIT);
  }

  @Override
  public void receiveCommand(final MyArchitectView view, int command, final ReadableArray args) {
  //String token = "rJhF2spa8nDvyfTKMzkGZLoE9SZoIxYfv1iHENynQVF3wcQ9wr+y/bXH2N2lFujzCkjtdhALGmG+CEeqbS/fVxAWFM1jOC120P3ad/qeElltQvKaG2r/7Oq5Mcu6K+139kDFhSmB6UVtRXRdFoYuKNU2TDpXkFSFd8FpD9W4N/1TYWx0ZWRfX24gnBnNIitXAzCS4mpY1QsEevZhktr9MvBPCyPeaAmAQX/0I3WCGkIq+4uvkBwuNa+c82F8Xjkm4Mnd+FQPzDMwaVvT63UFHEZp/AdOMRxfa8pTtAlwSwUsxx7l9xKf5QIjJIL9Ao3zFobHdyEa53+K83WhWtMo3tiDadpW+A7abs96Vp1Tc6dZvlTwJgoQ6tOK7bVei1fj1CzxZdbwR5EQhCWh08GGc5CdDdp8OQIp6wA6bxPc0HiviezbinrMT2jDSLN4WhG4iyJRDXR46yVJ61rwPiniTmrgQzgpIr6vz03Jdnhbo3wxuJHJ0sn2D/ADDv9PaxPhQ5DOkf6Pw9B9anEvMaSuysJGkSukZgIciffI5mYCV5wvVv/IwXPrh99yCBU4cR2aR/WJ1k3I18Owv1t+WYbQV7Pea5pwR1mem82XISkujFHp46GLMb5joTO92W46gOjNKBfc6a6x3pj3Zb5PvEHJ6S8bzh7/f1nYn44prGN8uXWZFFFbNzwlXNA8lgANmIEdqhfAv5jZztgk97d1hjDvbhqRG7S3erBz5A2BPfVwGt0g5D/ArjG0HSffhCA5ekGE";
    Log.d("WikitudeSerial", args.getString(0));
    switch (command){
      case COMMAND_INIT:{
        view.initFromReact(args.getString(0), args.getString(1));
        break;
      }
      default: {
        break;
      }
    }
  }

}