package com.revez;

import android.app.AlertDialog;
import android.view.MenuItem;
import android.widget.Toast;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.widget.TextView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.ArchitectJavaScriptInterfaceListener;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.common.camera.CameraSettings;
import com.wikitude.common.permission.PermissionManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.wikitude.common.util.SDKBuildInformation;

import org.json.JSONObject;

import java.io.IOException;


public class MyArchitectView extends LinearLayout implements ArchitectView.ArchitectWorldLoadedListener, LifecycleEventListener, ArchitectJavaScriptInterfaceListener {

    public static final int CULLING_DISTANCE_DEFAULT_METERS = 50 * 1000;
    protected ArchitectView architectView;
    // protected String architectWorldURL = "file:///android_asset/wikitude/station/index.html";

    protected ArchitectJavaScriptInterfaceListener mArchitectJavaScriptInterfaceListener;
    protected ArchitectView.ArchitectWorldLoadedListener worldLoadedListener;
    protected Context _context;
    protected ReactApplicationContext _reactAppContext;
    private String serial;
    private String architectWorldURL;

    private int getFeatures() {
        /*
        int features = (this.hasGeolocation ? ArchitectStartupConfiguration.Features.Geo : 0) |
                (this.hasImageRecognition ? ArchitectStartupConfiguration.Features.ImageTracking : 0) |
                (this.hasInstantTracking ? ArchitectStartupConfiguration.Features.InstantTracking : 0) ;
                */

        int features = 0 | 1 | 0;
        return features;
    }

    public void initFromReact(String serial, String architectWorldURL){
        this.serial = serial;
        this.architectWorldURL = architectWorldURL;
        // Toast.makeText(_context, this.serial, Toast.LENGTH_SHORT).show();
        createArView();
    }

    public void createArView(){
        // Toast.makeText(_context, "createArView", Toast.LENGTH_SHORT).show();
        final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration();
        config.setLicenseKey(this.serial);
        config.setFeatures(this.getFeatures());
        config.setCameraPosition(CameraSettings.CameraPosition.DEFAULT);
        config.setCameraResolution(CameraSettings.CameraResolution.AUTO);
        config.setCamera2Enabled(false);
        ThemedReactContext c = (ThemedReactContext) _context;

        _reactAppContext.addLifecycleEventListener(this);

        architectView = new ArchitectView(c.getCurrentActivity());
        architectView.onCreate(config);
        architectView.onPostCreate();
        this.addView(architectView);
        architectView.registerWorldLoadedListener(this);
        architectView.addArchitectJavaScriptInterfaceListener(this);

        try {
            architectView.load(architectWorldURL);
            architectView.setCullingDistance(CULLING_DISTANCE_DEFAULT_METERS);
        } catch (IOException e) {
            Toast.makeText(_context, "error", Toast.LENGTH_SHORT).show();
        }
    }



    public MyArchitectView(Context context, ReactApplicationContext reactAppContext) {
        super(context);
        _context = context;
        _reactAppContext = reactAppContext;
        /*
        this.setId(12345);
        this.setBackgroundColor(Color.BLUE);
        TextView textView = new TextView(context);
        textView.setTextColor(Color.WHITE);
        textView.setText("ccb2");
        this.addView(textView);
        */
    }

    @Override
    public void worldWasLoaded(String url) {
        //Toast.makeText(_context, "yea", Toast.LENGTH_SHORT).show();
        // showSdkBuildInformation();
    }

    @Override
    public void worldLoadFailed(int errorCode, String description, String failingUrl) {
        Toast.makeText(_context, "worldLoadFailed: " + description, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onJSONObjectReceived(JSONObject jsonObject) {
        //need to run on uithread
        Handler mainHandler = new Handler(_context.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    String a = jsonObject.getString("action");
                    String p = jsonObject.getString("param");

                    //Toast.makeText(_context, "action:" + a, Toast.LENGTH_SHORT).show();

                    WritableMap params = Arguments.createMap();
                    params.putString("action", a);
                    params.putString("param", p);
                    sendEvent((ReactContext) _context, "wikitudeEvent", params);

                } catch (Exception e) {
                    Toast.makeText(_context, "error onJSONObjectReceived", Toast.LENGTH_SHORT).show();
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    @Override
    public void onHostResume() {
       if ( this.architectView != null ) {
            this.architectView.onResume();
       }
    }

    @Override
    public void onHostPause() {
        if ( this.architectView != null ) {
            this.architectView.onPause();
        }
    }

    @Override
    public void onHostDestroy() {
        if ( this.architectView != null ) {
            this.architectView.clearCache();
            this.architectView.onDestroy();
        }
    }


    public void showSdkBuildInformation() {
        final SDKBuildInformation sdkBuildInformation = ArchitectView.getSDKBuildInformation();
        new AlertDialog.Builder(_context)
                .setTitle("build_information_title")
                .setMessage(
                        "build_information_config" + sdkBuildInformation.getBuildConfiguration() + "\n" +
                        "build_information_date" + sdkBuildInformation.getBuildDate() + "\n" +
                        "build_information_number" + sdkBuildInformation.getBuildNumber() + "\n" +
                        "build_information_version" + ArchitectView.getSDKVersion()
                )
                .show();
    }

}