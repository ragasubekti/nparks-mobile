package com.revez;

import android.util.Log;

import com.facebook.react.common.MapBuilder;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import java.util.Map;

public class MyHiddenLocationManager extends SimpleViewManager<MyHiddenLocationView> {
  protected ReactApplicationContext _reactAppContext;

  private static final int COMMAND_INIT = 1;

  public MyHiddenLocationManager(ReactApplicationContext reactContext) {
    _reactAppContext = reactContext;
  }


  public static final String REACT_CLASS = "HiddenLocationView";
  private MyHiddenLocationView myHiddenLocationView;

  @Override
  public String getName() {
    return REACT_CLASS;
  }

  @Override
  public MyHiddenLocationView createViewInstance(ThemedReactContext context) {
      myHiddenLocationView = new MyHiddenLocationView(context, _reactAppContext);
      return myHiddenLocationView;
  }

  @Override
  public Map<String,Integer> getCommandsMap() {
      return MapBuilder.of("initFromReact", COMMAND_INIT);
  }

  @Override
  public void receiveCommand(final MyHiddenLocationView view, int command, final ReadableArray args) {
    switch (command){
      case COMMAND_INIT:{
        view.initFromReact(args.getString(0), args.getString(1));
        break;
      }
      default: {
        break;
      }
    }
  }

}