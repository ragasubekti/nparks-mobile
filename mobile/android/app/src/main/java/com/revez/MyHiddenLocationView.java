package com.revez;

import android.hardware.Camera;
import android.util.Log;
import android.view.Gravity;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.View;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.content.Context;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.LifecycleEventListener;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.squareup.picasso.Picasso;
import com.swmansion.gesturehandler.OnTouchEventListener;

public class MyHiddenLocationView extends LinearLayout implements LifecycleEventListener, SensorEventListener {

    protected Context _context;
    protected ReactApplicationContext _reactAppContext;

    private Camera mCamera;
    private CameraPreview mPreview;

    private SensorManager sensorManager;
    private float[] mValuesAccel       = new float[3];
    private float[] mValuesOrientation = new float[3];
    private float[] mRotationMatrix    = new float[9];
    private String imgUrl;
    private String[] msg = new String[]{"a","a"};
    private float xpos= (float) 0.0;
    private float ypos = (float)0.0;
    private ImageView imageView;
    private String rare_flag;

    private CustomSurfaceView hiddenLocationView;

    private TextView textView;

    public void initFromReact(String imgUrl, String rare_flag){
        this.imgUrl = imgUrl;
        this.rare_flag = rare_flag;
        createArView();
    }

    public void createArView(){
        setupLayouts();
    }

    private void setupLayouts(){
        //create hiddenLocationView
        hiddenLocationView = new CustomSurfaceView(_context,this.imgUrl, this.rare_flag);

        //create camera
        mCamera =  Camera.open();
        mCamera.setDisplayOrientation(90);
        mPreview = new CameraPreview(_context, mCamera);

        //FrameLayout
        FrameLayout frameLayout = new FrameLayout(_context);
        frameLayout.setMinimumHeight(200);
        //added the camera to frameLayout
        frameLayout.addView(mPreview);
        //added the hiddenLocationView to frameLayout
        frameLayout.addView(hiddenLocationView);

        //added the frameLayout to this
        this.addView(frameLayout);
    }

    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public MyHiddenLocationView(Context context, ReactApplicationContext reactAppContext) {
        super(context);
        _context = context;
        _reactAppContext = reactAppContext;
    }

    public static void sendEvent(ReactContext reactContext) {
        WritableMap params = Arguments.createMap();
        params.putString("status", "found");
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("hiddenLocationEvent", params);
    }
    @Override
    public void onHostResume() {
        if(mCamera == null) {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            mPreview.refreshCamera(mCamera);
            Log.d("nu", "null");
        }else {
            Log.d("nu","no null");
        }
    }

    @Override
    public void onHostPause() {
        if ( mCamera != null ) {
            releaseCamera();
        }
    }

    @Override
    public void onHostDestroy() {
        if ( mCamera != null ) {
            releaseCamera();
        }
        if( sensorManager != null ) {
            sensorManager.unregisterListener(this);
        }
    }
}