import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Button } from 'react-native-elements';
import Constants from '@helpers/Constants';

export default ModelDialog = ({type}) => {
    if(type == Constants.DIALOG_POPUP_MESSAGE){
        return (
            <View style={styles.content}>
                <Image style={{width:100, height: 100}} source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}/>
                <Text style={styles.txt}>{type}</Text>
                <Button 
                    title="ok"
                    onPress={handleOk}
                />
            </View>
        );
    } else {
        return (
            <View style={styles.content}>
                <Image style={{width:100, height: 100}} source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}/>
                <Text style={styles.txt}>{type}</Text>
                <Button 
                    title="not ok"
                    onPress={handleOk}
                />
            </View>
        );
    }
    

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    backgroundColor: '#000000aa',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  txt: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: 'white',
    paddingTop: 10
  }  
});
