import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { List, ListItem } from "react-native-elements";
import Utils from '@helpers/Utils';

export default ModelUser = { 
  Model: {
    credit:'',  user_id:'', email:'', nickname:'', avatar_url: '', mobile: '', account_status: '', email_status: '', 
  },

  renderItemWithOnPress:(navigate, route) => {
    const r = ({item}) => {
      return (
        <ListItem
          roundAvatar
          title={`${item.name.first} ${item.name.last}`}
          subtitle={item.email}
          leftAvatar={{ source: { uri: item.picture.thumbnail } }}
          onPress={()=>{
            Utils.log('werewr');
            navigate(route, {item:item});
          }}
        />
      )};
    return r;
  },
  renderItem : ({item}) => {
    return (
      <ListItem
        roundAvatar
        title={`${item.name.first} ${item.name.last}`}
        subtitle={item.email}
        leftAvatar={{ source: { uri: item.picture.thumbnail } }}
        onPress={()=>{Utils.log(item)}}
      />
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});