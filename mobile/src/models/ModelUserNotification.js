import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";
import Utils from '@helpers/Utils';

export default ModelUserNotification = { 
  Model: {
    id:'', user_id:'', type:'', action: '', parameter:'', message:'', credit:'', created_at:'', status_read:'', hyperlink_text: '', hyperlink_url:'', banner_url:'',
},

  renderUserNotification:(navigate, route) => {
    renderItem = ({item, index}) => {
      return (        
        <TouchableHighlight
            onPress={ () => navigate(route, {item:item})}>
            <View style={{width:'100%', flex:1 , flexDirection:'row', backgroundColor: "white"}}>
                <Image accessibilityLabel={item.status_read == true?"have read.":"have not read."} style={{width: 50, height: 50, margin: 10}} resizeMode={'contain'} source={item.status_read == true? require('@assets/img/mail_opened.png') : require('@assets/img/mail_notopenyet.png')} />

                <View style={{flex: 1, flexDirection:'column', alignSelf: 'center'}}>
                  <Text style={{color:"black" , fontSize: 12, fontWeight: "bold"}} numberOfLines= {1} ellipsizeMode ={'tail'}>{item.title}</Text>
                  <Text style={{color:"black" , fontSize: 12}} numberOfLines= {1} ellipsizeMode ={'tail'}>Click to find out more</Text>
                </View>

                <Text style={{color: 'gray', fontSize: 10, alignSelf: 'center', position: 'absolute', right: 10}}>{Utils.timestampToTimeDifference(new Date(item.created_at * 1000))}</Text>
            </View>
        </TouchableHighlight >
      )
    }
    return renderItem;
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});