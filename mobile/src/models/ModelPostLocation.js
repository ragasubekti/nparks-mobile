import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableWithoutFeedback, Image } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

// const Images = {
//   false: require('@assets/img/like.png'),
//   true: require('@assets/img/liked.png')
// };

export default ModelPostLocation = { 
  Model: {
    id:'', id_hash:'', user_id:'', location:'', description:'', nickname:'', avatar_url:'', thumbnail_base_url:'', thumbnail_path: '', votes: '', latitude: '', longitude: '', created_at: '', hasVoted:'',
  },

  renderItemWithOnPress:(navigate, route, title) => {
    const renderItem = ({item, index}) => {
      return (
         <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item, title: title})}>
            <View style={{margin: 10, backgroundColor: "white"}}>
              <View style={{flexDirection:"row"}}>
                <Image style={{width: 40, height: 40, borderRadius: 20}} source={{uri: item.avatar_url}}/>
                <View style={{left: 10}}>
                  <Text style={{maxWidth: 60, fontSize:12, color:'black'}} numberOfLines= {1}>{item.nickname}</Text>
                  <Text style={{maxWidth: 75, fontSize:12, color:'#cccccc'}} numberOfLines= {1}>{Utils.timestampToTimeDifference(new Date(item.created_at * 1000))}</Text>
                </View>
              </View> 
              <CustomImage style={{width:125, height: 125, marginTop: 15, marginBottom: 5}} path={ item.thumbnail_base_url + '/thumbnail_square/' + item.thumbnail_path}/>
              <View style={{flexDirection:"row"}}>
                <Image style={{width: 15, height: 15}} resizeMode={'contain'} source={item.hasVoted == true ? require('@assets/img/liked.png') : require('@assets/img/like.png')}/>
                <Text accessibilityLabel={item.votes+" likes"} style={{left: 2, maxWidth: 20, fontSize: 12, bottom: 1}} numberOfLines= {1}>{item.votes}</Text>
                <Icon name="map-marker" size={14} style={{left: 10}} resizeMode={'contain'} color={"red"}/>
                <Text accessibilityLabel={"taken at "+item.location} style={{left: 12, fontSize: 12, maxWidth: 75, bottom: 1}} numberOfLines= {1}>{item.location}</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
      )};
    return renderItem;
  },
  
  renderViewAllItemWithOnPress:(navigate, route, title) => {
    const renderItem = ({item, index}) => {
      return (
        <TouchableWithoutFeedback accessibilityLabel={"Photo. " + (item.description==""?"no caption":"caption: "+item.description) + ". Photo taken at " + item.location + ", " + Utils.timestampToTimeDifference(new Date(item.created_at * 1000))} onPress={ () => navigate(route, {item:item, title:title})}>
          <View style={styles.cardViewAll}>
            <CustomImage style={{width: "100%", height: "100%"}} path={ item.thumbnail_base_url + '/thumbnail/' + item.thumbnail_path} />
          </View>
        </TouchableWithoutFeedback>
      )};
    return renderItem;
  },
 
  renderMyPost:(navigate, route, title) => {
    const r = ({item, index}) => {
      return(
        <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item, title: title})}>
          <View style={{flex: 1, width: '100%', backgroundColor: 'white', flexDirection:'row'}}>
            
            <View style={{flexDirection:'column', margin: 10, width: 35}}>
              <Text style={{color:"black", fontSize: 14, fontWeight: "bold"}}>{Utils.timestampToMonth(new Date(item.created_at * 1000))}</Text>
              <Text style={{color:"black", fontSize: 24, fontWeight: "bold"}}>{Utils.timestampToDay(new Date(item.created_at * 1000))}</Text>
            </View>

            <CustomImage style={{width: 100, height: 100, margin: 10}} path={ item.thumbnail_base_url + '/thumbnail/' + item.thumbnail_path}/>
            
            <View style={{flex: 1, flexDirection: 'column', marginTop: 10, marginRight: 20, marginBottom: 10}}>
              <View style={{flexDirection: 'row'}}>
                <Icon name="map-marker" size={14} style={{alignSelf:'center'}} resizeMode={'contain'} color={"red"}/>
                <Text numberOfLines= {1} ellipsizeMode ={'tail'}>{item.location}</Text>
              </View>

              <Text style={{flex: 1, flexWrap: 'wrap'}} numberOfLines={4}>{item.description}</Text>
            </View>

          </View>
        </TouchableWithoutFeedback>
      )};
    return r;
  },

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
  cardViewAll:{
    flex: 1,
    maxWidth: "33.33%",
    padding: 2,
    height:120,
    backgroundColor: "white"
  }
});