import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableWithoutFeedback, ImageBackground, Image } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ModelNparksPoiGroup = { 
  Model: {
    id:'', poi_group_id: '', poi_group_name: '', name:'', description:'', thumbnail_base_url:'', thumbnail_path: '', latitude: '', longitude: '', created_at: '',
  },

  renderItemWithOnPress:(navigate, route) => {
    const r = ({item, index}) => {
      return (
        <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item})}>
          <View style={{flex: 1, margin: 1, height:150, backgroundColor: "white"}}>
            <ImageBackground
              resizeMode={'cover'}
              style={{flex:1}}
              source={{uri: item.thumbnail_base_url + '/' + item.thumbnail_path}}
            >
            <View justifyContent='center' style={{width: "100%", height: 50, position: "absolute", bottom: 0, backgroundColor:"rgba(153,102,68,0.6)"}}>

            <Text style={{color:"white", fontSize: 12, marginLeft: 10, marginRight: 10, fontWeight: "bold"}}>{item.name}</Text>
            <View style={{flexDirection:"row", marginLeft: 10, marginRight: 10}}>
              <Icon style={{alignSelf: 'center'}} name="map-marker" size={14} color={"white"}/>
              <Text style={{left: 5, alignSelf: 'center', fontSize: 12, color: "white"}}>{item.poi_group_name}</Text>
            </View>

            </View>
            </ImageBackground>
          </View>
        </TouchableWithoutFeedback>
      )};
    return r;
  },
 

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});