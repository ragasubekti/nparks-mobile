import React from 'react';
import { StyleSheet } from 'react-native';

export default ModelHiddenLocationToday = { 
  Model: {
    id:'',  hidden_location_id:'', name:'', status_rare:'', status: '', latitude: '', longitude: '', thumbnail_base_url: '', thumbnail_path : '',
  },

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});