import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";

export default ModelOneMapGeocodeInfo = { 
  Model: {
    BUILDINGNAME:'', BLOCK:'', ROAD: '', POSTALCODE: '', XCOORD : '', YCOORD: '', LATITUDE: '', LONGITUDE: '', LONGTITUDE: '', 
  },

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});