import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableWithoutFeedback, Image } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

export default ModelUserReward = { 
  Model: {
    id:'', id_hash:'', name:'', short_description: '', description:'', unique_code:'', status_priority:'', logo_thumbnail_base_url:'', logo_thumbnail_path: '', thumbnail_base_url: '', thumbnail_path: '', type: '', expired_at: '', start_at: '', end_at: '', allocated_at: '', status_redeem: '', redeem_at: '', status_expired: '',
  },

  renderActiveReward:(navigate, route) => {
    const r = ({item, index}) => {
      return (
        <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item})}>
          <View style={{flex: 1, width: '100%', backgroundColor: 'white', flexDirection:'row'}}>
            <CustomImage style={{width:80, height: 80, margin: 10}} resizeMode={FastImage.resizeMode.contain} path={ item.logo_thumbnail_base_url + '/' + item.logo_thumbnail_path}/>
            <View style={{flex:1 ,flexDirection:'column', marginTop: 10, marginRight: 10}}>
              <Text style={{color:"black", fontSize: 14, fontWeight: "bold"}} numberOfLines= {1} ellipsizeMode ={'tail'}>{item.name}</Text>
              <Text style={{color:"#999", fontSize: 12}} numberOfLines= {1} ellipsizeMode ={'tail'}>{item.short_description}</Text>

              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Icon name="calendar-check" size={14} resizeMode={'contain'} color={"gray"}/>
                <Text style={{color:"#999", fontSize: 12}} numberOfLines= {1}>Received on {Utils.timestampToDate(new Date(item.allocated_at * 1000))}</Text>
              </View>

              <View style={{flexDirection: 'row'}}>
                <Icon name="calendar-blank" size={14} resizeMode={'contain'} color={"gray"}/>
                <Text style={{color:"#999", fontSize: 12}} numberOfLines= {1}>Expires {Utils.timestampToTimeDifference(new Date(item.expired_at * 1000))}</Text>
              </View>
            </View>

          </View>
        </TouchableWithoutFeedback>
      )};
    return r;
  },
  renderInactiveReward:(navigate, route) => {
    const r = ({item, index}) => {
      return (
        <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item})}>
          <View style={{flex: 1, width: '100%', backgroundColor: 'white', flexDirection:'row'}}>
            <CustomImage style={{width:80, height: 80, margin: 10}} resizeMode={FastImage.resizeMode.contain} path={ item.logo_thumbnail_base_url + '/' + item.logo_thumbnail_path}/>
            <View style={{flex: 1, flexDirection:'column', marginTop: 10, marginRight: 10}}>
              <Text style={{color:"black", fontSize: 14, fontWeight: "bold"}} numberOfLines= {1}>{item.name}</Text>
              <Text style={{color:"#999", fontSize: 12}} numberOfLines= {1}>{item.short_description}</Text>

              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Icon name="calendar-blank" size={14} resizeMode={'contain'} color={"gray"}/>
                <Text style={{color:"#999", fontSize: 12}} numberOfLines= {1}>{item.status_redeem == "true" ? "Redeemed on " + Utils.timestampToDate(new Date(item.redeem_at * 1000)) : "Expired on " + Utils.timestampToDate(new Date(item.expired_at * 1000))}</Text>
              </View>
            </View>

          </View>
        </TouchableWithoutFeedback>
      )};
    return r;
  },

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});