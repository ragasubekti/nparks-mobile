import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { ShareStyle, StyleConstant } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";
import Utils from '@helpers/Utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ModelUserJourney = { 
  Model: {
    id:'', user_id:'', type:'', action: '', parameter:'', message:'', detailedTitle:'', detailedSubtitle:'', detailedBody:'', credit:'', created_at:'',
},

  renderUserJourney:(navigate, route) => {
    renderItem = ({item, index}) => {
      return (
        <TouchableHighlight
          onPress={ () => navigate(route, {item:item})}>
          <View style={{width:'100%', flex:1 , flexDirection:'row', backgroundColor: "white"}}>   
            <View style={{flexDirection:'column', margin: 10}}>
              <Text style={{color:"black" , fontSize: 14}}>{item.message}</Text> 
              <Text style={{color:"#cccccc" , fontSize: 12}}>{Utils.timestampToTimeDifference(new Date(item.created_at * 1000))}</Text>
            </View>
            
            <View style={{flexDirection: 'row', alignSelf:'center', position:'absolute', right: 10}}>
              <Text accessibilityLabel={item.credit+" credits"} style={{color: StyleConstant.primaryColor}}>{item.credit == 0 ? "" : item.credit}</Text>
              <Image style={{width: item.credit == 0 ? 0 : 12, height: item.credit == 0 ? 0 : 12}} alignSelf='center' source={require('@assets/img/flower.png')}/>
              <Icon style={{alignSelf: 'flex-end'}} name="chevron-right" size={16} color={"#cccccc"}/>
            </View>
          </View>
        </TouchableHighlight>
      )
    }
    return renderItem;

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});