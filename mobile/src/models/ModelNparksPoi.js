import React from 'react';
import { View, Button, Text, TextInput, StyleSheet, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { ShareStyle } from '@assets/MyStyle';
import { List, ListItem } from "react-native-elements";

export default ModelNparksPoi = { 
  Model: {
    id:'',  name:'', description:'', thumbnail_base_url:'', thumbnail_path: '', created_at: '', created_by: '', updated_at: '', updated_by : '',
  },

  renderItemWithOnPress:(navigate, route) => {
    const r = ({item, index}) => {
      return (
        <TouchableWithoutFeedback onPress={ () => navigate(route, {item:item})}>
        <View style={{flex: 1, maxWidth: "50%", padding: 4, height:175}}>
          <ImageBackground
            resizeMode={'cover'}
            style={{flex:1}}
            source={{uri: item.thumbnail_base_url + '/' + item.thumbnail_path}}
          >
          <View style={{width: "100%", height: 55, position: "absolute", bottom: 0, backgroundColor:"rgba(153,102,68,0.6)"}}></View>
          <Text style={{color:"white", position: "absolute" ,bottom: 15, padding: 5, fontSize: 12, fontWeight: "bold"}}>{item.name}</Text>
          <Text style={{color:"white", position: "absolute" ,bottom: 0, padding: 5, fontSize: 11}}>{item.location_name}</Text>
          </ImageBackground>
        </View>
      </TouchableWithoutFeedback>
      )};
    return r;
  },
 

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});