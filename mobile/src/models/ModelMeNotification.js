import React from 'react';
import { StyleSheet } from 'react-native';

export default ModelMeNotification = { 
  Model: {
    id:'', name:'', description: '', thumbnail_url: '', type : '', zone_id: '', latitude: '', longitude: '', start_at: '', end_at: '',
  },
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    display: 'flex',
    width: 100,
    margin: 20,
  },
});