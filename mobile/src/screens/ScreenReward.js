import React, { useEffect, useState, useRef } from 'react';
import { ShareStyle, StyleConstant, HeaderStyleWithRightOnly, HeaderStyleWithRightOnlyIos, ShadowStyle } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, FlatList, ImageBackground, TouchableWithoutFeedback, Image, Platform, ScrollView, Dimensions } from 'react-native';
import {useNavigation} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import Constants from '@helpers/Constants';
import Utils from "@helpers/Utils";
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager-handy';
import ModelUserReward from '@models/ModelUserReward';
import CustomFlatList from '@components/CustomFlatList3';
import CustomFlatList2 from '@components/CustomFlatList2';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationEvents } from 'react-navigation';

const ScreenReward = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const dataUrl1 = Constants.API_SERVER_URL + '/user/my-rewards';
  const dataUrl2 = Constants.API_SERVER_URL + '/user/my-inactive-rewards';
  const r1 = ModelUserReward.renderActiveReward(navigate, 'detailReward');
  const r2 = ModelUserReward.renderInactiveReward(navigate, 'detailReward');
  const [hasChanges, setHasChanges] = useState(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
  const [xmasEventStatus, setXmasEventStatus] = useState(null);
  const [unlockedGingerbreadNumber, setUnlockedGingerbreadNumber] = useState(0);
  const { toggleRefresh, renderCustomFlatList } = CustomFlatList(dataUrl1, r1, Constants.LAYOUT_OFFSET);
  const panelHeight = (Dimensions.get('window').height)/8;
  const tabHeight = (Dimensions.get('window').height)/12;
  const nav2 = Platform.OS === "android"? HeaderStyleWithRightOnly : HeaderStyleWithRightOnlyIos; 

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptionsR});
    return function cleanup() { }
  }, []);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptionsR});
  }, [Settings.get(Settings.USER_CREDIT)]);

  // useEffect(() => {
  //   if(xmasEventStatus == null){
  //     getMyXmasEventStatus();
  //   }
  // }, [xmasEventStatus]);

  getMyXmasEventStatus = async()=>{
    let res = await WebApi.getMyXmasEventStatus();
    if (res.status == WebApi.STATUS_OK) {
      setXmasEventStatus(res.data);

      let gingerbreadman = res.data.gingerbreadman;
      var caughtCounter = 0;
      Object.keys(gingerbreadman).forEach(function(key) {
        if(gingerbreadman[key]){
          caughtCounter++;
        }
      });
      setUnlockedGingerbreadNumber(caughtCounter);
    } 
  }

  navOptionsR = {
    title: 'Rewards',
    headerRight: (
      <View style={{flexDirection:'row', marginRight: 10}}>
        <Text style={{color: 'white'}}>{Settings.get(Settings.USER_CREDIT)}</Text>
        <Image style={{width:16, height: 16}} alignSelf='center' source={require('@assets/img/flower.png')}/>
      </View>
    ),
    ...nav2
  };

  _renderTabIndicator = () => {
    let tabs = [
      { text: 'ACTIVE', },
      { text: 'INACTIVE', }
    ];
    return <PagerTabIndicator 
      style={{position:"absolute", top: 0, left: 0, right: 0, height: tabHeight, backgroundColor: '#ffffff', ...ShadowStyle}}
      tabs={tabs}
      textStyle = {{color: StyleConstant.tabGray}}
      selectedTextStyle={{color: StyleConstant.primaryColor, fontWeight: 'bold'}}
      itemStyle={{justifyContent: "center"}}
      selectedItemStyle={{justifyContent: "center"}}
    />;
  }


  renderActive = () =>{
    // Utils.log("active")
    return <CustomFlatList dataUrl={dataUrl1} renderItem={r1} result={Constants.LAYOUT_OFFSET} />;
  }

  renderInactive = () =>{
    // Utils.log("inactive")
    return <CustomFlatList2 dataUrl={dataUrl2} renderItem={r2} result={Constants.LAYOUT_OFFSET} />;
  }

  renderXmasPanel = () =>{
    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
      return(
        <View style={{flexDirection: 'column', width: '100%', height: (panelHeight*2), backgroundColor: 'white'}}>
          <ImageBackground style={{flex:1}} resizeMode="cover" source={require('@assets/img/xmas_bg_blue.png')}>

            <TouchableWithoutFeedback onPress={ () => navigate('infoEvent')}>
              <View style={{width:'90%', height: '50%', alignSelf: 'center', justifyContent: 'center', marginTop: '1%'}}>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>Christmas Event</Text>
                <Text style={{fontSize: 12}}>to find out more about Rewards</Text>
                <Icon name="chevron-right" style={{position: 'absolute', right: 0, alignSelf :'center'}} size={20} color={StyleConstant.xmasRed}/>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={ () => { if(xmasEventStatus!= null){ navigate('event',{xmasEventStatus : xmasEventStatus}) } }}>
              <View style={{flexDirection: 'row', width:'90%', height: '50%', alignSelf: 'center', marginTop: 1}}>
                <Image style={{width: (panelHeight * 0.6), height: (panelHeight * 0.6), alignSelf: 'center'}} source={require('@assets/img/rewards_breadman.png')}/>
                <Text style={{alignSelf: 'center', marginLeft: 10}}>Unlocked {unlockedGingerbreadNumber} of 12 Gingerbread Man</Text>
                <Icon name="chevron-right" style={{position: 'absolute', right: 0, alignSelf :'center'}} size={20} color={StyleConstant.xmasRed}/>
              </View>
            </TouchableWithoutFeedback>
            
          </ImageBackground>
        </View>
      );
    }
  }

  return (
    <View style={{flex : 1}}>

      <NavigationEvents
        onDidFocus={()=>{async function checkChanges(){
          if(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA)){
            setHasChanges(!hasChanges);
            toggleRefresh();
            Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
          }
          await Utils.refreshUserProfile();
          props.navigation.setParams({"navOptions":navOptionsR});
        }
        checkChanges();}}
        onWillFocus={()=>{
          // getMyXmasEventStatus();
        }}
      />
      {/* {renderXmasPanel()} */}
      <TouchableWithoutFeedback onPress={ () => navigate('infoReward')}>
        <View style={{flexDirection: 'column', width: '100%', height: panelHeight - (panelHeight/5), backgroundColor: 'white', justifyContent: 'center'}}>
          <View style={{justifyContent: 'center', width: '90%', alignSelf: 'center'}}>  
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Find out more</Text>
            <Text style={{fontSize: 12, color: '#cccccc'}}>about the rewards</Text>
            <Icon name="chevron-right" style={{position: 'absolute', right: 0, alignSelf :'center'}} size={20} color={"#999"}/>
          </View>
        </View>
      </TouchableWithoutFeedback>

      <IndicatorViewPager
        style={{flex:1, backgroundColor: "#eeeeee"}}
        indicator={this._renderTabIndicator()}>

        <View style={{paddingTop: tabHeight}}>
          {renderCustomFlatList()}
        </View>

        <View style={{paddingTop: tabHeight}}>
          {renderInactive()}
        </View>

      </IndicatorViewPager>

    </View>
  );

}

export default withScreenBase("reward", ScreenReward, ScreenBaseType.MAIN);

var styles = StyleSheet.create({
  holder:{
    marginTop: 50,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
  row: {
    flex: 1,
    justifyContent: "space-between"
  }
});
