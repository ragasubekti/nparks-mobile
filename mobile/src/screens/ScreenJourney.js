import React, { useEffect, useState } from 'react';
import { ShareStyle, HeaderStyleWithRight } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, FlatList, TouchableWithoutFeedback, TouchableOpacity, Image } from 'react-native';
import CustomButton from '@components/CustomButton';
import CustomTextInput from '@components/CustomTextInput';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import { Avatar } from 'react-native-paper';
import CustomFlatList from '@components/CustomFlatList';
import ModelUserJourney from '@models/ModelUserJourney';
import Constants from '@helpers/Constants';

const ScreenJourney = (props) => {
  const { navigate } = useNavigation();
  const [data, setData] = useState([]);
  const [dataProfile, setDataProfile] = useState([]);
  const [imageSource, setImageSource] = useState("");
  const dataUrl = Constants.API_SERVER_URL + '/user/my-journey-history';

  useEffect(() => {
    return function cleanup() {

    }
  }, []);

  const r = ModelUserJourney.renderUserJourney(navigate, 'journeyDetail');
  return (
    <View style={styles.holder}>
      <View style={{backgroundColor: 'white', flex: 1}}>
        <CustomFlatList dataUrl={dataUrl} renderItem={r} result={Constants.LAYOUT_OFFSET} />
      </View>
    </View>   
  );  
}


navigationOptions = ({navigation}) => ({
  title: 'My Journey',
  headerRight: (
    <View style={{flexDirection:'row', marginRight: 10}}>
      <Text accessibilityLabel={"you have "+navigation.getParam("credit", "0")+" credits"} style={{color: 'white'}}>{navigation.getParam("credit", "0")}</Text>
      <Image style={{width:16, height: 16}} alignSelf='center' source={require('@assets/img/flower.png')}/>
    </View>
  ),
  ...HeaderStyleWithRight
});
export default withScreenBase("me/journey", ScreenJourney, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    height: 200,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',    
  },  
});


