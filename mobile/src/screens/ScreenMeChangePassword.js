import React, { useEffect, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Button, Platform } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import CustomButton from '@components/CustomButton';
import { NavigationEvents } from "react-navigation";
import {CustomTextInput, CustomTextInputType} from '@components/CustomTextInput';
import WebApi from '@helpers/WebApi';
import Utils from '@helpers/Utils';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';

const ScreenMeChangePassword = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const [currentPassword, setcurrentPassword] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const { toggleActivityIndicator, showCustomDialog } = useContext(GlobalContext);

  const handleChangePassword = () => {
    if (currentPassword == "") {
      Utils.showToast("Please enter your current password");
      return;
    } else if (password == "") {
      Utils.showToast("Please enter your new password");
      return;
    } else if (confirmPassword == "") {
      Utils.showToast("Please confirm your new password");
      return;
    } else if (password != confirmPassword) {
      Utils.showToast("The passwords you entered do not match");
      return;
    } else if (Utils.hasEmoji(password)) {
      Utils.showToast("Password cannot contain any emoji");
      return;
    } else if (!Utils.validatePasswordStrength(password)) {
      Utils.showToast("Password must contain 12 or more characters with at least 1 UPPER case character and 1 lower case character."); 
    } else {
      changePassword();
    }    
  };

  const changePassword = async() => {
    toggleActivityIndicator(true)
    const res = await WebApi.changePassword(currentPassword, password, confirmPassword);
    Utils.log(res);
    toggleActivityIndicator(false)
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Utils.showToast(res.data[0].message);
    } else if (res.status == WebApi.STATUS_FORBIDDEN) {
      Utils.showToast(res.data.message);
    } else if (res.status == WebApi.STATUS_OK) {
      Utils.showToast("Successfully Changed Password");
      navigate('me');
      // need to make showToast function able to workm after navigate to new screen
    }
  }

  useEffect(() => {
    return function cleanup() {

    }
  }, []);

  return (
    <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#ffffff'}}>
      <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Current Password' value={currentPassword} setValue={setcurrentPassword}/>
      <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Password' value={password} setValue={setPassword}/>
      <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Confirm Password' value={confirmPassword} setValue={setconfirmPassword}/>
      <CustomButton style={{marginTop: 25}} onPress={handleChangePassword}>Confirm</CustomButton>
      
    </View>
  );  
}

navigationOptions = ({navigation}) => ({
  title: 'Change Password',
  ...HeaderStyleWithBack
});

export default withScreenBase("me/settings/change_password", ScreenMeChangePassword, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  map: {
    //...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  holder:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
});
