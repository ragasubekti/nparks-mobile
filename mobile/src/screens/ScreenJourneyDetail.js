import React, { useEffect, useState } from 'react';
import { ShareStyle, HeaderStyleWithRight, HeaderStyleWithBack } from '@assets/MyStyle';
import { View, StyleSheet, Image, TouchableWithoutFeedback, Linking, ScrollView, TouchableOpacity, ImageBackground, Alert } from 'react-native';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks';
import { Text } from 'react-native-paper';
import CustomFlatList from '@components/CustomFlatList';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import ModelUser from "@models/ModelUser";
import WebApi from '@helpers/WebApi';
import CustomButton from '@components/CustomButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Utils from "@helpers/Utils";
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';

const ScreenJourneyDetail = (props) => {
  const { navigationOptions } = useNavigation();
  const { navigate } = useNavigation();
  const item = useNavigationParam('item');
  const [hasChanges, setHasChanges] = useState(false);
  const [ inEventMode, setInEventMode] = useState(false);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE1 || Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
      setInEventMode(true);
    }
    return function cleanup() {}
  }, []);

  navOptions = {
    title: "History",
    ...HeaderStyleWithBack
  };

  Utils.log(item);
  return (
    <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
      <View style={{flex: 1, flexDirection: 'column'}}>
        <ImageBackground style={{flex: 1, alignItems: 'center'}} source={inEventMode? require('@assets/img/xmas_detail_bg.png') : require('@assets/img/detailednotification_bg.png')}>      

          <View style={{flex: 1, justifyContent:'center'}}>
            <Text style={{fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>{item.detailedTitle}</Text>
            <Text style={{alignSelf: 'center', marginTop: 20}}>{item.detailedSubtitle}</Text>        
            <Text style={{alignSelf: 'center', marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center'}}>{item.detailedBody}</Text>
          </View>

          <View style={{marginBottom: 20}}>
              <Text style={[ShareStyle.hyperlinkTxt]}>{Utils.timestampToDate(item.created_at*1000)}</Text>
          </View>

        </ImageBackground>
      </View>
    </ScrollView>
  );

}

export default withScreenBase("me/journey", ScreenJourneyDetail, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
  },
  holder1:{
    height: 250,
    justifyContent: 'center',
  },
  holder2:{
    flex: 1,
    height: 1000,
    flexGrow: 1,
  },
});