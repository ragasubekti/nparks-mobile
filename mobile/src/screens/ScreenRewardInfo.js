import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, FlatList, StyleSheet, Image, Dimensions, Platform, Linking } from 'react-native';
import {useNavigation} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import { stringToBytes } from 'convert-string';
import Constants from '@helpers/Constants';
import Utils from "@helpers/Utils";
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings } from '@helpers/Settings';
import  HTML from 'react-native-render-html';


const ScreenRewardInfo = () => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const iWidth = Dimensions.get('window').width;
  const iHeight = iWidth / 2;
  const [dataRewards, setDataRewards] = useState([]);
  //change the write up for the rewards info
  const htmlContent = "<span style='font - family:Helvetica Neue; color:#000000;font-size:14px'>Log in to the app, check-in at any of the 10 checkpoints, complete quests, share photos of your experience or find hidden fruits along the Trail to earn credits!<br><br>Convert your hard-earned credits into exciting rewards from out partners!</b><br><br><i><font color='#999999'>For more information on the rewards, click </font></i><a href='" + Constants.FRONTEND_SERVER_URL + "/site/faqs'>here.</a></span>";

  useEffect(() => {

    Utils.log('ScreenReward');

    return function cleanup() {
    }
  }, []);
  
  //do the same style as info tab for viewpager

  return (
    <View style={{flex: 1, backgroundColor: '#ffffff'}}>
      <Image style={{width: iWidth, height: iHeight}} alignSelf='center' source={require('@assets/img/Banner.png')} />
      <View style={{flex: 1, width: '90%', alignSelf: 'center', marginTop: 20}}>
        <HTML onLinkPress={()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL + "/site/faqs")}} textSelectable={true} html={htmlContent}/>
      </View>
    </View>
  );
}

navigationOptions = ({navigation}) => ({
  title: 'About Rewards',
  ...HeaderStyleWithBack
});

export default withScreenBase("reward/info", ScreenRewardInfo, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    marginTop: 50,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  scan:{
    width: 200,
    height: 200,
    backgroundColor: 'transparent',
  },  
});
