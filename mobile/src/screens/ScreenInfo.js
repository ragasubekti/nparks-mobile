import React, { useEffect, useState } from 'react';
import { ShareStyle, StyleConstant, HeaderStyle } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, FlatList, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { List, ListItem } from 'react-native-elements';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import CustomButton from '@components/CustomButton';
import { Settings, StoreSettings } from '@helpers/Settings';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager-handy';
import Constants from '@helpers/Constants';
import WebApi from '@helpers/WebApi';
import ModelNparksTenant from '@models/ModelNparksTenant';
import ModelNparksPoi from '@models/ModelNparksPoi';
import style from 'react-native-modal-selector/style';

const ScreenInfo = (props) => {
  const { navigate } = useNavigation();
  const [data, setData] = useState([]);
  const [dataService, setDataService] = useState([]);

  useEffect(() => {
    listPoi();
    listTenants();  
    return function cleanup() { }
  }, []);

  listPoi = async() => {
    const res = await WebApi.listPoi();
    setData(res.data);
  };

  listTenants = async() => {
    const res = await WebApi.listTenants();
    setDataService(res.data);
  };

  const d = [];

  const r1 = ModelNparksPoi.renderItemWithOnPress(navigate, 'poiList');
  const r2 = ModelNparksTenant.renderItemWithOnPress(navigate, 'tenantDetail');

  _renderTabIndicator = () => {
    let tabs = [{ text: 'POINTS OF INTEREST' }, { text: 'SERVICES' }];
    return <PagerTabIndicator 
      style={{position:"absolute", top: 0, left: 0, right: 0, height: 50}}
      tabs={tabs}
      textStyle = {{color: StyleConstant.tabGray}}
      selectedTextStyle={{color: StyleConstant.primaryColor, fontWeight: 'bold'}}
      itemStyle={{justifyContent: "center"}}
      selectedItemStyle={{justifyContent: "center"}}
      />;
  }

  renderPoiFlatlist = () =>{
    if(data.length != 0){
      return(
        <FlatList
            ListEmptyComponent={
              <View style={styles.centreContent}>
                  <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
              </View>
            }
            bounces={false}
            data={data}
            numColumns={2}
            columnWrapperStyle = {styles.row}
            renderItem={r1}
        />       
      );
    } else{
      return(
        <FlatList
            contentContainerStyle={styles.centreContainer}
            ListEmptyComponent={
              <View style={{flex: 1, ...styles.centreContainer}}>
                  <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
              </View>
            }
            bounces={false}
            data={data}
            numColumns={2}
            columnWrapperStyle = {styles.row}
            renderItem={r1}
        />       
      );
    }
  }

  renderTenantFlatlist = () =>{
    if(dataService.length != 0){
      return(
        <FlatList
            ListEmptyComponent={
              <View style={styles.centreContent}>
                  <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
              </View>
            }
            data={dataService}
            numColumns={2}
            columnWrapperStyle = {styles.row}
            renderItem={r2}
        />  
      );
    } else{
      return(
        <FlatList
            contentContainerStyle={styles.centreContainer}
            ListEmptyComponent={
              <View style={styles.centreContent1}>
                  <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
              </View>
            }
            data={dataService}
            numColumns={2}
            columnWrapperStyle = {styles.row}
            renderItem={r2}
        />  
      );
    }
  }

  return (
    <IndicatorViewPager
      style={{flex:1, backgroundColor: "#eeeeee"}}
      indicator={this._renderTabIndicator()}
    >
    <View style={{paddingTop: 50}}>
      {renderPoiFlatlist()}
    </View>

      <View style={{paddingTop: 50}}>
        {renderTenantFlatlist()}     
      </View>
    </IndicatorViewPager>
  );  
}

navigationOptions = ({navigation}) => ({ title: 'Information', ...HeaderStyle });

export default withScreenBase("Information", ScreenInfo, ScreenBaseType.MAIN, navigationOptions);

var styles = StyleSheet.create({
  holder:{ marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},  
  row: { flex: 1, justifyContent: "space-between" },
  centreContent: {flex: 1, height: '100%', alignItems: 'center', justifyContent: 'center'},
  centreContainer: {height: '100%', alignItems: 'center', justifyContent: 'center'},
  centreContent1: {flex: 1, height: 400, alignItems: 'center', justifyContent: 'center'}
});
