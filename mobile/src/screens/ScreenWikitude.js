import React, { useEffect, useRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Button, DeviceEventEmitter, TouchableWithoutFeedback, TouchableOpacity, Image , Platform, BackHandler } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import CustomButton from '@components/CustomButton';
import { NavigationEvents, NavigationActions } from "react-navigation";
import ArchitectView from '@components/ArchitectView';
import WebApi from '@helpers/WebApi';
import Constants from '@helpers/Constants';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import useJourneyAction from '@hooks/useJourneyAction';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Utils from '@helpers/Utils';

const ScreenWikitude = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const [count, setCount] = useState('');
  const counterRef = useRef(null);
  const arRef = useRef(null);
  const [showingDialog, setShowingDialog] = useState(false);
  const [navigatedAway, setNavigatedAway] = useState(false);
  const [stationData, setStationData] = useState([]);
  const [array, setArray] = useState([]);
  const [index, setIndex] = useState(0);
  const path = "file:///android_asset/wikitude/station/index.html";
  //apple is lame, cannot use full file path.
  const path_resource = "index";
  const path_ext = "html";
  const path_dir = "assets/wikitude/station";
  const serial = Platform.OS === "android"? Constants.WIKITUDE_SDK_KEY_DROID : Constants.WIKITUDE_SDK_KEY_IOS;
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const { doJourneyAction } = useJourneyAction();
  let backHandler = null;
  const hasTriggered = useRef(null);

  useEffect(() => {
    setCount(serial);
    navigateToExplore = navigateToExplore.bind(this);
    backHandler = BackHandler.addEventListener('hardwareBackPress', navigateToExplore);
    return function cleanup() {}
  }, []);

  navigateToExplore = () =>{
    backHandler.remove();
    props.navigation.reset([NavigationActions.navigate({routeName : "explore"})], 0);
    return true;
  }

  navigateExplore = () =>{
    props.navigation.reset([NavigationActions.navigate({routeName : "explore"})], 0);
    return true;
  }

  useEffect(() => {
    renderArView();
    if(arRef.current != null){
      if(Platform.OS === 'ios'){
        arRef.current.initFromReact(path_resource, path_ext, path_dir, serial) // for iOS
      } else {
        arRef.current.initFromReact(serial, path) // for Android
        // DeviceEventEmitter.addListener('wikitudeEvent', onArEventCb);
        if(DeviceEventEmitter.listeners('wikitudeEvent').length > 0){
          DeviceEventEmitter.removeAllListeners('wikitudeEvent');
        }
        DeviceEventEmitter.addListener('wikitudeEvent', onArEventCb);
      }
    }
    return function cleanup() {
      if(Platform.OS === 'ios'){
        arRef.current.stopWikitude();
      }
    }
  }, [count]);

  useEffect(() => {
    if(showingDialog == true){
      //alert('show dialog here');
    }
  }, [showingDialog]);

  onArEventCb = async(e) => {
    getStationData(e.param);
    if(e.action == "touch_model" && showingDialog == false){
      if(hasTriggered.current == null){
        hasTriggered.current = true;
        await doJourneyAction(Constants.ACTION_AR_SCAN, e.param);
      }
      setShowingDialog(true);
    }
  };

  getStationData = async(param)=>{
    const res = await WebApi.getStationQuest(param);
    if (res.status == WebApi.STATUS_OK) {
      setStationData(res.data);
      setArray(res.data.content);
    } else {
      alert("Unable to get quest");
    }
  }

  const displayBanner = async()=>{
    if(backHandler != null){ backHandler.remove(); }
    var res = await WebApi.getQuestTask(stationData.id, "banner");
    if (res.status == WebApi.STATUS_OK) {
      showCustomDialog({okButtonText:"OK", imagePath:res.data.banner_url, isImagePathUrl:res.data.banner_url, okCallback:{navigateExplore}, hasCancelButton:true, cancelCallback: {navigateExplore}});
      await doJourneyAction(Constants.ACTION_QUEST_COMPLETED, res.data.quest_key);
    } else {
      alert("Unable to get banner");
    }
  }
  
  renderArView = () =>{
    if(count != ""){
      return(
        <ArchitectView ref={(ref)=>{arRef.current = ref}} style={styles.wrapper} onArEvent={onArEventCb} />
      );
    }
  }

  doRandomLogic = () =>{
    let number = Math.floor(Math.random() * 2);
    if(number == 1){
      navigate('quiz',{station : stationData.id});
    } else{ 
      displayBanner();
    }
  }

  renderArDialog = (i)=>{
    if(showingDialog == true && stationData != null){
      return( 
        <TouchableWithoutFeedback onPress={()=>{
          if(i < array.length - 1){ setIndex(i + 1); }
          else {
            if(!navigatedAway){
              setNavigatedAway(true);
              doRandomLogic();
            }
          }
        }}>
        <View style={{flex:1, flexDirection:'row', width:'90%', height: 100, position:'absolute', bottom: 0, margin: 10, alignSelf:'center', backgroundColor:'white'}}>
          <Image style={{width: 100, height: 100}} source={{uri: stationData.content[i].img}}/>
          <Text style={{flex: 1 , flexWrap: 'wrap', fontSize: 12, margin: 5}}>{stationData.content[i].desc}</Text>
          <Icon style={{alignSelf: 'flex-end', marginBottom: 5, marginRight: 5}} name="chevron-right" size={24} color={"black"}/>
        </View>
        </TouchableWithoutFeedback>
      );
    }
  }

  return (
    <View style={ShareStyle.flex1}>
      {renderArView()}
      {renderArDialog(index)}
    </View>
  );  
}

navigationOptions = ({navigation}) => ({
  title: 'AR Scanner',
  headerLeft: (
    <TouchableOpacity style={{marginLeft: 10}} activeOpacity={0.5} onPress={() => navigation.reset([NavigationActions.navigate({routeName : "explore"})], 0)}>
      <Icon name={'close'} size={25} color={"white"}/>
    </TouchableOpacity>
  ),
  ...HeaderStyleWithBack
});

export default withScreenBase("explore/camera/wikitude_camera", ScreenWikitude, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  map: {
    //...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  holder:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
  container: {
    flex: 1
  },
  wrapper: {
    flex: 1
  },
  border: {
    borderColor: "#eee", borderBottomWidth: 1
  },
  button: {
    fontSize: 50, color: "orange"
  }
});
