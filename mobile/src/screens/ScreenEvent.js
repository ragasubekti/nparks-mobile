import React, { useEffect, useRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithRight, StyleConstant } from '@assets/MyStyle';
import { Text, View, FlatList, StyleSheet, Image, Dimensions, Platform, ImageBackground, ScrollView, TouchableOpacity, Linking } from 'react-native';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import Constants from '@helpers/Constants';
import Utils from "@helpers/Utils";
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import  HTML from 'react-native-render-html';
import { Button } from 'react-native-elements';
import useJourneyAction from '@hooks/useJourneyAction';
import Tooltip from 'rn-tooltip';

const ScreenEvent = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const iWidth = Dimensions.get('window').width;
  const iHeight = iWidth / 2;
  const [btnEnabled, setBtnEnabled] = useState(false);
  const status = useNavigationParam('xmasEventStatus');
  const { doJourneyAction } = useJourneyAction();
  const [numberOfLuckyDrawChance, setNumberOfLuckyDrawChance] = useState(0);
  const [completedAll, setCompletedAll] = useState(true);
  const [credit, setCredit] = useState(Settings.get(Settings.USER_CREDIT));
  const { toggleActivityIndicator, showCustomDialog } = useContext(GlobalContext);

  const tipArray = ['Gingerbread man 1', 'Gingerbread man 2', 'Gingerbread man 3', 'Gingerbread man 4', 'Gingerbread man 5', 'Gingerbread man 6', 'Gingerbread man 7', 'Gingerbread man 8', 'Gingerbread man 9', 'Gingerbread man 10', 'Gingerbread man 11', 'Gingerbread man 12'];
  const imgArray = [require('@assets/img/ginger1_thumb.png'), require('@assets/img/ginger2_thumb.png'), require('@assets/img/ginger3_thumb.png'), require('@assets/img/ginger4_thumb.png'), require('@assets/img/ginger5_thumb.png'), require('@assets/img/ginger6_thumb.png'), require('@assets/img/ginger7_thumb.png'), require('@assets/img/ginger8_thumb.png'), require('@assets/img/ginger9_thumb.png'), require('@assets/img/ginger10_thumb.png'), require('@assets/img/ginger11_thumb.png'), require('@assets/img/ginger12_thumb.png')];
  const boxArray = [require('@assets/img/ginger1_box.png'), require('@assets/img/ginger2_box.png'), require('@assets/img/ginger3_box.png'), require('@assets/img/ginger4_box.png'), require('@assets/img/ginger5_box.png'), require('@assets/img/ginger6_box.png'), require('@assets/img/ginger7_box.png'), require('@assets/img/ginger8_box.png'), require('@assets/img/ginger9_box.png'), require('@assets/img/ginger10_box.png'), require('@assets/img/ginger11_box.png'), require('@assets/img/ginger12_box.png')];

  const htmlContent = "<span style='font - family:Helvetica Neue; color:#000000;font-size:14px'><b>Earn lucky draw chances & stand to win $100 NTUC Fairprice Gift Vouchers!</b><br><br>All you have to do is:<br><br>- Visit checkpoints, complete quests, share photos of your experience or find the Gingerbread Men hidden along the C2C Trail to earn Flower Points. Find all 12 Gingerbread Men to earn 1,000 bonus Points!<br><br>- Every 1,500 Points earned will be exchanged for 1 lucky draw chance to win the NTUC Vouchers<br><br>The more Points you accumulate, the more chances you get to win!<br><br><i><font color='#999999'>Tap </font><a href='https://www.nparks.gov.sg/gardens-parks-and-nature/parks-and-nature-reserves/coast-to-coast/c2c-christmas-challenge'>here</a> to find out more!</i></span>";

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
    renderBtnLogic();
    for(let i = 0; i < Object.keys(status.gingerbreadman).length; i++){
      if(!status.gingerbreadman[i+1]){
        setCompletedAll(false);
      }
    }
    return function cleanup() {
    }
  }, []);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
  }, [credit]);

  renderBtnLogic = async() => {
    let credit = await Settings.get(Settings.USER_CREDIT); 
    if(credit >= 1000){
      setBtnEnabled(true);
    } else { setBtnEnabled(false); }
  }

  navOptions = {
    title: 'Gingerbread Men Hunt',
    headerRight: (
      <View style={{flexDirection:'row', marginRight: 10}}>
        <Text style={{color: 'white'}}>{credit}</Text>
        <Image style={{width:16, height: 16}} alignSelf='center' source={require('@assets/img/flower.png')}/>
      </View>
    ),
    ...HeaderStyleWithRight
  };

  handleFb = () =>{ Utils.log('fb'); }
  handleTwitter = () =>{ Utils.log('twiter'); }
  handleInsta = () =>{ Utils.log('insta'); }

  renderBreadmans = () =>{
    let arr = [];
    if(status != null){
      for(let i = 0; i < Object.keys(status.gingerbreadman).length; i++){
        if(status.gingerbreadman[i + 1]){
          arr.push(
            <Tooltip backgroundColor={"#ffffff"} withOverlay={false} popover={<Text>{tipArray[i]}</Text>} key={i}>
              <Image style={styles.imageBox} source={imgArray[i]}/>
            </Tooltip>);
        } else{ arr.push(<Image style={styles.imageBox} source={boxArray[i]} key={i}/>); }
      }
    }
    return( arr );
  }

  renderText = () =>{
    if(completedAll){
      return(
        <View>
          <Text style={styles.tipText}><Text style={{fontWeight: 'bold'}}>Congratulations!</Text> You have collected all 12 Gingerbread man.{'\n'}Continue to get more of them to get more points to win the vouchers</Text>
        </View>
      );
    } else{
      return(
        <View>
          <Text style={styles.tipText}>Unlock all Gingerbread Man{'\n'}to get 300 bonus points</Text>
        </View>
      );
    }
  }

  return (
    <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>   
      <View style={{flex: 1, backgroundColor: '#ffffff'}}>
        <View style={styles.topHolder} source={require('@assets/img/event_screen_bg.png')}>
            <Image style={styles.bgImage} source={require('@assets/img/event_screen_bg.png')}/>
            <View style={styles.boxHolder}>
              {renderBreadmans()}
            </View>
        </View>

        <View style={{flex: 1}}>
         
{/* 
          <View style={{backgroundColor: '#c6d9f1', alignItems: 'center'}}>
            <View style={{width: '100%', height: 50}}>
              <ImageBackground style={{flex:1, justifyContent:'flex-end'}} resizeMode="contain" source={require('@assets/img/event_divider.png')}>
                <Text style={styles.btmText}>CONVERTED {numberOfLuckyDrawChance}X CHANCE(S)</Text>
              </ImageBackground>
            </View>
            {renderText()}
          </View> */}

          <View style={styles.btmHolder}>
            <HTML onLinkPress={()=>{Linking.openURL("https://www.nparks.gov.sg/gardens-parks-and-nature/parks-and-nature-reserves/coast-to-coast/c2c-christmas-challenge")}} html={htmlContent}/>
          </View>
        </View>
        
      </View>
    </ScrollView>
  );
}

export default withScreenBase("reward/event", ScreenEvent, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  bgImage: {flex: 1, width: '100%', height: '100%', position: 'absolute', paddingBottom: 10},
  topHolder: {flex: 1, height: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start',
  alignContent: 'flex-start', flexWrap: 'wrap'},
  boxHolder: {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start',
  alignContent: 'flex-start', flexWrap: 'wrap', paddingTop: 20, paddingBottom: 20, paddingLeft: 5, paddingRight: 5},
  btn: {flex: 1, height: 40, width:'90%', alignSelf: 'center', marginTop: 20},
  fabBtn: {width: 16, height: 16, borderRadius: 8, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center', marginLeft: 5, marginRight: 5},
  btmHolder: {flexDirection: 'column', width: '90%', alignSelf: 'center', marginTop: 20, marginBottom: 20},
  fabHolder: {flexDirection: 'row', alignSelf: 'center', position: 'absolute', right: 10},
  imageBox: {width: (Dimensions.get('window').width) * 0.31, height: (Dimensions.get('window').width) * 0.31, margin: 2},
  btmText: {alignSelf: 'center', fontWeight: 'bold', marginBottom: 7, fontSize: 12},
  tipText: {alignSelf: 'center', width: '80%', marginTop: 3, marginBottom: 5, textAlign: 'center'}
});
