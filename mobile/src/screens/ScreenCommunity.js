import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, StyleConstant } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, ScrollView, Image, StatusBar, FlatList, TouchableWithoutFeedback, SafeAreaView, Dimensions } from 'react-native';
import {useNavigation} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import CustomFlatList from '@components/CustomFlatList';
import Utils from '@helpers/Utils';
import Constants from '@helpers/Constants';
import ModelPostLocation from '@models/ModelPostLocation';
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import { NavigationEvents } from 'react-navigation';

const ScreenCommunity = () => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const [userLat, setUserLat] = useState("");
  const [userLong, setUserLong] = useState("");
  const [dataTopPost, setDataTopPost] = useState([]);
  const [dataNearbyPost, setDataNearbyPost] = useState([]);
  const [dataRecentPost, setDataRecentPost] = useState([]);
  const [ inEventMode, setInEventMode] = useState(false);
  
  // const [hasChanges, setHasChanges] = useState(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));

  const imageWidth = Dimensions.get('window').width;
  const imageHeight = imageWidth/2.25;

  useEffect(() => {
    async function getLatitude(){
      setUserLat(await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE));
    }
    async function getLongitude(){
      setUserLong(await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE));
    }
    async function listPostLocation(){
      await listPostLocationTop();
      await listPostLocationNearby();
      await listPostLocationRecent();
    }
    getLatitude();
    getLongitude();
    listPostLocation();

    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }
    return function cleanup() {
    }
  }, []);

  const listPostLocationTop = async() => {
    const res = await WebApi.listPostLocationTop(0,Constants.LAYOUT_OFFSET);
    Utils.log(res);
    if (res.status == WebApi.STATUS_OK) {
      setDataTopPost(res.data);
    } else if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Utils.showToast(res.data[0].message);
    } else {
      Utils.showToast(res.data.message);
    }
  };

  const listPostLocationNearby = async() => {
    const res = await WebApi.listPostLocationNearby(0,Constants.LAYOUT_OFFSET);
    Utils.log(res);
    if (res.status == WebApi.STATUS_OK) {
      setDataNearbyPost(res.data);
    } else if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      // Utils.showToast(res.data[0].message);
    } else {
      Utils.showToast(res.data.message);
    }
  };

  const listPostLocationRecent = async() => {
    const res = await WebApi.listPostLocationRecent(0,Constants.LAYOUT_OFFSET);
    Utils.log(res);
    if (res.status == WebApi.STATUS_OK) {
      setDataRecentPost(res.data);
    } else if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Utils.showToast(res.data[0].message);
    } else {
      Utils.showToast(res.data.message);
    }
  };
  
  const r1 = ModelPostLocation.renderItemWithOnPress(navigate, 'communityDetail', Constants.TOP_POSTS);
  const r2 = ModelPostLocation.renderItemWithOnPress(navigate, 'communityDetail', Constants.NEARBY_POSTS);
  const r3 = ModelPostLocation.renderItemWithOnPress(navigate, 'communityDetail', Constants.RECENT_POSTS);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <NavigationEvents
        onWillFocus={()=>{async function checkChanges(){
          if(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA)){
            // setHasChanges(!hasChanges);
            Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
            await listPostLocationTop();
            await listPostLocationNearby();
            await listPostLocationRecent();
          }
        }
        checkChanges();}}
      />
      <ScrollView bounces={false} style={styles.scrollView}>
        <View style={{flex: 1}}>
          <Image style={{width: imageWidth, height: imageHeight}} resizeMode={'cover'} source={ inEventMode? require('@assets/img/communitybanner_xmas.png') : require('@assets/img/communitybanner.png')}/>
          <View style={styles.flatlistHolder}>
            <View style={{flexDirection:"row"}}>
              <Text style={styles.containerTitleText}>{Constants.TOP_POSTS}</Text>
              <Text accessibilityLabel={"view more "+Constants.TOP_POSTS} style={styles.viewMoreText} onPress={ () => navigate('communityViewAll', {title: Constants.TOP_POSTS})}>View more ›</Text>
            </View>
            <View style={styles.alignCentre}>
              <FlatList
                ListEmptyComponent={
                  <View style={styles.contentCentre}>
                      <Text style={styles.textGrey}>There is nothing to show at the moment</Text>
                  </View>
                }
                horizontal={true}
                data={dataTopPost}
                showsHorizontalScrollIndicator={false}
                renderItem={r1}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>

          <View style={styles.flatlistHolder}>
            <View style={{flexDirection:"row"}}>
              <Text style={styles.containerTitleText}>{Constants.NEARBY_POSTS}</Text>
              <Text accessibilityLabel={"view more "+Constants.NEARBY_POSTS} style={styles.viewMoreText} onPress={ () => navigate('communityViewAll', {title: Constants.NEARBY_POSTS, lat: userLat, long: userLong})}>View more ›</Text>
            </View>
            <View style={styles.alignCentre}>
              <FlatList
                // style={{justifyContent:'center', alignItems: 'center'}}
                ListEmptyComponent={
                  <View style={styles.contentCentre}>
                      <Text style={styles.textGrey}>There is nothing to show at the moment</Text>
                  </View>
                }
                horizontal={true}
                data={dataNearbyPost}
                showsHorizontalScrollIndicator={false}
                renderItem={r2}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>

          <View style={styles.flatlistHolder}>
            <View style={{flexDirection:"row"}}>
              <Text style={styles.containerTitleText}>{Constants.RECENT_POSTS}</Text>
              <Text accessibilityLabel={"view more "+Constants.RECENT_POSTS} style={styles.viewMoreText} onPress={ () => navigate('communityViewAll', {title: Constants.RECENT_POSTS})}>View more ›</Text>
            </View>
            <View style={styles.alignCentre}>
              <FlatList
                ListEmptyComponent={
                  <View style={styles.contentCentre}>
                      <Text style={styles.textGrey}>There is nothing to show at the moment</Text>
                  </View>
                }
                horizontal={true}
                data={dataRecentPost}
                showsHorizontalScrollIndicator={false}
                renderItem={r3}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

navigationOptions = ({navigation}) => ({ header: null });

export default withScreenBase("community", ScreenCommunity, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{  marginTop: 50,  height: 200,  justifyContent: 'center', alignItems: 'center',},
  scrollView: {flex: 1, backgroundColor: StyleConstant.bgGray},
  safeAreaView: {flex: 1, backgroundColor: '#003300'},
  flatlistHolder: {marginTop:10, backgroundColor: "white", minHeight: 250},
  containerTitleText: {color: "black", fontSize: 16, fontWeight: "bold", left: 10},
  viewMoreText: {color: "#111111", fontSize: 12, right: 10, position: 'absolute', alignSelf: "flex-end"},
  textGrey: {alignSelf: 'center', color: "#cccccc"},
  contentCentre: {width: '100%', height: 250, flex: 1, alignItems: 'center', justifyContent: 'center'},
  alignCentre: {flex: 1, alignItems: 'center'}
});
