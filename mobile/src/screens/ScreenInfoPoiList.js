import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Button as Btn, ImageBackground, FlatList, TouchableWithoutFeedback, Image, Platform } from 'react-native';
import { List, ListItem } from "react-native-elements";
import CustomButton from '@components/CustomButton';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import ModelNparksPoiGroup from '@models/ModelNparksPoiGroup';

const ScreenInfoPoiList = () => { 
  const i = useNavigationParam('item');
  const [data, setData] = useState([]);
  const { navigate, navigationOptions } = useNavigation();

  useEffect(() => {
    listPoiGroup();
    return function cleanup() {
    }
  }, []);

  listPoiGroup = async() => {
    const res = await WebApi.listPoiGroup(i.id);
    setData(res.data);
  };

  const r = ModelNparksPoiGroup.renderItemWithOnPress(navigate, 'poiDetail');

  return (
    <View style={styles.flatListBg}>
      <FlatList
          data={data}
          numColumns={1}
          columnWrapperStyle = {styles.row}
          renderItem={r}
          keyExtractor={(item, index) => index.toString()}
      />       
    </View>
  );

}

navigationOptions = ({navigation}) => ({
  title: 'Points of Interest',
  ...HeaderStyleWithBack
});

export default withScreenBase("Information/place_of_interest", ScreenInfoPoiList, ScreenBaseType.MAIN, navigationOptions);

var styles = StyleSheet.create({
  holder:{ marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},  
  cardHolder:{ maxWidth: "100%",  height: 175,  backgroundColor: "white"},
  flatListBg: {height: "100%", backgroundColor: StyleConstant.bgGray}
});
