import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, ScrollView, Image, StatusBar, FlatList, TouchableWithoutFeedback, Platform } from 'react-native';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import Constants from '@helpers/Constants';
import WebApi from '@helpers/WebApi';
import CustomFlatList from '@components/CustomFlatList';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import ModelPostLocation from '@models/ModelPostLocation';

const ScreenCommunityViewAll = () => {
  const params = useNavigationParam('title');
  const { navigate, navigationOptions } = useNavigation();
  var dataUrl;

  useEffect(() => {
    return function cleanup() {}
  }, []);

  if(params ==Constants.TOP_POSTS){
    dataUrl=(Constants.API_SERVER_URL + '/user/list-post-location-top-post');
  } else if(params == Constants.NEARBY_POSTS){
      const lat = useNavigationParam('lat');
      const long = useNavigationParam('long');
      dataUrl=(Constants.API_SERVER_URL + '/user/list-post-location-nearby?&latitude=' + lat + '&longitude=' + long);
  } else if(params == Constants.RECENT_POSTS){
    dataUrl=(Constants.API_SERVER_URL + '/user/list-post-location-recent');
  } else {
    //cluster post
    const lat = useNavigationParam('lat');
    const lng = useNavigationParam('lng');
    const result = useNavigationParam('number_of_posts');
    dataUrl=(Constants.API_SERVER_URL + '/user/list-post-location-cluster?&latitude=' + lat + '&longitude=' + lng + '&result=' + result);
  }

  let route = params == "Cluster Posts"? "exploreCommunityDetail" : "communityDetail";
  const r = ModelPostLocation.renderViewAllItemWithOnPress(navigate, route, params);
  
  return (
    <View style={styles.greyBG}>
      <CustomFlatList dataUrl={dataUrl} renderItem={r} result={Constants.LAYOUT_OFFSET} numColumns={3} seperator={false} />
    </View>
  );
}

navigationOptions = ({navigation}) => ({
  title: navigation.getParam("title", ""),
  ...HeaderStyleWithBack
});
export default withScreenBase("community/view_all", ScreenCommunityViewAll, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{ marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},
  heart:{ width: 20, height: 20, backgroundColor: 'transparent',},
  cardViewAll:{ flex: 1, maxWidth: "33.33%", padding: 2, height:120, backgroundColor: "white"},
  greyBG: {flex: 1, backgroundColor: StyleConstant.bgGray}  
});
