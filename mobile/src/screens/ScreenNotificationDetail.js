import React, { useEffect, useState } from 'react';
import { ShareStyle, HeaderStyleWithRight, StyleConstant } from '@assets/MyStyle';
import { View, StyleSheet, Image, TouchableWithoutFeedback, Linking, ScrollView, TouchableOpacity, ImageBackground, Alert, Dimensions } from 'react-native';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks';
import { Text } from 'react-native-paper';
import CustomFlatList from '@components/CustomFlatList';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import ModelUser from "@models/ModelUser";
import WebApi from '@helpers/WebApi';
import CustomButton from '@components/CustomButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Utils from "@helpers/Utils";
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import CustomImage from '@components/CustomImage';

const ScreenNotificationDetail = (props) => {
  const { navigationOptions } = useNavigation();
  const { navigate } = useNavigation();
  const item = useNavigationParam('item');
  const [hasChanges, setHasChanges] = useState(false);
  const [ inEventMode, setInEventMode] = useState(false);
  const imageWidth = (Dimensions.get('window').width) * 0.5;

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
    if(item.status_read == false){
      readNotification();
      setHasChanges(true)
    }
    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }    
    return function cleanup() {}
  }, []);

  const readNotification = async() => {
    let res = await WebApi.readNotification(item.id);
    // Utils.log(res.status);
    let success = false;
      if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
        const err = res.data[0].message
        Utils.showToast(err);
      } else if (res.status == WebApi.STATUS_FORBIDDEN) {
        const err = res.data.message
        // Utils.showToast(err);
      } else if (res.status == WebApi.STATUS_OK) {
        let data = (res.data);
        Utils.log(data);
        Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
        // Utils.showToast("read noti");
        success = true;
      }
    return success;
  }

  const deleteAlert = ()=> {
    Alert.alert(
      'Delete notification?',
      'Are you sure you want to delete this notification?',
      [
        {text: 'NO', onPress: ()=> {Utils.log('no')}},
        {text: 'YES', onPress: ()=>{async function onYes(){
          await deleteNotification();
        }
        onYes(); }},
      ]
    );
  }

  const deleteNotification = async() => {
    let res = await WebApi.deleteNotification(item.id);
    // Utils.log(res.status);
    let success = false;
      if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
        const err = res.data[0].message
        Utils.showToast(err);
      } else if (res.status == WebApi.STATUS_OK) {
        let data = (res.data);
        // Utils.log(data);
        success = true;
        if(!hasChanges){
          Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
        }
        // Utils.showToast("Deleted");
        navigate("notification");
      } else {
        navigate("notification");
      }
    return success;
  }

  navOptions = {
    title: "Notification",
    headerRight: (
      <TouchableOpacity accessibilityLabel="delete this message button" style={{marginRight: 10}} activeOpacity={0.5} onPress={deleteAlert}>
        <Icon name="trash-can-outline" size={20} color={"white"}/>
      </TouchableOpacity>
    ),
    ...HeaderStyleWithRight
  };

  // Utils.log(item);
  return (
    <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
      <View style={{flex: 1, flexDirection: 'column'}}>
        <ImageBackground style={{flex: 1, alignItems: 'center'}} source={inEventMode? require('@assets/img/xmas_detail_bg.png') : require('@assets/img/detailednotification_bg.png')}>   
          
          <View style={{flex: 1, justifyContent:'center'}}>
            <CustomImage style={{width: imageWidth, height: imageWidth, marginTop: 10, alignSelf:'center', marginBottom: 10}} resizeMode={'contain'} path={item.banner_url}/>
            <Text style={{fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>{item.title}</Text>
            <Text style={{alignSelf: 'center', marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center'}}>{item.subtitle}</Text>        
            <Text style={{alignSelf: 'center', marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center'}}>{item.body}</Text>
          </View>

          <View style={{marginBottom: 20}}>
            <TouchableWithoutFeedback onPress={ () => Linking.openURL(Utils.reformHyperlink(item.hyperlink_url))}>
              <Text style={[styles.hyperlinkTxt]}>{item.hyperlink_text}</Text>
            </TouchableWithoutFeedback>
          </View>

        </ImageBackground>
      </View>
    </ScrollView>
  );

}

export default withScreenBase("me/notification/detail", ScreenNotificationDetail, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  container:{ flex: 1, alignItems: 'center', },
  holder1:{ height: 250, justifyContent: 'center', },
  holder2:{ flex: 1, height: 1000, flexGrow: 1, },
  hyperlinkTxt: { color: StyleConstant.primaryColor, textDecorationLine: 'underline' }
});