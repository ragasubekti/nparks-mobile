import React, { useEffect, useState, useContext, useRef } from 'react';
import { ShareStyle, HeaderStyleWithRight, StyleConstant } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Button, TouchableOpacity,Linking,Platform } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import CustomButton from '@components/CustomButton';
import { NavigationEvents } from "react-navigation";
import Constants from '@helpers/Constants';
import { Avatar } from 'react-native-paper';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import WebApi from '@helpers/WebApi';
import {CustomTextInput, CustomTextInputType} from '@components/CustomTextInput';
import Utils from '@helpers/Utils';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {ImagePath} from '@assets/imagePaths';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

const ScreenMeEditProfile = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const [imageSource, setImageSource] = useState("");
  const [nick, setNick] = useState(""); 
  const [currentAvatar, setcurrentAvatar] = useState(""); 
  const [fileSize, setfileSize] = useState(0); 
  const { toggleActivityIndicator, showCustomDialog } = useContext(GlobalContext);
  const avatarRef = useRef(null);
  const nickRef = useRef(null);
  const [hasStoragePermission, setHasStoragePermission] = useState(false);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
    checkStoragePermission(false);
    async function getCurrentAvatar(){
      setNick(await StoreSettings.get(StoreSettings.USER_NICKNAME));
      setImageSource(await StoreSettings.get(StoreSettings.USER_AVATAR_URL));
      setcurrentAvatar(await StoreSettings.get(StoreSettings.USER_AVATAR_URL));
    }
    getCurrentAvatar();
    avatarRef.current = currentAvatar;
    return function cleanup() {
    }
  }, []);

  checkStoragePermission = async(showDialog) =>{
    let result;
    if (Platform.OS === "android") {
      let writeResult = await check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
      let readResult = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
      result = writeResult == RESULTS.GRANTED || readResult == RESULTS.GRANTED;
    } else {
      let library = await check(PERMISSIONS.IOS.PHOTO_LIBRARY);
      if(library == RESULTS.BLOCKED){

        if(showDialog){
          openAppSettings=()=>{
            Linking.openURL('app-settings:');
          }
          showCustomDialog({body:"Coast-to-Coast would like access to your Library to pick a new avatar photo", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{openAppSettings}, hasCancelButton:true});
        }
        return;
      }
      result = library == RESULTS.GRANTED;
    }

    if(result){
      setHasStoragePermission(true);
    } else {
      if(showDialog){
        showCustomDialog({body:"Coast-to-Coast would like access to your Library to pick a new avatar photo", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{requestForStoragePermissions}, hasCancelButton:true});
      }
    }
  }

  navigateToMe = () =>{
    navigate("me");
  }

  requestForStoragePermissions =()=>{
    requestForPermissions3()
    .then((status)=>{
      Utils.log("ScreenMeEditProfile - hasRequiredPermissions: " + status);
      if(status){
        setHasStoragePermission(true);
      } else {
        // navigateToMe();
      }
    })
  }

  requestForPermissions3 = async() =>{
    var result = false;
    try{
      if (Platform.OS === "android") {
        var writeResult = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
        var readResult = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
        result = writeResult == RESULTS.GRANTED || readResult == RESULTS.GRANTED;
      } else {
        var photoResult = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
        result = photoResult == RESULTS.GRANTED;
      }
      
    } catch (e) {
      Utils.log(e);
    }
    return result;
  }

  selectPhotoTapped = () => {
    if(hasStoragePermission){
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
        mediaType:'photo'
      };
  
      launchImageLibrary(options, (response)  => {
        // Same code as in above section!
        Utils.log('Response = ', response);
        if (response.didCancel) {
          Utils.log('User cancelled photo picker');
        } else if (response.error) {
          Utils.log('launchImageLibrary Error: ', response.error);
        } else {
          //addToList(response);
          setImageSource(response.uri);
          avatarRef.current = response.uri;
          setfileSize(response.fileSize)
        }
      });
    } else {
      checkStoragePermission(true);
    }
  }

  renderAvatar = ()=>{
    if(imageSource != ""){
      return(
        <TouchableOpacity accessibilityLabel="select a profile photo from gallery" style={{marginTop: 20}} onPress={selectPhotoTapped}>
          <CustomImage style={{width: 120, height: 120, borderRadius: 60}} path={imageSource}/>
        </TouchableOpacity>
      );
    }
    else {
      return(
        <TouchableOpacity accessibilityLabel="select a profile photo from gallery" style={{marginTop: 20}} onPress={selectPhotoTapped}>
          <CustomImage style={{width: 120, height: 120, borderRadius: 60}} path={Constants.DEFAULT_PROFILE_PICTURE_URL} />
        </TouchableOpacity>
      );
    }
  }

  onPressUpdate = async() =>{    
    Utils.log(avatarRef.current);
    Utils.log(nickRef.current);
    if(avatarRef.current == null || avatarRef.current == currentAvatar && nickRef.current == null){      
      Utils.showToast("Nothing to update");
    } else {
      await updateProfile();
    }
  }

  navOptions = {
    title: "Edit Profile",
    headerRight: (
      <TouchableOpacity style={{marginRight: 10}} activeOpacity={0.5} onPress={onPressUpdate}>
        <Text style={{color:'white'}}>SAVE</Text>
      </TouchableOpacity>
    ),
    ...HeaderStyleWithRight
  };

  updateProfile = async()=>{
    toggleActivityIndicator(true)
    let isPhotoChanged = false;
    let isNicknameChanged = false;

    if (imageSource != "") {
      //change photo
      if (!Utils.isPhotoLessThanMaxSize(fileSize)) {
        toggleActivityIndicator(false)
        return;
      }
      if(await uploadAvatar()){
        isPhotoChanged = true;
      }
    }

    if (nick != "") {
      //change nickname
      if (nick.length < 6) {
        toggleActivityIndicator(false)
        Utils.showToast("Nickname cannot be shorter than 6 characters");
        return;
      }
      if(nick.length > 30) {
        toggleActivityIndicator(false)
        Utils.showToast("Please make sure that nickname is not longer than 30 characters");
        return;
      }
      if(await changeNickname()){
        isNicknameChanged = true;
      }
    }      
    
    if (isPhotoChanged & isNicknameChanged) {
      //both successful
      Utils.showToast("Updated nickname and photo");
    } else if (!isPhotoChanged & isNicknameChanged) {
      //nickname successful
      Utils.showToast("Updated nickname");
    } else if (isPhotoChanged & !isNicknameChanged) {
      //photo successful
      Utils.showToast("Updated photo");
    } else {
      return;
    }

    toggleActivityIndicator(false)
    navigateToMe();
  }

  changeNickname = async() =>{
    var res = await WebApi.changeNickname(nickRef.current);
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Utils.showToast(res.data[0].message);
    } else if (res.status == WebApi.STATUS_FORBIDDEN) {
      Utils.showToast(res.data.message);
    } else if (res.status == WebApi.STATUS_OK) {
      return true;
    }
    return false;
  }

  uploadAvatar = async() =>{
    var res = await WebApi.uploadAvatar(imageSource, 1);
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Utils.showToast(res.data[0].message);
    } else if (res.status == WebApi.STATUS_FORBIDDEN) {
      Utils.showToast(res.data.message);
    } else if (res.status == WebApi.STATUS_OK) {
      return true;
    }
    return false;
  }

  onChangeText = (text) =>{
    if(text == Constants.DEBUG_KEY){
      Settings.store(Settings.DEBUG_MODE, Constants.STATUS_ENABLED);
      Utils.showToast("DEBUG MODE ON")
    }
  }

  return (
    <View style={{flexDirection: 'column', flex: 1, alignItems: 'center', backgroundColor: '#ffffff'}}>
      {renderAvatar()}
      <TouchableOpacity style={{marginTop: 10}} onPress={selectPhotoTapped}>
        <Text style={{color: StyleConstant.primaryColor}}>Change Profile Picture</Text>
      </TouchableOpacity>
      <CustomTextInput placeholder={nick} value={nick} setValue={(text) =>{nickRef.current = text; onChangeText(text)}}/>
    </View>
  );  
}

export default withScreenBase("me/settings/edit_profile", ScreenMeEditProfile, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  map: {
    //...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  holder:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
});
