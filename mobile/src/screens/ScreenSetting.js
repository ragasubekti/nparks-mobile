import React, { useEffect, useState, useContext } from 'react';
import { HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, StyleSheet, FlatList, TouchableOpacity, Linking, Alert } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ImagePath} from '@assets/imagePaths';
import Utils from '@helpers/Utils';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Constants from '@helpers/Constants';

const ScreenSetting = (props) => {
  const { navigate } = useNavigation();
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const [nickname, setNickname] = useState("");
  const [email, setEmail] = useState("");

  useEffect(() => {
    async function getNickname(){
      setNickname(await StoreSettings.get(StoreSettings.USER_NICKNAME));
    }
    async function getEmail(){
      setEmail(await StoreSettings.get(StoreSettings.USER_EMAIL));
    }
    getNickname();
    getEmail();
    return function cleanup() {

    }
  }, []);

  const handleLogout = async() => {
    showCustomDialog({body:"Are you sure you want to leave?", okButtonText:"Log out", imagePath:ImagePath.LOGOUT, okCallback:{logout}, hasCancelButton:true});
  }

  setTelemetry = () =>{
    Alert.alert(
      'Make Mapbox Maps Better',
      'You are helping to make OpenStreetMap and Mapbox maps better by contributing anonymous usage data.',
      [
        {text: 'DISAGREE', onPress: ()=>{MapboxGL.setTelemetryEnabled(false);}},
        {text: 'AGREE', onPress: ()=>{MapboxGL.setTelemetryEnabled(true);}}
      ]
    );
  }

  const logout = async() => {
    //call logout api
    await StoreSettings.store(StoreSettings.IS_LOGGED_IN, 'false');
    WebApi.logout()
    .then(async(res) =>{
      await StoreSettings.reset();
      Settings.reset();
    })
    .catch((err) =>{Utils.error(err)})
    .finally(()=>{navigate('loading')})
  }

  const handleEdit = ()=>{ navigate('editProfile'); }
  const handlePw = ()=>{ navigate('changePw'); }
  
  const settingList = [
    {
      "title": "Support",
      "type": "header",
      "action" : ()=>{}
    },
    {
      "title": "FAQs",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/faqs")}
    },
    {
      "title": "Feedback & Enquiry",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.NPARKS_FEEDBACK_ENQUIRY_URL)}
    },
    {
      "title": "Report an App Bug",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/feedback?name=" + nickname + "&email=" + email)}
    },
    {
      "title": "Report Vulnerability",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.GOV_TECH_REPORT_VULNERABILITY_URL)}
    },
    {
      "title": "Tutorial",
      "type": "title",
      "action" : ()=>{navigate('tutorial', {page:"settings"})}
    },
    {
      "title": "About",
      "type": "header",
      "action" : ()=>{}
    },
    {
      "title": "About the App",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/app")}
    },
    {
      "title": "Terms of Use",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/terms")}
    },
    {
      "title": "Privacy Policy",
      "type": "title",
      "action" : ()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/privacy")}
    },
    {
      "title": "Account",
      "type": "header",
      "action" : ()=>{}
    },
    {
      "title": "Password",
      "type": "title",
      "action" : ()=>{navigate('changePw')}
    },
    {
      "title": "Edit Profile",
      "type": "title",
      "action" : ()=>{navigate('editProfile')}
    },
    {
      "title": "Telemetry Settings",
      "type": "title",
      "action" : setTelemetry
    },
    {
      "title": "Logout",
      "type": "title",
      "action" : handleLogout
    },
  ]
 
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <FlatList
        data={settingList}
        renderItem={({item}) => 
        <TouchableOpacity activeOpacity={0.5} onPress={item.action}>
        <View style={{width: '100%', height: 40, backgroundColor:'white'}}>
          <Text style={{marginLeft: 10, fontSize: 16, fontWeight: item.type === "header"? 'bold' : 'normal'}}>{item.title}</Text>
          <Icon style={{position:'absolute', right: 10}} name={item.type === "header"? '' : 'chevron-right'} size={20} color={"gray"}/>
        </View>
        </TouchableOpacity>
        }
      />
    </View>   
  );  
}

navigationOptions = ({navigation}) => ({
  title: 'Settings',
  ...HeaderStyleWithBack
});

export default withScreenBase("me/settings", ScreenSetting, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    marginTop: 50,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
});
