import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { Text, View, FlatList, StyleSheet, Image, ScrollView, Alert, Clipboard } from 'react-native';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import Constants from '@helpers/Constants';
import Utils from "@helpers/Utils";
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Slider from '@react-native-community/slider';
import { Button } from 'react-native-elements';
import  HTML from 'react-native-render-html';
import useJourneyAction from '@hooks/useJourneyAction';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

const ScreenRewardDetail = () => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const [htmlContent, setHtmlContent] = useState([]);
  const [value, setValue] = useState(0);
  const [textVisible, setTextVisible] = useState(true);
  const item = useNavigationParam('item');
  const sliderRef = useRef(null);
  const { doJourneyAction } = useJourneyAction();

  useEffect(() => {
    Utils.log(item);
    setHtmlContent(item.description);
    return function cleanup() {
    }
  }, []);
  
  const handleSlider = (v)=>{
    setTextVisible(true);
    if(v > 0.8){
      setValue(1);
      redeemAlert();
    } else{ setValue(0.001); }
  }

  const redeemAlert = ()=> {
    Alert.alert(
      'Redeem Reward?',
      'Are you sure you want to this reward?',
      [
        {text: 'NO', onPress: ()=> {navigate('reward')}},
        {text: 'YES', onPress: ()=>{redeemReward()}},
      ]
    );
  }

  const redeemReward = async()=> {
    let res = await doJourneyAction(Constants.ACTION_REDEEM_REWARD, item.id_hash);
    if(res == true){
      Utils.showToast('redeemed successfully');
      Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
      navigate('reward');
    } else {
      Utils.showToast('error redeeming reward');
      setValue(0.001);
    }
  }

  writeToClipboard = async () => {
    await Clipboard.setString(item.unique_code);
    Utils.showToast('Copied to Clipboard!');
  };

  const renderFooter = ()=>{
    //if item.status_redeem is true render expired or redeemed button
    if(item.status_redeem == 'true' || item.status_expired == "true"){
      return renderInctive();
    } else {
      return renderActive();
    }
    
  }

  const renderActive = () =>{
    if(item.type == 'store_voucher'){
      return(
        <View style={{width: '80%', height: 40, margin: 10, borderRadius: 20, overflow:'hidden', alignSelf: 'center', justifyContent:'center'}}>
          <Slider
              ref={(r)=>{sliderRef.current = r}} step={0.1} style={{flex: 1, backgroundColor:'#1b5e20'}} value={value} minimumValue={0} maximumValue={1}
              onSlidingComplete = {handleSlider}
              onValueChange = {(sliderValue)=>{
                setValue(sliderValue);
                setTextVisible(false);
              }}
              minimumTrackTintColor={"#ffffff00"}
              maximumTrackTintColor={"#ffffff00"}
          />
          <Text style={{alignSelf:'center', position: 'absolute', color: 'white', opacity: textVisible? 100 : 0}}>Swipe to redeem</Text>
        </View>
      );
    } else {
      return(
        <Button
          containerStyle={{flex: 1, height: 40, alignSelf: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10}}
          title={item.unique_code}
          onPress={this.writeToClipboard}
          buttonStyle={{backgroundColor: StyleConstant.primaryColor}}
        />
      );
    }    
  }

  const renderInctive = () =>{
    if(item.status_redeem == 'true'){
      return(
        <Button
          containerStyle={{flex: 1, height: 40, alignSelf: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10}}
          title="REDEEMED"
          onPress={() => {Utils.showToast("Redeemed on " + Utils.timestampToDate(new Date(item.redeem_at * 1000)));}}
          buttonStyle={{backgroundColor: "#ff0000"}}
        />
      );
    } else {
      return(
        <Button
          containerStyle={{flex: 1, height: 40, alignSelf: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10}}
          title="EXPIRED"
          onPress={() => {Utils.showToast("Expired on " + Utils.timestampToDate(new Date(item.expired_at * 1000)));}}
          buttonStyle={{backgroundColor: "#ff0000"}}
        />
      );
    }
  }

  return (
    <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>   
      <View style={{flex: 1 , backgroundColor: '#eeeeee'}}>
      
        <CustomImage style={{width: '100%', height: 150}} resizeMode={FastImage.resizeMode.cover} path={item.thumbnail_base_url + '/' + item.thumbnail_path} />
        <View style={{width: 80, height: 80, borderWidth: 1, borderColor: '#cccccc', backgroundColor: 'white', justifyContent: 'center', alignSelf: 'center', marginTop: -40}}>
        <CustomImage style={{width: 80, height: 80}} resizeMode={FastImage.resizeMode.contain} path={ item.logo_thumbnail_base_url + '/' + item.logo_thumbnail_path} />
        </View>

        <View style={{flex: 1, marginLeft: 10, marginRight: 10, marginBottom: 50}}>
          <Text style={{color: 'black', fontSize: 16, alignSelf: 'center', textAlign: 'center', marginTop: 10, marginBottom: 10}}>{item.name}</Text>
          <Text style={{color: 'gray', alignSelf: 'center', textAlign: 'center'}}>{item.short_description}</Text>
          <HTML html={htmlContent}/>
        </View>

        <View style={{flex: 1, width:'100%', backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'}}>
          {renderFooter()}
        </View>
      
      </View>
    </ScrollView>
  );
}

navigationOptions = ({navigation}) => ({
  title: 'Rewards',
  ...HeaderStyleWithBack
});

export default withScreenBase("reward/detail", ScreenRewardDetail, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    marginTop: 50,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  scan:{
    width: 200,
    height: 200,
    backgroundColor: 'transparent',
  },  
});
