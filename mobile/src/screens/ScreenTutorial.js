import React, { useEffect, useRef } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Button, ScrollView, Image, Dimensions, ImageBackground, TouchableOpacity, SafeAreaView, BackHandler } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import CustomButton from '@components/CustomButton';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager-handy';
import { NavigationEvents } from "react-navigation";
import ImageZoom from 'react-native-image-pan-zoom';
import  HTML from 'react-native-render-html';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ScreenTutorial = (props) => {
  const prevScreen = useNavigationParam('page');
  const { navigate, navigationOptions } = useNavigation();
  let zoomRef2, zoomRef3, zoomRef4, zoomRef5, zoomRef6 = null;
  const setZoomRef2 = ref2 => zoomRef2 = ref2;
  const setZoomRef3 = ref3 => zoomRef3 = ref3;
  const setZoomRef4 = ref4 => zoomRef4 = ref4;
  const setZoomRef5 = ref5 => zoomRef5 = ref5;
  const setZoomRef6 = ref6 => zoomRef6 = ref6;

  
  const iWidth = (Dimensions.get('window').width) * 0.9;
  const iHeight = (iWidth/4) * 5;

  const htmlContent = "<span style='font - family:Helvetica Neue; color:#000000;font-size:14px'>Hi there! I'm Phil, a leaf from the native broad-leaved Sterculia (<i><u>Sterculia macrophylla)</u></i><br><br>Welcome to the Coast-to-Coast (C2C) app, the first AR-enabled app by the National Parks Board (NParks)!<br><br>Join me as I guide you through a curated walking experience to discover parks, park connectors, points of interest, local flora and fauna and earn attractive rewards while doing so!</span>";

  useEffect(() => {
    handleStart = handleStart.bind(this);
    BackHandler.addEventListener('hardwareBackPress', handleStart);
    return function cleanup() {}
  }, []);

  handleStart = ()=>{
    BackHandler.removeEventListener('hardwareBackPress', handleStart);
    prevScreen === "settings" ? navigate('setting') : navigate('explore');
    return true;
  }

  _renderDotIndicator= ()=> { return <PagerDotIndicator pageCount={6} />; }

  onPageChanged = ()=>{
    zoomRef2.reset();
    zoomRef3.reset();
    zoomRef4.reset();
    zoomRef5.reset();
    zoomRef6.reset();
  }

  return (
    <View style={{flex: 1}}>
      <IndicatorViewPager
          style={{flex: 1}}
          onPageSelected={()=>{onPageChanged()}}
          indicator={this._renderDotIndicator()}
      >
          {/* 1st screen*/}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                  <SafeAreaView style={{flex: 1}}>

                    <View style={{width:'100%', alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                      <Text style={styles.headerText}>Welcome to</Text>
                      <Text style={styles.headerText}>NParks Coast-to-Coast App</Text>
                      <TouchableOpacity accessibilityLabel="close tutorial button" style={{position: 'absolute', right:10}} onPress={handleStart}>
                        <Icon name={'close-circle-outline'} size={32} color={"#1b5e20"}/>
                      </TouchableOpacity>
                    </View>

                    <Image style={{width: 200, height: 200}} alignSelf='center' source={require('@assets/img/phil_tutorial.png')} />

                    <View style={{alignSelf: 'center', margin: 20}}>
                      <HTML html={htmlContent}/>
                    </View>
                  </SafeAreaView>
                </ImageBackground>
              </View>
          </ScrollView>
          {/* 2nd Screen */}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                  <SafeAreaView style={{flex: 1}}>

                    <View style={{width:'100%', alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                      <Text style={styles.headerText}>At a Glance</Text>
                      <TouchableOpacity accessibilityLabel="close tutorial button" style={{position: 'absolute', right:10}} onPress={handleStart}>
                        <Icon name={'close-circle-outline'} size={32} color={"#1b5e20"}/>
                      </TouchableOpacity>
                    </View>

                    <View style={{alignSelf:'center', alignItems: 'center'}}>
                      <ImageZoom 
                        ref={setZoomRef2}
                        cropWidth={iWidth}
                        cropHeight={iHeight}
                        imageWidth={iWidth}
                        imageHeight={iHeight}
                        maxOverflow={10}
                        maxScale={2}
                        minScale={1}>
                        <Image style={{width:iWidth, height: iHeight}} source={require('@assets/img/tutorial_screen1.png')}/>
                      </ImageZoom>
                    </View>
                  </SafeAreaView>
                </ImageBackground>
              </View>
          </ScrollView>
          {/* 3rd Screen */}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                  <SafeAreaView style={{flex: 1}}>

                    <View style={{width: '100%', alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                      <Text style={styles.headerText}>At a Glance</Text>
                      <TouchableOpacity accessibilityLabel="close tutorial button" style={{position: 'absolute', right:10}} onPress={handleStart}>
                        <Icon name={'close-circle-outline'} size={32} color={"#1b5e20"}/>
                      </TouchableOpacity>
                    </View>

                    <View style={{alignSelf:'center', alignItems: 'center'}}>
                      <ImageZoom 
                        ref={setZoomRef3}
                        cropWidth={iWidth}
                        cropHeight={iHeight}
                        imageWidth={iWidth}
                        imageHeight={iHeight}
                        maxScale={2}
                        minScale={1}>
                        <Image style={{width:iWidth, height: iHeight}} source={require('@assets/img/tutorial_screen2.png')}/>
                      </ImageZoom>
                    </View>
                  </SafeAreaView>
                </ImageBackground>
              </View>
          </ScrollView>
          {/* 4th Screen */}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                <SafeAreaView style={{flex: 1}}>

                  <View style={{width:'100%', alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                    <Text style={styles.headerText}>Discover and Explore</Text>
                    <TouchableOpacity accessibilityLabel="close tutorial button" style={{position: 'absolute', right:10}} onPress={handleStart}>
                      <Icon name={'close-circle-outline'} size={32} color={"#1b5e20"}/>
                    </TouchableOpacity>
                  </View>

                  <View style={{alignSelf:'center', alignItems: 'center'}}>
                    <ImageZoom 
                      ref={setZoomRef4}
                      cropWidth={iWidth}
                      cropHeight={iHeight}
                      imageWidth={iWidth}
                      imageHeight={iHeight}
                      maxScale={2}
                      minScale={1}>
                      <Image style={{width:iWidth, height: iHeight}} source={require('@assets/img/tutorial_screen3.png')}/>
                    </ImageZoom>
                  </View>

                  <View style={{alignItems: 'center', alignSelf: 'center', margin: 20}}>
                    <Text style={{textAlign: 'center'}}>Tap to explore and learn about the sights and attractions within the C2C Trail, as well as park amenities and services</Text>
                  </View>
                </SafeAreaView>
              </ImageBackground>
            </View>
          </ScrollView>
          {/* 5th screen */}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                <SafeAreaView style={{flex: 1}}>

                  <View style={{width:'100%', alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                    <Text style={styles.headerText}>Navigation Made Easy</Text>
                    <TouchableOpacity accessibilityLabel="close tutorial button" style={{position: 'absolute', right:10}} onPress={handleStart}>
                      <Icon name={'close-circle-outline'} size={32} color={"#1b5e20"}/>
                    </TouchableOpacity>
                  </View>

                  <View style={{alignSelf:'center', alignItems: 'center'}}>
                    <ImageZoom 
                      ref={setZoomRef5}
                      cropWidth={iWidth}
                      cropHeight={iHeight}
                      imageWidth={iWidth}
                      imageHeight={iHeight}
                      maxScale={2}
                      minScale={1}>
                      <Image style={{width:iWidth, height: iHeight}} source={require('@assets/img/tutorial_screen4.png')}/>
                    </ImageZoom>
                  </View>

                  <View style={{alignItems: 'center', alignSelf: 'center', margin: 20}}>
                    <Text style={{textAlign: 'center'}}>Turn on your GPS and navigate seamlessly from your location to explore the Coast-to-Coast Trail, and locate park amenities and services</Text>
                  </View>
                </SafeAreaView>
              </ImageBackground>
            </View>
          </ScrollView>
          {/* last screen */}
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
                <SafeAreaView style={{flex: 1}}>
                  <View style={{alignSelf: 'center', flexDirection: 'column', alignItems: 'center', margin: 20}}>
                    <Text style={styles.headerText}>Personalize Your Profile</Text>
                  </View>

                  <View style={{alignSelf:'center', alignItems: 'center'}}>
                    <ImageZoom 
                      ref={setZoomRef6}
                      cropWidth={iWidth}
                      cropHeight={iHeight}
                      imageWidth={iWidth}
                      imageHeight={iHeight}
                      maxScale={2}
                      minScale={1}>
                      <Image style={{width:iWidth, height: iHeight}} source={require('@assets/img/tutorial_screen5.png')}/>
                    </ImageZoom>
                  </View>

                  <CustomButton style={{marginTop: 10}} onPress={()=>{handleStart()}}>Let's Go!</CustomButton> 
                </SafeAreaView>
              </ImageBackground>
            </View>
          </ScrollView>
          
      </IndicatorViewPager>
    </View>
    
  );  
}


navigationOptions = ({navigation}) => ({
  header: null
});

export default withScreenBase("main/tutorial", ScreenTutorial, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
  headerText:{
    fontSize: 16,
    fontWeight: 'bold'
  }
});
