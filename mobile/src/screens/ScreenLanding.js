import React, { useEffect, useState } from 'react';
import { ShareStyle } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, ImageBackground, Image, Linking, SafeAreaView } from 'react-native';
import CustomButton from '@components/CustomButton';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import FastImage from 'react-native-fast-image';
import Constants from '@helpers/Constants';
import {useNavigationParam} from 'react-navigation-hooks';

const ScreenLanding = (props) => {
  const { navigate } = useNavigation();
  const [ appVers, setAppVers] = useState("");
  const [ inEventMode, setInEventMode] = useState(false);
  const event_mode = useNavigationParam('event');

  useEffect(() => {
    async function getSettingsInfo(){
      setAppVers(await Settings.get(Settings.INSTALLED_APP_VERSION));
    }
    getSettingsInfo();
    if(event_mode == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }
    return function cleanup() {}
  }, []);

  const gotoLogin = () => { navigate('login', {page:"landing"})};
  const gotoSignUp = () => { navigate('signUp')};
 
  return (
    <View style={styles.viewHolder}>
      <ImageBackground style={styles.imageBg} source={require('@assets/img/mainBG.png')}>
        <SafeAreaView style={styles.safeAreaView}>
          <Text style={styles.appVersText}>Version {appVers}</Text>

          <Image style={styles.logo} alignSelf='center' source={require('@assets/img/c2c_logo.png')} />
          <Image style={styles.phil} alignSelf='center' alignItems='center' source={ inEventMode? require('@assets/img/phil_xmas.png') : require('@assets/img/phil_tutorial.png') } />

          <CustomButton onPress={gotoLogin}>Login</CustomButton>
          <CustomButton onPress={gotoSignUp}>Sign up</CustomButton>
          
          <Image style={styles.nparksLogo} resizeMode={'contain'} alignSelf='center' source={require('@assets/img/logo.png')} />

          <View style={styles.btmTextContainer}>
            <Text style={styles.txtPolicy} onPress={()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/privacy")}}>Privacy Policy</Text>
            <Text style={styles.txtTerms} onPress={()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+"/terms")}}>Terms of Use</Text>
          </View>
        </SafeAreaView>
      </ImageBackground>
    </View>
  );  
}

navigationOptions = ({navigation}) => ({
  header: null,
});

export default withScreenBase("main/landing", ScreenLanding, ScreenBaseType.LANDING, navigationOptions);

const styles = StyleSheet.create({
  viewHolder:{flex: 1, flexDirection: 'column', backgroundColor: "white"},
  imageBg: { flex: 1, flexDirection: 'column' },
  safeAreaView: { flex: 1 },
  appVersText: { alignSelf: 'flex-end', fontWeight: 'bold', marginRight: 10, marginTop: 10 },
  logo: {width: 120, height: 90, marginTop: 20},
  phil: {width: 200, height: 200, marginTop: 5, marginBottom: 10, alignSelf:'center', alignItems:'center'},
  nparksLogo: {width: 65, height: 65, position: 'absolute', bottom: 30},
  btmTextContainer: {flexDirection: 'column', width: '100%', position: 'absolute', bottom: 30},
  txtPolicy: {fontWeight: 'bold', marginLeft: 10},
  txtTerms: {fontWeight: 'bold', position:"absolute", alignSelf:'flex-end', right: 10}
});
