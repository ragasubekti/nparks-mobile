import React, { useEffect, useRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Dimensions, Text, View, StyleSheet, Image, TouchableWithoutFeedback, Platform, Linking, Animated, DeviceEventEmitter } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import { accelerometer, gyroscope, setUpdateIntervalForType, SensorTypes, magnetometer } from "react-native-sensors";
import { RNCamera } from 'react-native-camera';
import { NavigationEvents } from "react-navigation";
import useJourneyAction from '@hooks/useJourneyAction';
import Constants from '@helpers/Constants';
import Utils from '@helpers/Utils';
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {ImagePath} from '@assets/imagePaths';
import HiddenLocationView from '@components/HiddenLocationView';
import { Button } from 'react-native-elements';

const ScreenCameraHiddenFruit = (props) => {
  const { navigate } = useNavigation();
  const arRef = useRef(null);
  const screenWidth = Math.round(Dimensions.get('window').width);
  const screenHeight = Math.round(Dimensions.get('window').height);
  const data = useNavigationParam('data');
  const { doJourneyAction } = useJourneyAction();
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const [contestMode, SetContestMode] = useState(false);
  const hiddenObjectRef = useRef(null); 

  useEffect(() => {
    renderHiddenLocationView();
    if(arRef.current != null){
      arRef.current.initFromReact(data.thumbnail_base_url + "/" + data.thumbnail_path, data.status_rare.toString());
      
      if(DeviceEventEmitter.listeners('hiddenLocationEvent').length > 0){
        DeviceEventEmitter.removeAllListeners('hiddenLocationEvent');
      }
      DeviceEventEmitter.addListener('hiddenLocationEvent', onArEventCb);
    }

  }, []);

  useEffect(() => {
    if(data.object_name == Constants.TYPE_2019_XMAS_SANTA){
      hiddenObjectRef.current = 'Santa Claus';
    } else{
      if(contestMode){ hiddenObjectRef.current = 'Gingerbread Men'; }
      else { hiddenObjectRef.current = 'Hidden Fruit'; }
    }
    props.navigation.setParams({"navOptions":navOptions});
  }, [contestMode]);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
  }, [hiddenObjectRef.current]);

  const onArEventCb = async(e) => {
    // handle trigger once check on native(objc/java) files instead of js
      var type = data.status_rare == true? Constants.ACTION_HIDDEN_FRUIT_FOUND_RARE : Constants.ACTION_HIDDEN_FRUIT_FOUND;
      var param = data.hidden_location_id;
      if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
        type = Constants.ACTION_EVENT_XMAS_2019;
        param = data.object_name+"_"+('00' + parseInt(data.hidden_location_id, 10)).substr(-2);
      }
      
      Utils.showToast("Processing...");
      const res = await doJourneyAction(type, param + "");
      if(res){
        // console.log("success:",data.id);
        await Utils.recordHiddenLocationFound(data.id);
      }
  };

  renderHiddenLocationView = () =>{
    return(
      <HiddenLocationView ref={(ref)=>{arRef.current = ref}} style={styles.wrapper} onTouchHiddenFruit={()=>onArEventCb()} />
    );
  }

  renderTips = () =>{
    return(
      <Text style={styles.tip}>Move the camera around to find the {hiddenObjectRef.current}</Text>
    );
  }

  navOptions = {
    title: hiddenObjectRef.current,
    ...HeaderStyleWithBack
  }

  return (
    <View style={{flex: 1, backgroundColor: 'transparent'}}>
      <NavigationEvents
        onWillFocus={
          (payload) => {
            if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
              SetContestMode(true);
            }
            props.navigation.setParams({"navOptions":navOptions});
          }}
      />
      {renderHiddenLocationView()}
      <View style={{...styles.tipContainer, top: 0}}>
        {renderTips()}
      </View>
      <View style={{...styles.tipContainer, bottom: 0}}>
        <Text style={styles.tip}>* Please be aware of your surroundings. ** Parental supervision is required for safety of young children.</Text>
      </View>
      
    </View>
  )
  
}

export default withScreenBase("explore/hidden_fruit_camera", ScreenCameraHiddenFruit, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  container: { position:"absolute", backgroundColor:"transparent"},
  hiddenFruit: {width: 100, height: 100 },
  hiddenFruitRare: {width: 150, height: 150 },
  wrapper: { flex: 1 },
  tipContainer: {width: '100%', backgroundColor:"#ffffff90", position: 'absolute'},
  tip: {fontSize: 14, margin: 10, alignSelf: 'center', fontWeight: 'bold', textAlign: 'center'}
});
