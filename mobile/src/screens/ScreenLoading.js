import React, { useEffect, useState, useRef, useContext } from 'react';
import { View, Text, Platform, PermissionsAndroid, Alert, Image, ImageBackground, SafeAreaView, StyleSheet } from 'react-native';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {useNavigation} from 'react-navigation-hooks';
import { StyleConstant } from '@assets/MyStyle';
import firebase, { Notification, RemoteMessage } from 'react-native-firebase';
import Utils from '@helpers/Utils';
import Constants from '@helpers/Constants';
import DeviceInfo from 'react-native-device-info';
import WebApi from '@helpers/WebApi';
import NetInfo from "@react-native-community/netinfo";
import { withScreenBase } from '@screens/withScreenBase';
import {ImagePath} from '@assets/imagePaths';
import {request, PERMISSIONS} from 'react-native-permissions';
import Tracker from 'react-native-tracker';
import JailMonkey from 'jail-monkey';

const semver = require('semver');

const ScreenLoading = () => {
  const { navigate } = useNavigation();
  const { toggleActivityIndicator, showCustomDialog, showDropdownAlert } = useContext(GlobalContext);
  const [isConnected, setIsConnected] = useState(false);
  const [hasSetupFcm, setHasSetupFcm] = useState(false);
  const [eventMode, setEventMode] = useState('');

  useEffect(() => {
    Utils.log("ScreenLoading");
    checkInternetConnection();
    return function cleanup() {} 
  }, []);

  useEffect(() => {
    if(isConnected){
      toggleActivityIndicator(false);
      setup();
    }
    return function cleanup() {} 
  }, [isConnected]);

  setup = async() => {
    // toggleActivityIndicator(true)
    setupAnalytics();
    await Settings.init();
    await StoreSettings.init();
    getDeviceInfo();
    await setupFcm();
    if(await getServerStatus()){
      await checkIfLogin();
    } else {
      setTimeout(() => {()=>{navigate('landing', {event: eventMode})}}, 1000);
    }
  }

  checkIfLogin = async () => {
    StoreSettings.get(StoreSettings.IS_LOGGED_IN).then(async(isLoggedIn)=>{
      if (isLoggedIn == 'true' && await Utils.refreshUserProfile() && await Utils.refreshHiddenLocationToday()) {
        firebase.analytics().setUserId(await StoreSettings.get(StoreSettings.USER_ID));
        navigate('mainBottomTab');        
      } else {
        navigate('landing', {event: eventMode});
      }
    });
  }

  getDeviceInfo = async() => {
    Settings.store(Settings.DEVICE_ID, DeviceInfo.getUniqueId());
    Settings.store(Settings.DEVICE_PLATFORM, DeviceInfo.getSystemName());
    Settings.store(Settings.IS_EMULATOR, await DeviceInfo.isEmulator());
    Settings.store(Settings.INSTALLED_APP_VERSION, DeviceInfo.getVersion());

    let text = 'DeviceId: ' + Settings.get(Settings.DEVICE_ID) + "\n"
    + 'DevicePlatform: ' + Settings.get(Settings.DEVICE_PLATFORM) + "\n"
    + 'isEmulator: ' + Settings.get(Settings.IS_EMULATOR) + "\n"
    + 'InstalledAppVersion: ' + Settings.get(Settings.INSTALLED_APP_VERSION);

    Utils.log(text);
  }

  setupAnalytics = () =>{
    firebase.analytics().setAnalyticsCollectionEnabled(true);
    Tracker.start(Constants.STAGING_MODE ? "STAGING" : "PRODUCTION");
  }

  setupFcmChannel = () => {
    // Build a channel
    const channel = new firebase.notifications.Android.Channel(Constants.CHANNELID, 'NParks C2C Channel', firebase.notifications.Android.Importance.Max)
    .setDescription('NParks C2C Notification Channel');
    // Create the channel
    firebase.notifications().android.createChannel(channel);
  }

  setupFcm = async () => {
    setHasSetupFcm(true);
    if(!hasSetupFcm){
      try {
        const res = await firebase.messaging().requestPermission();
        const fcmToken = await firebase.messaging().getToken();
        setupFcmChannel();
        if (fcmToken) {
          Utils.log('FCM Token: ' + fcmToken);
          firebase.messaging().subscribeToTopic(Constants.FCM_TOPIC);
          StoreSettings.store(StoreSettings.FCM_TOKEN, fcmToken);
          const enabled = await firebase.messaging().hasPermission();
          if (enabled) {
            // Utils.log('FCM messaging has permission:' + enabled)
          } else {
            try {
              await firebase.messaging().requestPermission();
              // Utils.log('FCM permission granted')
            } catch (error) {
              Utils.log('FCM Permission Error', error);
            }
          }
          /*firebase.notifications().onNotificationDisplayed((notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
            Utils.log('Notification1: ', notification);
          });*/
          notificationListener = firebase.notifications().onNotification((notification) => {
            // notification fcm
            Utils.log(notification);
            Utils.showNotification(notification)
          });
          announcementListener = firebase.messaging().onMessage((announcement) => {
            // data fcm
            Utils.log(announcement);
            handleAnnouncement(announcement.data);
          });
        } else { 
          Utils.log('FCM Token not available');
        }
      } catch (e) {
        Utils.log('Error initializing FCM', e);
      }
    }
    
  }

  handleAnnouncement = async(data) =>{
    // dataKeys = Object.keys(data);
    // dataKeys.forEach((item)=>{
    //   Utils.log(item)
    // })
    var action_type = ("action_type" in data) ? data.action_type : "";
    Utils.log(action_type);
    switch (action_type) {
      case Constants.ACTION_TYPE_SYSTEM_ALERT:
        //show alert
        Alert.alert(
          data.title,
          data.body
        );
        break;

      case Constants.ACTION_TYPE_FORCE_LOGOUT:
        //force logout without an alert
        logout();
        break;

      case Constants.ACTION_TYPE_FORCE_LOGOUT_WITH_ALERT:
        //force logout with an alert
        Alert.alert(
          data.title,
          data.body,
          [
            {text: 'OK', onPress: ()=>{logout()}},
          ]
        );
        break;

      case Constants.ACTION_TYPE_DAILY_RESYNC:
        // DoDailyRefresh
        await resetDailyStoreSettings();
        await syncServerSettings();
        await Utils.refreshHiddenLocationToday();
        break;

      case Constants.ACTION_TYPE_CUSTOM_DIALOG:
        // show custom dialog
        showCustomDialog({title:data.title, body:data.body, okButtonText:"OK", imagePath:data.banner_url, isImagePathUrl:true, hyperlink:data.hyperlink_url, hasCancelButton:true});
        break;

      case Constants.ACTION_TYPE_CUSTOM_ALERT:
        // show custom alert
        showDropdownAlert(data.title, data.body);
        break;

      default:
        break;
    }
  }

  resetDailyStoreSettings = async() =>{
    await StoreSettings.store(StoreSettings.HAD_VISITED_C2C_TODAY, 'false');
    await StoreSettings.store(StoreSettings.HIDDEN_LOCATION_TODAY_FOUND_LIST, '');
    Settings.store(Settings.HAD_PROMPT_C2C_MSG, false);
    Settings.store(Settings.HAD_PROMPT_WELCOME_MSG, false);
    Settings.store(Settings.CURRENT_NEAR_HIDDEN_LOCATION, "");
    Settings.store(Settings.HAD_CHECK_SERVER_SYNC, false);
  }

  syncServerSettings = async() =>{
    // get server settings and save it into ephemeral settings
    var res = await WebApi.listServerSettingEncypt();
    //catch high traffic
    //TODO:: check to retry dialog
    // Utils.log(res)
    if (res.status == WebApi.STATUS_OK) {
      //if != STATUS_OK also return false?
      let data = res.data;
      if(data.length == 0){
        return false;
      }
      data.forEach(element => {
        Settings.store(element.keyword, element.value);
      });
      setEventMode(data[14].value);
      const s1 = await StoreSettings.get(StoreSettings.LAST_DAILY_RESYNC);
      const s2 = await Settings.get(Settings.APP_LAST_SYNC);
      if(s1 != s2){
        await resetDailyStoreSettings();
      }
      StoreSettings.store(StoreSettings.LAST_DAILY_RESYNC, s2);
    } else {
      return false;
    }
    return true;
  }

  subscribeInternetConnectionListener = async() =>{
    NetInfo.addEventListener(state => {
      Utils.log("isconnected: " + state.isConnected)
      setIsConnected(state.isConnected)
      if(!state.isConnected){        
        toggleActivityIndicator(true, "Please connect to the internet");
      }else {
        toggleActivityIndicator(false);
      }
    });
  }

  checkInternetConnection = async() =>{
    NetInfo.fetch().then((state) => {
      subscribeInternetConnectionListener();
      if(state.isConnected){
        setup();
      }
    });
  }

  getServerStatus = async() =>{

    // HadCheckServerSync
    if(!Settings.get(Settings.HAD_CHECK_SERVER_SYNC)){
      Settings.store(Settings.HAD_CHECK_SERVER_SYNC, true);
      
      if(await syncServerSettings() == false){
        showCustomDialog({title:"System Message", body:"Server is experiencing high traffic.\nPlease wait for 5 minutes and try again."});
        return false;
      }
      
      // toggleActivityIndicator(false)

      // checkRootedOrSimulator
      if (await checkRootedOrSimulator() == true) {
        //TODO:: check color for disabled button
        return false;                    
      }

      // checkMaintenance
      if (await checkMaintenance() == true) {
        // return false;
      }
      
      // checkAppVersionObsolete
      if (await checkAppVersionObsolete() == true) {
        return false;
      }  

      // checkPopupMessage
      if (await checkPopupMessage() == true) {
        return false;
      }
    } else {
      // toggleActivityIndicator(false)
      return false;
    }
    return true;

  }

  checkRootedOrSimulator = async() =>{
    // console.log("Settings.get(Settings.DEBUG_MODE)", Settings.get(Settings.DEBUG_MODE))
    // console.log("await JailMonkey.isDebuggedMode()", await JailMonkey.isDebuggedMode())
    // console.log("Settings.get(Settings.IS_EMULATOR)", Settings.get(Settings.IS_EMULATOR))
    // console.log("JailMonkey.trustFall()", JailMonkey.trustFall())
    // console.log("JailMonkey.AdbEnabled()", JailMonkey.AdbEnabled())
    // console.log("JailMonkey.hookDetected()", JailMonkey.hookDetected())
    if (!(Settings.get(Settings.DEBUG_MODE) == Constants.STATUS_ENABLED) && !(await JailMonkey.isDebuggedMode())) { // not dev mode
      var failed = (Settings.get(Settings.IS_EMULATOR) || JailMonkey.isJailBroken());
      if (failed) {
        await StoreSettings.reset();
        Settings.reset();
        showCustomDialog({title:"System Message", body:"We've detected irregularities in your device. Access will be limited."});
        return true;
      }
    }
    return false;
  }
  
    //ref:
    //https://github.com/npm/node-semver#readme
  checkAppVersionObsolete = async () => {
    var current_ver = Settings.get(Settings.INSTALLED_APP_VERSION);
    var latest_ver;
    if(Platform.OS === 'ios'){
      latest_ver = Settings.get(Settings.APP_VERSION_IOS) // for iOS
    } else {
      latest_ver = Settings.get(Settings.APP_VERSION_ANDROID)// for Android
    }
    let valid = semver.valid(current_ver) && semver.valid(latest_ver)
    if (valid) {
      let outdated = semver.lt(current_ver, latest_ver);      
      if (outdated) {
        let diff = semver.diff(current_ver, latest_ver);  
        var body = "A new version for the Coast To Coast Mobile App is available. Please update to version " + latest_ver;
        if(diff == "patch"){
          showCustomDialog({title:"Update Available", body:body, okButtonText:"OK", okCallback:{checkIfLogin}});
        } else {
          await StoreSettings.reset();
          Settings.reset();
          showCustomDialog({title:"Update Available", body:body});
        }
        return true;
      }
    }
    return false;    
  }
  /*checkAppVersionObsolete = async() =>{
    let SERVER_APP_VERSION;
    if(Platform.OS === 'ios'){
      SERVER_APP_VERSION = Settings.get(Settings.APP_VERSION_IOS) // for iOS
    } else {
      SERVER_APP_VERSION = Settings.get(Settings.APP_VERSION_ANDROID)// for Android
    }
    ServerVersionInteger = SERVER_APP_VERSION.split('.');
    let ServerFirstInteger, ServerSecondInteger, ServerThirdInteger;
    ServerFirstInteger = parseInt(ServerVersionInteger[0]);
    ServerSecondInteger = parseInt(ServerVersionInteger[1]);
    ServerThirdInteger = parseInt(ServerVersionInteger[2]);

    InstalledVersionInteger = Settings.get(Settings.INSTALLED_APP_VERSION).split('.');
    let InstalledFirstInteger, InstalledSecondInteger, InstalledThirdInteger;
    InstalledFirstInteger = parseInt(InstalledVersionInteger[0]);
    InstalledSecondInteger = parseInt(InstalledVersionInteger[1]);
    InstalledThirdInteger = parseInt(InstalledVersionInteger[2]);

    // //check 1st int
    // if (ServerFirstInteger > InstalledFirstInteger) {
    //   // server 1st int higher than installed 1st int
    //   await StoreSettings.reset();
    //   Settings.reset();
    //   let body = "A new version for the Coast To Coast Mobile App is available. Please update to version " + SERVER_APP_VERSION;
    //   showCustomDialog({title:"Update Available", body:body});
    //   return true;
    // }

    // //check 2nd int
    // if (ServerSecondInteger > InstalledSecondInteger) {
    //   // server 2nd int higher than installed 2nd int
    //   if (ServerFirstInteger < InstalledFirstInteger) {
    //     // 1.1.1 to 2.0.0 without updating the SysSettings table first
    //     return false;
    //   }
    //   await StoreSettings.reset();
    //   Settings.reset();
    //   let body = "A new version for the Coast To Coast Mobile App is available. Please update to version " + SERVER_APP_VERSION;
    //   showCustomDialog({title:"Update Available", body:body});
    //   return true;
    // }

    // //check 3rd int
    // if (ServerThirdInteger > InstalledThirdInteger) {
    //   // server 3rd int higher than installed 3rd int
    //   if (ServerSecondInteger < InstalledSecondInteger || ServerFirstInteger < InstalledFirstInteger) {
    //     // 1.0.6 to 1.1.0 or 1.0.6 to 2.0.0 without updating the SysSettings table first
    //     return false;
    //   }
    //   let body = "A new version for the Coast To Coast Mobile App is available. Please update to version " + SERVER_APP_VERSION;
    //   showCustomDialog({title:"Update Available", body:body, okButtonText:"OK", okCallback:{checkIfLogin}});
    //   return true;
    // }
    return false;
  }*/

  checkMaintenance = async() =>{
    var underMaintenance = Settings.get(Settings.SYSTEM_UNDER_MAINTENANCE_STATUS);
    if (underMaintenance == Constants.STATUS_ENABLED) {
      let body = Settings.get(Settings.SYSTEM_UNDER_MAINTENANCE_MESSAGE);
      showCustomDialog({title:"System Message", body:body});
      await StoreSettings.reset();
      Settings.reset();
      return true;
    }
    return false;
  }

  checkPopupMessage = async() =>{
    var showPopUp = Settings.get(Settings.APP_POPUP_STATUS);
    if (showPopUp == Constants.STATUS_ENABLED) {
      var res = await WebApi.getPopupBanner();
      if (res.status == WebApi.STATUS_OK) {
        let body = Settings.get(Settings.APP_POPUP_MESSAGE);
        let url = Settings.get(Settings.APP_POPUP_HAS_THUMBNAIL) == Constants.STATUS_ENABLED ? res.data.banner_url : null ;
        let href = Settings.get(Settings.APP_POPUP_HAS_LINK) == Constants.STATUS_ENABLED ? Settings.get(Settings.APP_POPUP_LINK) : null;
        let customButtonColor = eventMode == Constants.MODE_2020_XMAS? StyleConstant.xmasRed : '#1b5e20';
        showCustomDialog({title:"Announcement", body:body, okButtonText:"OK", okCallback:{checkIfLogin}, imagePath:url, isImagePathUrl:true, hyperlink:href, customButtonColor:customButtonColor });
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  logout = async() => {
    //call logout api
    await StoreSettings.store(StoreSettings.IS_LOGGED_IN, 'false');
    WebApi.logout()
    .then(async(res) =>{
      await StoreSettings.reset();
      Settings.reset();
    })
    .catch((err) =>{Utils.error(err)})
    .finally(()=>{navigate('loading')})
  }

  return (
    <View style={styles.viewHolder}>
      <ImageBackground style={styles.imageBg} source={require('@assets/img/mainBG.png')}>
        <SafeAreaView style={styles.safeAreaView}>

          <Image style={styles.logo} alignSelf='center' source={require('@assets/img/c2c_logo.png')} />
          <Image style={styles.phil} alignSelf='center' alignItems='center' source={require('@assets/img/phil_tutorial.png')} />
          
          <Image style={styles.nparksLogo} resizeMode={'contain'} alignSelf='center' source={require('@assets/img/logo.png')} />
          
          <Text style={{alignSelf: 'center', fontWeight: 'bold'}}>Loading...</Text>
        </SafeAreaView>
      </ImageBackground>
    </View>
  );
}

export default withScreenBase("main", ScreenLoading);

const styles = StyleSheet.create({
  viewHolder:{flex: 1, flexDirection: 'column', backgroundColor: "white"},
  imageBg: { flex: 1, flexDirection: 'column' },
  safeAreaView: { flex: 1 },
  appVersText: { alignSelf: 'flex-end', fontWeight: 'bold', marginRight: 10, marginTop: 10 },
  logo: {width: 120, height: 90, marginTop: 20},
  phil: {width: 200, height: 200, marginTop: 5, marginBottom: 10, alignSelf:'center', alignItems:'center'},
  nparksLogo: {width: 65, height: 65, position: 'absolute', bottom: 30},
  btmTextContainer: {flexDirection: 'column', width: '100%', position: 'absolute', bottom: 30},
  txtPolicy: {fontWeight: 'bold', marginLeft: 10},
  txtTerms: {fontWeight: 'bold', position:"absolute", alignSelf:'flex-end', right: 10}
});