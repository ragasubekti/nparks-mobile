import React, { useEffect, userRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithBack, HeaderStyleWithBackIos, StyleConstant } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, TouchableOpacity, Linking, ImageBackground, Alert, Platform, BackHandler } from 'react-native';
import CustomButton from '@components/CustomButton';
import { CustomTextInput, CustomTextInputType } from '@components/CustomTextInput';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import Utils from '@helpers/Utils';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Constants from '@helpers/Constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import { NavigationEvents } from 'react-navigation';

const ScreenLogin = () => {
  const prevScreen = useNavigationParam('page');
  const { navigate } = useNavigation();
  const [email, setEmail] = useState('');
  const [firstRun, setFirstRun] = useState('');
  const [password, setPassword] = useState('');
  const [ inEventMode, setInEventMode] = useState(false);
  const [emailNotVerified, setEmailNotVerified] = useState(false);
  let authCode = '';
  const { toggleActivityIndicator, showCustomDialog } = useContext(GlobalContext);

  useEffect(() => {
    if(prevScreen == "signup"){
      handleStart = handleStart.bind(this);
      BackHandler.addEventListener('hardwareBackPress', handleStart);
    };
    getPresetEmail();
    getFirstRun();

    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }
  }, []);

  handleStart = ()=>{
    BackHandler.removeEventListener('hardwareBackPress', handleStart);
    navigate('landing');
    return true;
  }

  const getPresetEmail = async() => {
    const email = await StoreSettings.get(StoreSettings.USER_EMAIL);
    setEmail(email);
  }

  const getFirstRun = async()=> {
    const value = await StoreSettings.get(StoreSettings.FIRST_RUN);
    setFirstRun(value);
  }

  const handleLogin = async () => {
    setEmail(e => e.trim());
    if (email.trim() == '') {
      Utils.showToast("Please enter your email");
      return;
    } else if (password == '') {
      Utils.showToast("Please enter your password");
      return;
    } else if (!Utils.isEmail(email.trim())) {
      Utils.showToast("Email is not valid");
      return;
    } 
    await StoreSettings.store(StoreSettings.USER_EMAIL, email.trim());
    toggleActivityIndicator(true, "Logging in...")
    const success = await doLogin();
    if (success) {
      StoreSettings.store(StoreSettings.IS_LOGGED_IN, 'true').then(async()=>{        
        await firebase.analytics().setUserId(await StoreSettings.get(StoreSettings.USER_ID));
        if(firstRun == "true"){
          toggleActivityIndicator(false);
          await StoreSettings.store(StoreSettings.FIRST_RUN, 'false')
          navigate('tutorial');
        } else {
          toggleActivityIndicator(false);
          navigate('mainBottomTab');
        }
      }).catch((e)=>{
        Utils.log(e);
      }); 
    }
  };

  const doLogin = async () => {
    try {
      const res1 = await doAuthorise(email.trim(), password);
      if (!res1) { return false; }
      const res2 = await doAccessToken(authCode);
      if (!res2) { return false; }
      const res3 = await doRegisterFcmToken();
      if (!res3) { return false; }
      const res4 = await Utils.refreshUserProfile();
      if (!res4) { return false; }
      const res5 = await Utils.refreshHiddenLocationToday();
      if (!res5) { return false; }      
      
      return true;
    } catch(e) {
      toggleActivityIndicator(false);
      Utils.log(e)
      Utils.showToast("Unexpected error occured");
    }
  }

  const doAuthorise = async(email, password) => {
    let res = await WebApi.authorise(email, password);
    let success = false;
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      const err = res.data[0].message
      toggleActivityIndicator(false);
      Utils.showToast(err);
    } else if (res.status == WebApi.STATUS_OK) {
      authCode = res.data.authorization_code;
      success = true;
    } else {
      const err = res.data.message;
      if(res.data.message_key == Constants.EMAIL_NOT_VERIFIED){
        setEmailNotVerified(true);
        Alert.alert(
          "Verify Email",
          err,
          [
            {text: 'OK', onPress: ()=>{toggleActivityIndicator(false)}},
          ]
        );
      } else if (res.data.message_key == Constants.ACCOUNT_SUSPENDED || res.data.message_key == Constants.LOGIN_LIMIT_ACCOUNT_SUSPENDED){
        Alert.alert(
          "System Message",
          err,
          [
            {text: 'OK', onPress: ()=>{toggleActivityIndicator(false)}},
          ]
        );
      } else {
        toggleActivityIndicator(false);
        Utils.showToast(err);
      }
    }
    return success;
  }
  const doAccessToken = async(code) => {
    let res = await WebApi.accessToken(code);
    let success = false;
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      const err = res.data[0].message
      toggleActivityIndicator(false);
      Utils.showToast(err);
    } else if (res.status == WebApi.STATUS_OK) {
      await StoreSettings.store(StoreSettings.ACCESS_TOKEN, res.data.access_token);
      success = true;
    } else {
      const err = res.data.message
      toggleActivityIndicator(false);
      Utils.showToast(err);
    }
    return success;
  }
  const doRegisterFcmToken = async() => {
    let res = await WebApi.registerFcmToken();
    let success = false;
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      const err = res.data[0].message
      toggleActivityIndicator(false);
      Utils.showToast(err);
    } else if (res.status == WebApi.STATUS_OK) {
      success = true;
    } else {
      const err = res.data.message
      toggleActivityIndicator(false);
      Utils.showToast(err);
    }
    return success;
  }

  resendEmail = async()=> {
    let res = await WebApi.resendVerifyEmail(email.trim());
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      const err = res.data[0].message
      Utils.showToast(err);
    } else if (res.status == WebApi.STATUS_OK) {
      Utils.showToast("New verification email sent.");
    } else {
      const err = res.data.message
      Utils.showToast(err);
    } 
  }

  resendLogic = ()=> {
    if(prevScreen == "signup" || emailNotVerified){
      return(
        <CustomButton onPress={resendEmail}>Resend Verification Email</CustomButton>
      );
    }
  }

  return (
    <KeyboardAvoidingView style={styles.flex1}>
      <View style={styles.viewHolder}>
        <NavigationEvents
          onDidFocus={()=>{
            if(prevScreen == "signup"){
              Alert.alert(
                'System Message',
                'Before you try to login to the app, please go to your email and verify it.',
                [
                  {text: 'OK', onPress: ()=>{Utils.log('ok')}}
                ]
              )
            }
          }}
          />
          <ImageBackground style={styles.flex1} source={inEventMode? require('@assets/img/bg_xmas.png') : require('@assets/img/bg.png')}>
            <View style={{marginTop: 30}}>
              <CustomTextInput placeholder='Email' value={email} setValue={setEmail}/>
              <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Password' value={password} setValue={setPassword}/>
            </View>

            <TouchableOpacity style={styles.touchableContainer} onPress={()=>{Linking.openURL(Constants.FRONTEND_SERVER_URL+'/user/request-password-reset')}}>
              <Text style={styles.forgotPw}>Forgot Password?</Text>
            </TouchableOpacity>

            {resendLogic()}
            <CustomButton onPress={handleLogin}>Login</CustomButton>
          </ImageBackground>

      </View>
    </KeyboardAvoidingView>
  );  
}

navigationOptions = ({navigation}) => ({
  title: 'Login',
  headerLeft: (
    <TouchableOpacity style={{marginLeft: 10}} activeOpacity={0.5} onPress={() => navigation.navigate("landing")}>
      <Icon name={'arrow-back'} size={25} color={"white"}/>
    </TouchableOpacity>
  ),
  ...HeaderStyleWithBack
});

export default withScreenBase("main/login", ScreenLogin, ScreenBaseType.LANDING, navigationOptions);
//export default ScreenLogin;

const styles = StyleSheet.create({
  flex1: {flex: 1},
  viewHolder: {flex: 1, flexDirection: 'column'},
  touchableContainer: {marginTop: 20, marginBottom: 20},
  forgotPw: {alignSelf: 'center', color: '#000000', fontWeight: 'bold'},
});
