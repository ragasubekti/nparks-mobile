import React, { useEffect, useState, useRef } from 'react';
import { ShareStyle, HeaderStyleWithRight } from '@assets/MyStyle';
import { View, FlatList, StyleSheet, TouchableHighlight, TouchableOpacity, Alert, Button } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';
import { Text } from 'react-native-paper';
import CustomFlatList from '@components/CustomFlatList3';
import ModelUserNotification from '@models/ModelUserNotification';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import ModelUser from "@models/ModelUser";
import WebApi from '@helpers/WebApi';
import CustomButton from '@components/CustomButton';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationEvents } from 'react-navigation';
import Constants from '@helpers/Constants';
import Utils from '@helpers/Utils';

const ScreenNotification = (props) => {
  const { navigationOptions } = useNavigation();
  const { navigate } = useNavigation();
  const dataUrl = Constants.API_SERVER_URL + '/user/list-my-inbox';
  const r = ModelUserNotification.renderUserNotification(navigate, 'notificationDetail');
  const { toggleRefresh, renderCustomFlatList } = CustomFlatList(dataUrl, r, Constants.LAYOUT_OFFSET);
  const [hasChanges, setHasChanges] = useState(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
  
  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
    return function cleanup() {}
  }, []);
  

  const deleteAlert = ()=> {
    Alert.alert(
      'Delete all notifications?',
      'Are you sure you want to delete all notifications?',
      [
        {text: 'NO', onPress: ()=> {Utils.log('no')}},
        {text: 'YES', onPress: ()=>{async function onYes(){
          await deleteAllNotification();
        }
        onYes(); }},
      ]
    );
  }

  const deleteAllNotification = async() => {
    let res = await WebApi.deleteAllNotification();
    // Utils.log(res.status);
    let success = false;
      if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
        const err = res.data[0].message
        Utils.showToast(err);
      } else if (res.status == WebApi.STATUS_FORBIDDEN) {
        const err = res.data.message
        Utils.showToast(err);
      } else if (res.status == WebApi.STATUS_OK) {
        let data = (res.data);
        Utils.log(data);
        success = true;
        Utils.showToast("Deleted ALL");
        navigate("me");
      }
      return success;
  }
  navOptions = {
    title: 'Inbox',
    headerRight: (
      <TouchableOpacity accessibilityLabel="delete all messages button" style={{marginRight: 10}} activeOpacity={0.5} onPress={deleteAlert}>
        <Icon name="trash-can-outline" size={20} color={"white"}/>
      </TouchableOpacity>
    ),
    ...HeaderStyleWithRight
  };

  return (
    <View style={styles.container}>    
      <NavigationEvents
        onDidFocus={()=>{async function checkChanges(){
          if(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA)){
            setHasChanges(!hasChanges);
            toggleRefresh();
            Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
          }
        }
        checkChanges();}}
      />
      {renderCustomFlatList()}
    </View>
  );

}

export default withScreenBase("me/notification", ScreenNotification, ScreenBaseType.MAIN);


const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: 'white'
  },
  holder1:{
    height: 250,
    justifyContent: 'center',
  },
  holder2:{
    flex: 1,
    height: 1000,
    flexGrow: 1,
  },
});