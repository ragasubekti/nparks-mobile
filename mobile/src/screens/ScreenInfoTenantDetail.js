import React, { useEffect, useRef, useState, useContext } from 'react';
import { View, Text, StyleSheet, Button as Btn, ImageBackground, Image, Linking, ScrollView, Platform, TouchableOpacity, Clipboard } from 'react-native';
import { List, ListItem } from "react-native-elements";
import CustomButton from '@components/CustomButton';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import { ShareStyle, StyleConstant, HeaderStyleWithBack } from '@assets/MyStyle';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import {WebView} from 'react-native-webview';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import useMapboxLocation from '@hooks/useMapboxLocation';
import Utils from '@helpers/Utils';

const ScreenInfoTenantDetail = () => { 
  const item = useNavigationParam('item');
  const { renderMapStuff, prepareMarker } = useMapboxLocation();
  const [hasEmail, setHasEmail] = useState(false);

  useEffect(() => {
    if(item.email != "" && item.email != null){ setHasEmail(true); }
    prepareMarker([item.longitude, item.latitude]);
    return function cleanup() { }
  }, []);

  writeToClipboard = async () => {
    await Clipboard.setString(item.email);
    Utils.showToast('Copied to Clipboard!');
  };

  return (
    <ScrollView bounces={false} style={{flex: 1, backgroundColor: "white"}}>
      <View style={{backgroundColor: "white", flex: 1}}>
        <View style={styles.cardHolder}>
          <ImageBackground
            resizeMode={'cover'}
            style={{flex:1}}
            source={{uri: item.thumbnail_base_url + '/' + item.thumbnail_path}}
          >
          <View justifyContent='center' style={{width: "100%", height: 55, position: "absolute", bottom: 0, backgroundColor:"rgba(153,102,68,0.6)"}}>

            <Text style={{color:"white", marginLeft: 15, marginRight: 15, fontSize: 14, fontWeight: "bold"}}>{item.name}</Text>
            <View style={{flexDirection:"row", marginLeft: 15, marginRight: 15, marginTop: 2}}>
              <Icon style={{alignSelf: 'center'}} name="map-marker" size={13} color={"white"}/>
              <Text style={{marginLeft: 5, alignSelf: 'center', fontSize: 12, color: "white"}}>{item.location_name}</Text>
            </View>

          </View>
          </ImageBackground>
        </View>

        <Text style={{marginTop: 20, marginLeft: 15, marginRight: 15, fontSize: 12, color: "black"}}>{item.description}</Text>
        <Text style={{marginTop: 15, left: 15, right: 15, fontSize: 12, color: "#cccccc", fontWeight: "bold"}}>Contact Details</Text>
        <View style={styles.txtHolder}>
          <Icon style={{alignSelf: 'center'}} name="map-marker" size={13} color={"gray"}/>
          <Text style={{flex: 1 , flexWrap: 'wrap', fontSize: 12, color: "#cccccc", marginLeft: 10, marginRight: 10}}>{item.address !=""? item.address : "-"}</Text>
        </View>
        <View style={styles.txtHolder}>
          <Icon style={{alignSelf: 'center'}} name="web" size={13} color={"gray"}/>
          <TouchableOpacity activeOpacity={0.5} onPress= {()=> Linking.openURL(item.website)}>
            <Text style={{flex: 1 , flexWrap: 'wrap', left: 10, right: 15, fontSize: 12, color: "#1b5e20"}}>{item.website != ""? item.website : "-"}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.txtHolder}>
          <Icon style={{alignSelf: 'center'}} name="phone" size={13} color={"gray"}/>
          <Text style={{left: 10, right: 15, fontSize: 12, color: "#1b5e20"}} onPress={ () => Linking.openURL('tel:${' + item.contact + '}')}>{item.contact != ""? item.contact : "-"}</Text>
        </View>
        <View style={styles.txtHolder}>
          <Icon style={{alignSelf: 'center'}} name="email" size={13} color={"gray"}/>
          <TouchableOpacity activeOpacity={0.5} onPress= {this.writeToClipboard}>
            <Text style={{left: 10, right: 15, fontSize: 12, color: "#cccccc"}}>{hasEmail? item.email : "-"}</Text>
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 20, height: 300, backgroundColor: StyleConstant.bgGray}}>
          {renderMapStuff()}
        </View>
      </View>
    </ScrollView>
  );

}

navigationOptions = ({navigation}) => ({
  title: 'Services',
  ...HeaderStyleWithBack
});

export default withScreenBase("Information/tenant/detail", ScreenInfoTenantDetail, ScreenBaseType.MAIN, navigationOptions);

var styles = StyleSheet.create({
  holder:{
    marginTop: 50,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
  cardHolder:{
    maxWidth: "100%", 
    height: 175, 
    backgroundColor: "white"
  },
  txtHolder: {flex: 1, flexDirection:"row", marginTop:15, marginLeft: 15, marginRight: 15},
});
