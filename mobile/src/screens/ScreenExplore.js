import React, { useEffect, useRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyle, HeaderStyleWithRight, StyleConstant, HeaderStyleWithBack, ShadowStyle, HeaderStyleWithRightOnly } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Button, TouchableOpacity, NativeModules, Image, Linking, Platform } from 'react-native';
import { withScreenBase, ScreenBaseContext, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import { NavigationEvents } from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import useMapbox from '@hooks/useMapbox';
import useLocationLogic from '@hooks/useLocationLogic';
import useDebug from '@hooks/useDebug';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import WebApi from '@helpers/WebApi';
import Constants from '@helpers/Constants';
import {ImagePath} from '@assets/imagePaths';
import useJourneyAction from '@hooks/useJourneyAction';
// import useToggleDialog from '@hooks/useToggleDialog';
import Utils from '@helpers/Utils';
import firebase from 'react-native-firebase';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';

const ScreenExplore = (props) => {
  const shouldResetCurrentNearHiddenLocation = useNavigationParam('resetCurrentNearHiddenLocation');
  const { navigate } = useNavigation();
  const { renderMapStuff, myLocation, handleOnPressMyLocation, handleNavigateToDetail, selectedItem, touchedData, followMap, undrawPolyLine, showPhotos, setShowPhotos } = useMapbox();
  const [isInMap, setIsInMap] = useState(false);
  const [navInstructions, setNavInstructions] = useState(false);
  const [contestMode, SetContestMode] = useState(false);
  const { withinC2cBorder, currentNearHiddenLocation, setCurrentNearHiddenLocation } = useLocationLogic(myLocation, isInMap);
  const { showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const { renderDebugMenu, renderDebugIcon } = useDebug(false);
  //const { showToggleDialog, renderToggleDialog, amenitiesOn } = useToggleDialog();
  const { doJourneyAction } = useJourneyAction();
  const itemRef = useRef(null);
  const photoRef = useRef(true);
  const hiddenObjectRef = useRef(null);

  useEffect(() => {
    //props.navigation.setParams({"navOptions":navOptions});
    // firebase.analytics().logEvent('foo', { bar: '123'});
    return function cleanup() { }
  }, []);

  useEffect(()=>{
    if(selectedItem != itemRef.current){
      itemRef.current = selectedItem;
      setNavInstructions(false);
      undrawPolyLine();
    }
    if(!selectedItem){
      setNavInstructions(false);
    }
  },[selectedItem]);

  useEffect(() => {
    //Loynote: need to watch withinC2cBorder & set nav option for show/hide camera button
    props.navigation.setParams({"navOptions":navOptions});
    return function cleanup() { }
  }, [withinC2cBorder]);

  useEffect(() => {
    if(currentNearHiddenLocation != null){
      if(currentNearHiddenLocation.object_name == Constants.TYPE_2019_XMAS_SANTA){
        hiddenObjectRef.current = 'santa';
      } else{
        if(contestMode){ hiddenObjectRef.current = 'gingerbread men'; }
        else{ hiddenObjectRef.current = 'fruit'; }
      }
    }
  }, [currentNearHiddenLocation]);
  
  useEffect(() => {
    //Loynote: need to watch Settings.DEBUG_MODE & set nav option for show/hide camera button
    props.navigation.setParams({"navOptions":navOptions});
    return function cleanup() { }
  }, [Settings.get(Settings.DEBUG_MODE)]);

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});
  }, [showPhotos]);

  handleNavigate = () =>{
    withinC2cBorder? handleNavigateToButton() : openOnemapWeb();
  }

  handleToggle = () =>{
    photoRef.current = !photoRef.current;
    setShowPhotos(photoRef.current);
  }

  detailPoiTenant = (e) =>{
    if(selectedItem){
      let touchedType = selectedItem.properties.featureType;
      if(touchedType == "poi"){ navigate('explorePoiDetail', {item:touchedData}); }
      else{ navigate('exploreTenantDetail', {item:touchedData}); }
    }
  }

  handleNavigateToButton = ()=>{
    setNavInstructions(true);
    handleNavigateToDetail();
  }

  openOnemapWeb = async()=>{
    // let lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
    // let long = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);
    let lat = myLocation.coords.latitude;
    let long = myLocation.coords.longitude;
    let uri = Constants.FRONTEND_SERVER_URL + '/redirect-one-map?startX=' + long + '&startY=' + lat + '&endX=' + selectedItem.geometry.coordinates[0] + '&endY=' + selectedItem.geometry.coordinates[1];
    Linking.openURL(uri);
  }

  showFruitDialog =() =>{
    var url = currentNearHiddenLocation.thumbnail_base_url + "/" + currentNearHiddenLocation.thumbnail_path;
    // console.log(currentNearHiddenLocation);
    if(contestMode){
      var body = "Find it and earn Flower Points!";
      var title = "Psst!\nA Gingerbread Man is nearby!";
      if(currentNearHiddenLocation.object_name == Constants.TYPE_2019_XMAS_SANTA){
        body = "Find him and win the grand prize,\na 13-inch MacBook Pro!";
        title = "HO! HO! HO!\nWHERE AM I ?";
      }
      showCustomDialog({title:title, body:body, okButtonText:"Let's Go!", imagePath:url, okCallback:{checkForPermission}, hasCancelButton:true, isImagePathUrl:true, customButtonColor:StyleConstant.xmasRed, santa: true});
    } else {
      let body = currentNearHiddenLocation.status_rare == "true" ? "There's hidden rare fruit nearby.\nLook for them to earn flowers!" : "There's hidden fruit nearby.\nLook for them to earn flowers!";
      showCustomDialog({title:"Psssst!", body:body, okButtonText:"Let's Go!", imagePath:url, okCallback:{checkForPermission}, hasCancelButton:true, isImagePathUrl:true});
    }
  }

  checkForPermission = async() =>{
    let result;
    if (Platform.OS === "android") {
        let cameraResult = await check(PERMISSIONS.ANDROID.CAMERA);
        result = cameraResult == RESULTS.GRANTED;
    } else {
        let camera = await check(PERMISSIONS.IOS.CAMERA);
        let motion = await check(PERMISSIONS.IOS.MOTION);
        if(camera == RESULTS.BLOCKED || motion == RESULTS.BLOCKED){
        openAppSettings=()=>{
            Linking.openURL('app-settings:');
        }
        showCustomDialog({body:"Coast-to-Coast would like access to your Camera to find the hidden " + hiddenObjectRef.current, okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{openAppSettings}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
        return;
        }
        result = camera == RESULTS.GRANTED && motion == RESULTS.GRANTED;
    }

    if(result){
        hiddenFruitDialogOnOkAction();
    } else {
        showCustomDialog({body:"Coast-to-Coast would like access to your Camera to find the hidden " + hiddenObjectRef.current, okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{requestForCameraMotionPermissions}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
    }
  }

  navigateToExplore = () =>{
    navigate("explore");
  }

  requestForCameraMotionPermissions = ()=>{
  requestForPermissions2()
  .then((status)=>{
      Utils.log("ScreenCameraHiddenFruit - hasRequiredPermissions: " + status);
      if(status){
      hiddenFruitDialogOnOkAction();
      } else {
      navigateToExplore();
      }
  })
  }

  requestForPermissions2 = async() =>{
  var result = false;
  try{
      if (Platform.OS === "android") {
      var cameraResult = await request(PERMISSIONS.ANDROID.CAMERA);
      result = cameraResult == RESULTS.GRANTED;
      } else {
      var cameraResult = await request(PERMISSIONS.IOS.CAMERA);
      var motionResult = await request(PERMISSIONS.IOS.MOTION);
      result = cameraResult == RESULTS.GRANTED && motionResult == RESULTS.GRANTED;
      }
      
  } catch (e) {
      Utils.log(e);
  }
  return result;
  }

  hiddenFruitDialogOkAction = () => {
    Utils.log("hiddenFruitDialogOkAction");
    navigate("hiddenFruit" , {data: currentNearHiddenLocation});
  }

  renderNavigateBtn = ()=>{
    if(selectedItem){
      return(
        <TouchableOpacity onPress={handleNavigate} style={contestMode? styles.detailBtn : styles.navigateBtn}>
          <Icon name={'directions'} size={25} color={"#009fc3"}/>
        </TouchableOpacity>
      );
    }
  }

  renderDetailBtn = ()=>{
    if(selectedItem){
      let touchedType = selectedItem.properties.featureType;
      if(touchedType == "poi" || touchedType == "tenant"){
        return(
          <TouchableOpacity onPress={detailPoiTenant} style={contestMode? styles.detailBtnXmas : styles.detailBtn}>
            <Icon name={'information-variant'} size={25} color={"#009fc3"}/>
          </TouchableOpacity>
        );
      }
    }
  }

  handleMyLocationBtn = ()=>{
    handleOnPressMyLocation();
  }

  renderNavInstruction = ()=>{
    if(navInstructions && selectedItem){
      return(
        <View style={styles.navInstructions}>
          <Text style={{color: StyleConstant.primaryColor, margin: 10}}>Navigating to {selectedItem.properties.popupLabel}</Text>
        </View>
      );
    }
  }

  renderPhil = ()=>{ 
    return(
      currentNearHiddenLocation? 
      <TouchableOpacity accessibilityLabel="hidden fruit nearby" activeOpacity={0.8} onPress={showFruitDialog} style={styles.philFab}>
        <Image style={contestMode? styles.xmasPhil : styles.philImage} resizeMode="contain" source={!contestMode ? require('@assets/img/phil_hidden_location.png') : currentNearHiddenLocation.object_name == Constants.TYPE_2019_XMAS_SANTA ? require('@assets/img/phil_hidden_santa.png') : require('@assets/img/phil_hidden_breadman.png')}/>
      </TouchableOpacity> :
      <View></View>
    ); 
  }

  renderAttribution = () =>{
    return(
      <View style={{position: 'absolute', paddingLeft: 10, paddingRight: 10, left: 100, bottom: Platform.OS === "android"? 5:10, flexDirection: 'row', backgroundColor: 'white'}}>
        <Image style={{width: 16, height: 16}} source={require('@assets/img/onemap_logo.png')} />
        <Text style={{marginLeft: 5, color: StyleConstant.primaryColor}} onPress={()=>{Linking.openURL("https://www.onemap.sg/home/")}}>OneMap</Text>
      </View>
    );
  }

  renderLocationBtn = () =>{
    if(myLocation.coords.latitude != Constants.SINGAPORE_CENTER[1] && myLocation.coords.longitude != Constants.SINGAPORE_CENTER[0]){
      return(
        <TouchableOpacity accessibilityLabel="zoom to my current location" onPress={handleMyLocationBtn} style={contestMode? styles.navigateBtn : styles.locationBtn}>
          <Icon name={followMap?'crosshairs-gps' : 'crosshairs'} size={25} color={"#009fc3"}/>
        </TouchableOpacity>
      );
    }
  }
  //change the onpress to open custom dialog for event
  renderXmasBtn = () =>{
    if(contestMode){
      return(
        <TouchableOpacity onPress={() => {navigate('exploreEventInfo')}} style={styles.detailBtnXmas}>
          <Image style={{width: 52, height: 52}} source={require('@assets/img/xmas_fab.png')}/>
        </TouchableOpacity>
      );
    }
  }
 
  nav2 = withinC2cBorder || Settings.get(Settings.DEBUG_MODE) == Constants.STATUS_ENABLED ? HeaderStyleWithRight : HeaderStyleWithRightOnly; 
  navOptions = {
    title: "Explore",
    headerLeft: 
      withinC2cBorder || Settings.get(Settings.DEBUG_MODE) == Constants.STATUS_ENABLED ? (
        <TouchableOpacity accessibilityLabel="enter camera page" style={{marginLeft: 10}} activeOpacity={0.5} onPress={() => navigate("camera")}>
          <Icon name="camera" size={24} color={"white"}/>
        </TouchableOpacity>
      ) : "",
    headerRight: (
      <TouchableOpacity accessibilityLabel="toggle community photos on map" style={{marginRight: 10}} activeOpacity={0.5} onPress={() => {handleToggle()}}>
        <Image source={photoRef.current == true?require('@assets/img/toggleOn.png') : require('@assets/img/toggleOff.png')} style={{width: 24, height: 24}}/>
      </TouchableOpacity>
    ),
    ...nav2
  };

  return (
    <View style={{flex:1}}>
      <NavigationEvents
        onWillFocus={
          (payload) => {
            if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
              SetContestMode(true);
            }     
          }}
        onDidFocus={
          (payload) => {
            setIsInMap(true);
            if(shouldResetCurrentNearHiddenLocation){
              setCurrentNearHiddenLocation(null);
            };
          }}
        onWillBlur={ (payload) => {setIsInMap(false)} }
      />
      {/* to put this into cameraHiddenFruit */}
      {/* <Button title="test2" onPress={ async (e)=>{  
        await Utils.recordHiddenLocationFound(currentNearHiddenLocation.id);
        const navigateBackToExplore = ()=>{
          navigate('explore2',{resetCurrentNearHiddenLocation : true})
        }
        showCustomDialog({title:"Fantastic!", body:"You've earned " + 50, okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.CELEBREATE, hasFlowerImage:true});
      }} /> 
      <Text>{ currentNearHiddenLocation? currentNearHiddenLocation.name : "not near hidden any fruit" }</Text>*/}
      {/* <Button title="test2" onPress={test} /> */}
      { renderMapStuff() }
      { renderDebugMenu() }
      { renderPhil() }
      { renderNavigateBtn() }
      { renderDetailBtn() }
      { renderAttribution() }
      { renderNavInstruction() }
      { renderXmasBtn() }
      { renderLocationBtn() }
    </View>
  );  
}

export default withScreenBase("explore", ScreenExplore, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  map: { flex: 1 },
  border: { borderColor: "#eee", borderBottomWidth: 1  },
  navigateBtn: {width: 50, height: 50, position:'absolute', alignItems:'center', justifyContent: 'center', bottom: 65, right: 10, backgroundColor: '#ffffff', borderRadius: 25, ...ShadowStyle},
  detailBtn: {width: 50, height: 50, position:'absolute', alignItems:'center', justifyContent: 'center', bottom: 120, right: 10, backgroundColor: '#ffffff', borderRadius: 25, ...ShadowStyle},
  navInstructions: {flex:1 , width:'100%', backgroundColor:'white', position: 'absolute', top: 0},
  philFab: {position: 'absolute', bottom: 20, left: 10},
  philImage: {width: 200, height: 100},
  xmasPhil: {width: 225, height: 90},
  locationBtn: {width: 50, height: 50, position:'absolute', alignItems:'center', justifyContent: 'center', bottom: 10, right: 10, backgroundColor: '#ffffff', borderRadius: 25, ...ShadowStyle},
  detailBtnXmas: {width: 50, height: 50, position:'absolute', alignItems:'center', justifyContent: 'center', bottom: 10, right: 10, backgroundColor: '#ffffff', borderRadius: 25, ...ShadowStyle},
});
