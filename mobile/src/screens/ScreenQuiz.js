import React, { useEffect, useRef, useState, useContext } from 'react';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, TouchableOpacity, ImageBackground, Alert, SafeAreaView } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import { NavigationEvents, NavigationActions } from "react-navigation";
import { Button, Icon, Image } from 'react-native-elements';
import CustomButton from '@components/CustomButton';
import useJourneyAction from '@hooks/useJourneyAction';
import {ImagePath} from '@assets/imagePaths';
import Utils from '@helpers/Utils';

const ScreenQuiz = (props) => {
  const { navigate, navigationOptions } = useNavigation();
  const station_id = useNavigationParam('station');
  const [quizData, setQuizData] = useState([]);
  const [quizNumber, setQuizNumber] = useState();
  const [answerNumber, setAnswerNumber] = useState();
  const [selectedNumber, setSelectedNumber] = useState();
  const [isDisabled, setIsDisabled] = useState(true);
  const [pressedOkBtn, setPressedOkBtn] = useState(false);
  const [questKey, setQuestKey] = useState("");
  const btnRef = useRef(null);
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const { doJourneyAction } = useJourneyAction();

  useEffect(() => {
    getQuizData();
  }, []);

  useEffect(() => {
    randomize();
  }, [quizData])

  useEffect(() => {
    setQuizAnswer();
  }, [quizNumber])

  const getQuizData = async() =>{
    const res = await WebApi.getQuestTask(station_id, 'quiz');
    if (res.status == WebApi.STATUS_OK) {
      setQuizData(res.data.quiz);
      setQuestKey(res.data.quest_key);
    } else {
      alert("Unable to get quiz");
    }
  }

  const randomize = ()=>{
    if(quizData.length != 0){
      let number = Math.floor(Math.random() * quizData.length);
      if(number != quizNumber){
        setQuizNumber(number);
      } else { randomize(); }
    }
  }

  const resetQuiz = ()=>{
    setSelectedNumber(-1);
    setPressedOkBtn(false);
    setIsDisabled(true);
    randomize();
  }

  const setQuizAnswer = ()=>{
    if(quizData.length != 0 && quizNumber != null){
      setAnswerNumber(quizData[quizNumber].answer);
    }
  }

  renderQuizTitle = () =>{
    if(quizData.length != 0 && quizNumber != null){
      return(
        <Text style={{color:'black', fontWeight: 'bold', margin: 20}}>{quizData[quizNumber].question}</Text>
      );
    } else{
      return(
        <Text style={{color:'black'}}>Loading Quiz</Text>
      );
    }
  }

  renderButtons = ()=>{
    if(quizData.length != 0 && quizNumber != null){
      const views = [];
      for(var i = 0; i < quizData[quizNumber].choices.length; i++){
        views.push(
          <Button 
            key={i}
            containerStyle={{width: '90%', height: 40, alignSelf: 'center', borderRadius: 20, marginTop: 15}}
            ref={(r)=>{btnRef.current = r}}
            title={quizData[quizNumber].choices[i]}
            titleStyle={{fontSize: 12}}
            onPress={onBtnClick.bind(this,i)}
            buttonStyle={{backgroundColor: i === selectedNumber-1 ? StyleConstant.primaryColor : "#4c8c4a"}}
          />
        );
      }
      return views;
    }
  }

  onBtnClick = (i) =>{
    setSelectedNumber(i +1);
    Utils.log(btnRef.current.props);
    //for next button
    setIsDisabled(false);
  }

  handleAnswer = async() =>{
    if(!pressedOkBtn){
      setPressedOkBtn(true);
      if(selectedNumber == answerNumber){
        //correct : call addCredit api & show correct dialog
        const result = await doJourneyAction(Constants.ACTION_QUEST_COMPLETED, questKey);
        if(result){
          showCustomDialog({title:"Fantastic!", body:"You've earned 50", okButtonText:"OK", okCallback:{navigateToExplore}, imagePath:ImagePath.COMPLETED_QUIZ, hasFlowerImage:true});
        }
      } else {
        //wrong : show try again dialog and handle dialog close with randomizing the quiz again
        showCustomDialog({title:"You've scored 0/1", body:"Your answer is incorrect.\nTap 'OK' to try again.", okButtonText:"OK", imagePath:ImagePath.LOGOUT, okCallback:{resetQuiz}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
      } 
    }
  }

  const navigateToExplore = () =>{
    props.navigation.reset([NavigationActions.navigate({routeName : "explore"})], 0);
  }

  return (
    <View style={{flex: 1}}>
      <ImageBackground style={{flex: 1}} source={require('@assets/img/bg.png')}>
        <SafeAreaView style={{flex: 1}}>
          <TouchableOpacity style={{marginTop: 20, marginLeft: 20}} onPress={()=>props.navigation.reset([NavigationActions.navigate({routeName : "explore"})], 0)}>
            <Image style={{width: 30, height: 30}} source={require('@assets/img/exit.png')}/>
          </TouchableOpacity>
          {renderQuizTitle()}
          {renderButtons()}
          <TouchableOpacity style={{marginTop: 20}} disabled={isDisabled} onPress={()=>{handleAnswer()}}>
            <CustomButton disabled={isDisabled} onPress={()=>{handleAnswer()}}>Next</CustomButton> 
          </TouchableOpacity>
        </SafeAreaView>
      </ImageBackground>
  </View>
  );  
}

navigationOptions = ({navigation}) => ({
  header: null
});

export default withScreenBase("explore/camera/wikitude_camera/quiz", ScreenQuiz, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
  },  
});
