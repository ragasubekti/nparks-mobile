import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { Text, View, FlatList, StyleSheet, Image, Dimensions, Platform, ScrollView, Linking, ImageBackground } from 'react-native';
import {useNavigation} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import { stringToBytes } from 'convert-string';
import Constants from '@helpers/Constants';
import Utils from "@helpers/Utils";
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings } from '@helpers/Settings';
import  HTML from 'react-native-render-html';
import { Button } from 'react-native-elements';
import { NavigationEvents } from "react-navigation";

const ScreenEventInfo = () => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const iWidth = Dimensions.get('window').width;
  const iHeight = iWidth / 2;
  const [dataRewards, setDataRewards] = useState([]);
  const [noOfChances, setNoOfChances] = useState(0);
  const [xmasEventStatus, setXmasEventStatus] = useState(null);

  const htmlContent1 = "<span style='font - family:Helvetica Neue; color:#000000;font-size:14px'>Santa Claus is coming to town, and he has 100 $100 NTUC Fairprice Gift Vouchers and a 13&quot MacBook Pro to give away!<br><br>All you have to do is:<br><br>- Visit checkpoints, complete quests, share photos of your experience & find the Gingerbread Men hidden along the C2C Trail to earn Flower Points. Find all 12 Gingerbread Men to earn 1,000 bonus Points!<br><br>- Every 1,500 Points earned will be exchanged for 1 lucky draw chance to win the NTUC Vouchers<br><br>- Look for Santa Claus, who will be hiding at a mystery location along the Trail on Sun, 22 Dec. To find him, follow the clues we'll blast out on Sat, 21 Dec – first person to find Santa wins the grand prize of a 13&quot MacBook Pro!<br><br><i><font color='#999999'>Tap </font><a href='https://www.nparks.gov.sg/gardens-parks-and-nature/parks-and-nature-reserves/coast-to-coast/c2c-christmas-challenge'>here</a> to find out more!</i></span>";

  useEffect(() => {
    return function cleanup() {}
  }, []);

  useEffect(() => {
    if(xmasEventStatus != null){ setNoOfChances(xmasEventStatus.lucky_draw_chance); }
  }, [xmasEventStatus]);

  getMyXmasEventStatus = async()=>{
    let res = await WebApi.getMyXmasEventStatus();
    if (res.status == WebApi.STATUS_OK) {
      setXmasEventStatus(res.data);
    } 
  }

  return (
    
    <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>   
      <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#ffffff'}}>
        <NavigationEvents onWillFocus={() => {getMyXmasEventStatus()}}/>
        <Image style={{width: iWidth, height: iHeight}} alignSelf='center' source={require('@assets/img/event_info_banner.png')} />
        <Image style={styles.decorative} source={require('@assets/img/xmas_decorative.png')}/>

        <Text style={styles.headerText}>C2C Christmas Challenge</Text>
        <Text style={styles.mutedText}>11 Dec 2019 to 22 Dec 2019</Text>

        <View style={{width: '90%', alignSelf: 'center', marginTop: 10, height: 50}}>
          <ImageBackground style={{flex:1, justifyContent:'center'}} resizeMode="contain" source={require('@assets/img/dotted.jpg')}>
            <Text style={{alignSelf: 'center', fontSize: 13, fontWeight: 'bold'}}>You have exchanged {noOfChances} Lucky Draw chance(s).</Text>
          </ImageBackground>
        </View>
        

        <View style={styles.htmlContainer}>
          <HTML onLinkPress={()=>{Linking.openURL("https://www.nparks.gov.sg/gardens-parks-and-nature/parks-and-nature-reserves/coast-to-coast/c2c-christmas-challenge")}} html={htmlContent1}/>
        </View>

        <View style={styles.btmViewHolder}>
          <Button
              containerStyle={styles.btn}
              onPress={()=>{ navigate('exploreEvent',{xmasEventStatus : xmasEventStatus}); }}
              title="GINGERBREAD MEN HUNT"
              buttonStyle={{backgroundColor: StyleConstant.xmasRed, borderRadius: 20}}
            />
            <Button
              containerStyle={styles.btn}
              onPress={()=>{ navigate('explore')}}
              title="START EXPLORING"
              buttonStyle={{backgroundColor: StyleConstant.xmasRed, borderRadius: 20}}
            />
        </View>

      </View>
    </ScrollView>
  );
}

navigationOptions = ({navigation}) => ({
  title: 'Christmas Event',
  ...HeaderStyleWithBack
});

export default withScreenBase("reward/infoEvent", ScreenEventInfo, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  decorative:{width: '100%', height: 40},
  headerText:{alignSelf: 'center', fontWeight: 'bold', fontSize: 18},
  mutedText:{alignSelf: 'center', fontSize: 12, color: StyleConstant.mutedTextColor},
  btn: {flex: 1, height: 40, width:'80%', alignSelf: 'center', marginBottom: 20, borderRadius: 20},
  btmViewHolder: { width: '100%', marginTop: 20 },
  htmlContainer: {flex: 1, width: '90%', alignSelf: 'center', marginTop: 20, marginBottom: 20},
});
