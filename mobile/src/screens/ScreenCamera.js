import React, { useEffect, useRef, useState, useContext } from 'react';
import { ShareStyle, HeaderStyleWithRight } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, ImageBackground,Dimensions,CameraRoll, PermissionsAndroid,TouchableOpacity,PixelRatio,FlatList,Image,Linking,Platform } from 'react-native';
import {useNavigation} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import CustomFlatList from '@components/CustomFlatList';
import WebApi from '@helpers/WebApi';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Utils from '@helpers/Utils';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {ImagePath} from '@assets/imagePaths';
import { NavigationEvents } from 'react-navigation';

const landmarkSize = 2;

const ScreenCamera = () => {
  const { navigate, navigationOptions } = useNavigation();
  const anim = useRef(null);
  const cameraRef = useRef(null);
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);

  //camera settings
  const [hasCameraPermission, setHasCameraPermission] = useState(false);
  const [type, setType] = useState('back');
  const [flashMode, setFlashMode] = useState('off');
  const [ratio, setRatio] = useState('1:1');
  const [captureAudio, setCaptureAudio] = useState(false);
  //end setting


  //const deviceHeight = (Dimensions.get('window').height*0.4);
  const deviceHeight = (Dimensions.get('window').width);
  const deviceWidth = (Dimensions.get('window').width);

  useEffect(() => {
    Utils.log('ScreenNPCamera');
    init();
    return function cleanup() {
     
    }
  }, []);

  init = async() =>{
    let result;
    if (Platform.OS === "android") {
      let cameraResult = await check(PERMISSIONS.ANDROID.CAMERA);
      let writeResult = await check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
      let readResult = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
      result = cameraResult == RESULTS.GRANTED && writeResult == RESULTS.GRANTED || readResult == RESULTS.GRANTED;
    } else {
      let camera = await check(PERMISSIONS.IOS.CAMERA);
      let library = await check(PERMISSIONS.IOS.PHOTO_LIBRARY);
      let microphone = await check(PERMISSIONS.IOS.MICROPHONE);
      if(camera == RESULTS.BLOCKED || library == RESULTS.BLOCKED || microphone == RESULTS.BLOCKED){
        openAppSettings=()=>{
          Linking.openURL('app-settings:');
        }
        showCustomDialog({body:"Coast-to-Coast would like access to your Camera, Library, and Microphone to take photos", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{openAppSettings}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
        return;
      }
      result = camera == RESULTS.GRANTED && library == RESULTS.GRANTED && microphone == RESULTS.GRANTED;
    }

    if(result){
      setHasCameraPermission(true);
    } else {
     showCustomDialog({body:"Coast-to-Coast would like access to your Camera, Library, and Microphone to take photos", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{requestForCameraLocationPermissions}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
    }
  }

  navigateToExplore = () =>{
    navigate("explore");
  }

  requestForCameraLocationPermissions =()=>{
    requestForPermissions1()
    .then((status)=>{
      Utils.log("ScreenCamera - hasRequiredPermissions: " + status);
      if(status){
        setHasCameraPermission(true);
      } else {
        navigateToExplore();
      }
    })
  }

  requestForPermissions1 = async() =>{
    var result = false;
    try{
      if (Platform.OS === "android") {
        var cameraResult = await request(PERMISSIONS.ANDROID.CAMERA);
        var writeResult = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
        var readResult = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
        result = cameraResult == RESULTS.GRANTED && writeResult == RESULTS.GRANTED || readResult == RESULTS.GRANTED;
      } else {
        var cameraResult = await request(PERMISSIONS.IOS.CAMERA);
        var photoResult = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
        var microphoneResult = await request(PERMISSIONS.IOS.MICROPHONE);
        result = cameraResult == RESULTS.GRANTED && photoResult == RESULTS.GRANTED && microphoneResult == RESULTS.GRANTED;
      }
      
    } catch (e) {
      Utils.log(e);
    }
    return result;
  }

  takePicture = async function() {
    if (this.camera) {
      //const options = {skipProcessing: false};
      if(type == 'back'){
        toggleActivityIndicator(true);
        const data = await this.camera.takePictureAsync({skipProcessing: false});
        Utils.log(data);
        toggleActivityIndicator(false);
        navigate('preview', {data: data});
      } else{
        toggleActivityIndicator(true);
        const data = await this.camera.takePictureAsync({fixOrientation: true, mirrorImage: true});
        Utils.log(data);
        toggleActivityIndicator(false);
        navigate('preview', {data: data});
      }
    
    }


  };

  flipCam = () =>{
    if(type == 'back'){ setType('front'); }
    else{ setType('back'); }
  }

  toggleFlash = () =>{
    if(flashMode == 'on'){ setFlashMode('off'); }
    else{ setFlashMode('on'); }
  }

  const renderCamera = () => {

    if(hasCameraPermission){
      return (
        <View style={{
          flex: 1,
          alignItems:'center'
        }}>
          
          <RNCamera
            ref={ref => this.camera = ref}
            type={type === "front"? "front":"back"}
            flashMode={flashMode === "on" ? "on":"off"}
            style = {{
              width: deviceWidth,
              height: deviceHeight,
            }}
            ratio={ratio}
            captureAudio={captureAudio}
          />

          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around', alignItems:'center', width:'100%'}}>
            <TouchableOpacity accessibilityLabel="camera flash light" onPress={toggleFlash}>    
              <Icon name={flashMode === "on"? "flash" : "flash-off"} size={30} color={"white"}/>
            </TouchableOpacity>

            <TouchableOpacity accessibilityLabel="capture" onPress={()=>takePicture()}>
              <View style={styles.avatar}>
                <Icon name="camera" size={50} color={"black"}/>
              </View>
            </TouchableOpacity>

            <TouchableOpacity accessibilityLabel="switch camera" onPress={flipCam}>
              <Icon name="camera-switch" size={30} color={"white"}/>
            </TouchableOpacity>
          
          </View>

        </View>
      );
    }
  }

  return <View style={styles.container}>{renderCamera()}</View>;

}


navigationOptions = ({navigation}) => ({
  title: 'Camera',
  headerRight: (
    <TouchableOpacity accessibilityLabel="enter augmented reality camera page" activeOpacity={0.5} onPress={() => navigation.navigate("wikitude")}>
      <Image style={{width:24, height: 24, marginRight: 10}} source={require('@assets/img/scan.png')}/>
    </TouchableOpacity>
  ),
  ...HeaderStyleWithRight
});

export default withScreenBase("explore/camera",ScreenCamera, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({  
  container: { flex: 1, backgroundColor: '#000',},
  flipButton: { flex: 0.3,height: 40,marginHorizontal: 2,marginBottom: 10, marginTop: 10,borderRadius: 8,borderColor: 'white',borderWidth: 1,padding: 5,alignItems: 'center',justifyContent: 'center',},
  autoFocusBox: { position: 'absolute',height: 64,width: 64,borderRadius: 12,borderWidth: 2,borderColor: 'white',opacity: 0.4,},
  flipText: { color: 'white', fontSize: 15,},
  zoomText: { position: 'absolute',bottom: 70,zIndex: 2,left: 2,},
  picButton: {backgroundColor: 'darkseagreen',},
  facesContainer: { position: 'absolute',bottom: 0,right: 0, left: 0,top: 0,},
  face: {padding: 10,borderWidth: 2,borderRadius: 2,position: 'absolute',borderColor: '#FFD700',justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',},
  landmark: { width: landmarkSize, height: landmarkSize, position: 'absolute', backgroundColor: 'red',},
  faceText: { color: '#FFD700', fontWeight: 'bold', textAlign: 'center', margin: 10, backgroundColor: 'transparent',},
  text: { padding: 10, borderWidth: 2, borderRadius: 2, position: 'absolute', borderColor: '#F00', justifyContent: 'center',},
  textBlock: { color: '#F00', position: 'absolute', textAlign: 'center', backgroundColor: 'transparent',},
  avatarContainer: { borderColor: '#9B9B9B', borderWidth: 2 / PixelRatio.get(), justifyContent: 'center', alignItems: 'center',},
  avatar: { backgroundColor: 'white', borderRadius: 40, width: 80, height: 80, justifyContent: 'center', alignItems: 'center'},
});
