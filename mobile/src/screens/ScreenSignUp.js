import React, { useEffect, useState, useContext, useRef } from 'react';
import { ShareStyle, HeaderStyleWithBack } from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, StyleSheet, Picker, Alert, ImageBackground } from 'react-native';
import CustomButton from '@components/CustomButton';
import {CustomTextInput, CustomTextInputType} from '@components/CustomTextInput';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Utils from '@helpers/Utils';
import ModalSelector from 'react-native-modal-selector'

const ScreenSignUp = (props) => {
  const { navigate } = useNavigation();
  const [nick, setNick] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  // const [ageRange, setAgeRange] = useState("group_5");
  const ageRange = useRef("group_5");
  const { toggleActivityIndicator, showCustomDialog } = useContext(GlobalContext);
  const [ inEventMode, setInEventMode] = useState(false);

  useEffect(() => {
    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }
  }, []);

  const handleSignUp = () => {
    setEmail(e => e.trim());
    if (nick == "") {
      Utils.showToast("Please enter a username");
      return;
    } else if (email.trim() == "") {
      Utils.showToast("Please enter your email");
      return;
    } else if (password == "") {
      Utils.showToast("Please enter your password");
      return;
    } else if (password2 == "") {
      Utils.showToast("Please confirm your password");
      return;
    } else if (ageRange.current == "group_5") {
      Utils.showToast("Please choose your age group");
      return;
    } else if (nick.length < 6 || nick.length > 30) {
      Utils.showToast("Nickname cannot be shorter than 6 or longer than 30 characters");
      return;
    } else if (!Utils.isEmail(email.trim())){
      Utils.showToast("Email is not valid");
      return;
    } else if (password != password2) {
      Utils.showToast("The passwords you entered do not match");
      return;
    } else if (Utils.hasEmoji(password)) {
      Utils.showToast("Password cannot contain any emoji");
      return;
    } else if (!Utils.validatePasswordStrength(password)) {
      Utils.showToast("Password must contain 12 or more characters with at least 1 UPPER case character and 1 lower case character."); 
    } else {
      doSignUp();
    }
  };

  doSignUp = async() => {
    toggleActivityIndicator(true);
    const res = await WebApi.register(nick, password, password2, email.trim(), ageRange.current);
    Utils.log(res);
    //toggleActivityIndicator(false);
    if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      Alert.alert(
        "System Message",
        res.data[0].message,
        [
          {text: 'OK', onPress: ()=>{toggleActivityIndicator(false)}},
        ]
      );
    } else if (res.status == WebApi.STATUS_FORBIDDEN) {
      Alert.alert(
        "System Message",
        res.data.message,
        [
          {text: 'OK', onPress: ()=>{toggleActivityIndicator(false)}},
        ]
      );
    } else if (res.status == WebApi.STATUS_OK) {
      toggleActivityIndicator(false);
      StoreSettings.store(StoreSettings.USER_EMAIL, email.trim());
      navigate('login', {page: "signup"});
    }
  }

  const data = [
    { key: 1, label: 'Age Range', value: 'group_5' },
    { key: 2, label: '12 and below', value: 'group_1' },
    { key: 3, label: '13 - 18', value: 'group_2' },
    { key: 4, label: '19 - 64', value: 'group_3' },
    { key: 5, label: '65 and above', value: 'group_4' },
    { key: 6, label: 'Prefer not to disclose', value: 'group_0' },
  ];
 
  return (
    <View style={styles.viewHolder}>

      <ImageBackground style={{flex: 1}} source={ inEventMode? require('@assets/img/bg_xmas.png') : require('@assets/img/bg.png') }>
      <CustomTextInput placeholder='Nickname' value={nick} setValue={setNick}/>
      <CustomTextInput placeholder='Email' value={email} setValue={setEmail}/>
      {/* <View style={styles.pickerHolder}> */}
        <ModalSelector
          style={{backgroundColor: '#ffffff90', width: '80%', alignSelf: 'center', borderRadius: 80}}
          data={data}
          initValue="Age Range"
          onChange={(option)=>{ ageRange.current = option.value }} />
      <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Password' value={password} setValue={setPassword}/>
      <CustomTextInput type={CustomTextInputType.PASSWORD} placeholder='Confirm Password' value={password2} setValue={setPassword2}/>
      <CustomButton onPress={handleSignUp}>Sign Up</CustomButton>
      </ImageBackground>
      
    </View>
  );
}

navigationOptions = ({navigation}) => ({
  title: 'Sign Up',
  ...HeaderStyleWithBack
});

export default withScreenBase("main/signup", ScreenSignUp, ScreenBaseType.LANDING, navigationOptions);

const styles = StyleSheet.create({
  viewHolder: {flexDirection: 'column', flex: 1},
  pickerHolder: {width: '80%', height: 50, alignSelf: 'center', backgroundColor: '#ffffff90', borderRadius: 25}
});
