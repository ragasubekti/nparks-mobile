import React, { useEffect, useRef, useState } from 'react';
import { ShareStyle, HeaderStyleWithRight, StyleConstant} from '@assets/MyStyle';
import { Text, View, KeyboardAvoidingView, UIManager, StyleSheet, ScrollView, Image, StatusBar, FlatList, TouchableOpacity, ImageBackground, Alert } from 'react-native';
import {useNavigation,useNavigationParam} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import CustomFlatList from '@components/CustomFlatList';
import { Button, Icon } from 'react-native-elements';
import Utils from '@helpers/Utils';
import Menu, {MenuItem, MenuDivider, Position} from 'react-native-enhanced-popup-menu';
import WebApi from '@helpers/WebApi';
import { StackActions } from 'react-navigation'; 
import { NavigationEvents } from "react-navigation";
import Share from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

const ScreenCommunityDetail = () => {
  //item is modelpostlocation
  const item = useNavigationParam('item');
  const { navigate, navigationOptions } = useNavigation();
  const [filePath, setFilePath] = useState("");
  const [votes, setVotes] = useState(item.votes);
  const [hasVoted, setHasVoted] = useState(item.hasVoted);
  const [id, setId] = useState();
  const anim = useRef(null);
  const [hasChanges, setHasChanges] = useState(false);
  
  useEffect(() => {
    Utils.log(item);
    async function getId(){
      setId(await StoreSettings.get(StoreSettings.USER_ID));
    }
    getId();
    return function cleanup() {
      //this is the component unmount
    }
  }, []);

  const handleLike = async() =>{
    const res = await WebApi.votePostLocation(item.id_hash);
    if (res.status == WebApi.STATUS_OK) {
      setVotes(res.data.votes);
      setHasVoted(res.data.type == "vote" ? true : false);
      setHasChanges(true)
      Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
    } else{
      Utils.showToast('failed to vote');
    }
  }

  renderPopupMenu = () => {
      if(item.user_id == id){
        return (
          <Menu ref={setMenuRef}>
            <MenuItem onPress={() => sharePost(item)}>Share Post</MenuItem>
            <MenuItem onPress={() => deleteAlert(item.id_hash)}>Delete Post</MenuItem>
          </Menu>
        )
      } else{
        return (
          <Menu ref={setMenuRef}>
            <MenuItem onPress={() => reportPost(item.id_hash)}>Report Post</MenuItem>
          </Menu>
        )
      }
  }

  const deleteAlert = (id_hash)=> {
    Alert.alert(
      'Delete Photo',
      'Are you sure you want to delete this post?',
      [
        {text: 'NO', onPress: ()=>{ hideMenu(); Utils.log('cancel')}},
        {text: 'YES', onPress: ()=>{ hideMenu(); deletePost(id_hash)}}
      ]
    );
  }

  const deletePost = async(id_hash) => {
    const res = await WebApi.deletePostLocation(id_hash);
    if (res.status == WebApi.STATUS_OK) {
      Utils.showToast("Deleted");
      if(!hasChanges){
        Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
      }
      navigate('me');
    } else { 
      Utils.showToast("Error deleting post"); 
    }
  }

  const sharePost = async(item) => {
    let dirs = RNFetchBlob.fs.dirs
    let pic = RNFetchBlob
    .config({
      fileCache: true,
      //path : dirs.DocumentDir + '/Pictures/reactnative.jpg',
    })
    .fetch('GET', item.thumbnail_base_url + '/' + item.thumbnail_path )
    .then(async resp => {
      let base64s = RNFetchBlob.fs
      .readFile(resp.data, 'base64')
      .then(data=>"data:image/jpeg;base64," + data);
      return base64s;
    }).then( res=>{
      const options = {
        title: "Share to",
        type: 'image/jpeg',
        url: res
      };
      Share.open(options)
      .then((res) => { hideMenu() })
      .catch((err) => { err && Utils.log(err); })
      }
    )
  }

  return (
    <ScrollView bounces={false} style={styles.bgWhite}>
      <View style={styles.bgWhite}>
        <View style={{...styles.row, alignItems: 'center'}}>
          <CustomImage style={styles.customImage} path={item.avatar_url}/>
          <View style={styles.centreContent}>
            <Text style={styles.textName}>{item.nickname}</Text>
            <Text style={styles.textLocation} numberOfLines= {1}>{item.location}</Text>
          </View>
        </View>
        <CustomImage style={styles.image} resizeMode={FastImage.resizeMode.cover} path={ item.thumbnail_base_url + '/' + item.thumbnail_path}/>
        <View style={styles.row}>
          <TouchableOpacity accessibilityLabel={"like button. You have " + (hasVoted == true ? "liked" : "not liked") + " this photo"} activeOpacity={0.5} onPress={handleLike}>
            <Image style={styles.like} source={hasVoted == true ? require('@assets/img/liked.png') : require('@assets/img/like.png')}/>
          </TouchableOpacity>
          <TouchableOpacity style={{alignSelf: "center", marginLeft: 15}} activeOpacity={0.5} onPress={handleLike}>
            <Text style={{fontSize: 12}}>Liked by {votes} people</Text>
          </TouchableOpacity>
          <Text style={styles.date}>{Utils.timestampToDate(new Date(item.created_at * 1000))}</Text>
        </View>
        <View style={styles.alignbtm}>
          <Text style={styles.textName}>{item.nickname} <Text style={styles.textDescription}>{item.description}</Text></Text>
        </View>

        {this.renderPopupMenu()}
      </View>
    </ScrollView>
  );
}

let tref = React.createRef();
let menuRef = null;
const setMenuRef = ref => menuRef = ref;
const hideMenu = () => menuRef.hide();
const showMenu = () => menuRef.show(tref.current, stickTo = Position.BOTTOM_LEFT);

const reportPost = async(id_hash)=>{
  const res = await WebApi.reportPostLocation(id_hash);
  if (res.status == WebApi.STATUS_OK) {
    Alert.alert(
      "System Message",
      "Successfully reported post",
      [{text: 'OK', onPress: ()=>{hideMenu()}}]
    );
  } else{ 
    Alert.alert(
      "System Message",
      "Error reporting post",
      [{text: 'OK', onPress: ()=>{hideMenu()}}]
    );
  }
}

navigationOptions = ({navigation}) => ({
  title: navigation.getParam("title", "community"),
  headerRight: (
    <TouchableOpacity accessibilityLabel="menu icon" activeOpacity={0.5} ref={tref} onPress={ () => showMenu()}>
      <Icon name="more-vert" size={30} color={"white"}/>
    </TouchableOpacity>
  ),
  ...HeaderStyleWithRight
});
export default withScreenBase("community/detail", ScreenCommunityDetail, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder: { marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},
  heart: { width: 20, height: 20, backgroundColor: 'transparent',},  
  bgWhite: { flex: 1, backgroundColor: "white"},
  textLocation: { fontSize: 12, color: 'black', marginTop: 2, maxWidth: 400},
  textName: { fontSize: 12, fontWeight: "bold" ,color: 'black'},
  centreContent: { marginLeft: 10, alignSelf: 'center'},
  customImage: { width: 48, height: 48, borderRadius: 24, marginLeft: 10, alignSelf: 'center'},
  like: {width:30, height: 24, left: 10},
  image: {width: "100%", height: 350, marginTop: 10},
  date: {color: "#cccccc", fontSize: 12, right: 10, position: 'absolute', alignSelf: "center"},
  alignbtm: {flexDirection:"row", marginTop: 6, marginLeft: 10, marginRight: 10, marginBottom: 20},
  textDescription: {fontWeight: "normal", color: StyleConstant.tabGray},
  row: {flexDirection:"row", marginTop: 10},
  
});
