import React, { useEffect, useRef, useState, useContext } from 'react';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import { ShareStyle, HeaderStyleWithRight } from '@assets/MyStyle';
import { Text,TextInput, View, KeyboardAvoidingView, StyleSheet, ImageBackground,Dimensions,CameraRoll, PermissionsAndroid,TouchableOpacity,PixelRatio,FlatList,Image, ScrollView } from 'react-native';
import {useNavigation,useNavigationParam} from 'react-navigation-hooks';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import CustomButton from '@components/CustomButton';
import CustomFlatList from '@components/CustomFlatList';
import WebApi from '@helpers/WebApi';
import ImageResizer from 'react-native-image-resizer';
import clean from 'swearing-is-bad';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Avatar } from 'react-native-paper';
import useJourneyAction from '@hooks/useJourneyAction';
import Utils from '@helpers/Utils';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

const landmarkSize = 2;

const ScreenCameraPreview = (props) => {
  const params = useNavigationParam('data');
  const { navigate, navigationOptions } = useNavigation();
  const [userLat, setUserLat] = useState("");
  const [userLong, setUserLong] = useState("");
  const [location, setLocation] = useState("Searching for location...");
  const [imageSource, setImageSource] = useState([]);
  const [desc, setDesc] = useState();
  const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const [currentAvatar, setcurrentAvatar] = useState(""); 
  const imageRef = useRef(null);
  const textRef = useRef(null);
  const locationRef = useRef(null);
  const uploadingRef = useRef(null);
  const { doJourneyAction } = useJourneyAction();
  const iWidth = (Dimensions.get('window').width) - 30;

  useEffect(() => {
    props.navigation.setParams({"navOptions":navOptions});    
    async function getCurrentAvatar(){
      setcurrentAvatar(await StoreSettings.get(StoreSettings.USER_AVATAR_URL));
    }
    async function getLatitude(){
      setUserLat(await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE));
    }
    async function getLongitude(){
      setUserLong(await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE));
    }
    getCurrentAvatar();
    getLatitude();
    getLongitude();
    ImageResizer.createResizedImage(params.uri, 800, 800, 'JPEG', 100,).then((response) => {
      Utils.log(response);
      imageRef.current = [response];
      setImageSource([...imageSource, response]);
    }).catch((err) => {
      Utils.log(err);
    })
    return function cleanup() {
    }
  }, []);

  useEffect(() => {
    if(userLat != "" && userLong != ""){
      var isLocationSet = false;
      var b = 0;
      do{
        b += 10;
        isLocationSet = setUserLocation(b);
      } while(!isLocationSet)
    }

  }, [userLat, userLong]);

  
  setUserLocation = async(buffer) => {
    let res = await WebApi.reverseGeolocation(userLat , userLong, buffer);
    Utils.log(res);
    if(Object.keys(res.GeocodeInfo).length <= 0){
      return false;
      // setLocation("-");
      // locationRef.current = "-";
    }
    if(!res.GeocodeInfo[0].hasOwnProperty("BUILDINGNAME") || res.GeocodeInfo[0].BUILDINGNAME == "null"){
      if (res.GeocodeInfo[0].ROAD == "NIL") {
        return false;
      }
      setLocation(res.GeocodeInfo[0].ROAD);
      locationRef.current = res.GeocodeInfo[0].ROAD;
    } else {
      setLocation(res.GeocodeInfo[0].BUILDINGNAME);
      locationRef.current = res.GeocodeInfo[0].BUILDINGNAME;
    }
    return true;
  }

  const upload = async() => {
    if(uploadingRef.current == null){
      uploadingRef.current = "true";
      const img = imageRef.current;
      Utils.log(img);
      if(img.length > 0){
        toggleActivityIndicator(true);
        // const user_id = await StoreSettings.get(StoreSettings.USER_ID);
        let text = textRef.current == null ? textRef.current :clean(textRef.current);
        let post_res = await WebApi.postUserPostLocation(img, text, locationRef.current);
        Utils.log(post_res.data);
        let action_res = await doJourneyAction(Constants.ACTION_UPLOAD,post_res.data.id_hash);
        toggleActivityIndicator(false);
        if(post_res.status == WebApi.STATUS_OK && action_res == true){
          navigate('communityDetail', {item:post_res.data, title:'My Post'})
        } else {
          alert("Unable to upload post, please try again...")
        }
      }
    }
  }

  navOptions = {
    title: "New Post",
    headerRight: (
      <TouchableOpacity style={{marginRight: 10}} activeOpacity={0.5} onPress={(e)=>{upload()}}>
        <Text style={{color: 'white', fontWeight: 'bold'}}>SHARE</Text>
      </TouchableOpacity>
    ),
    ...HeaderStyleWithRight
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#ffffff'}}>
      <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#cccccc'}}>
        <View style={{backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
          {/* avatar and textinput */}
          <View style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
            <CustomImage style={{width: 50, height: 50, borderRadius: 25, marginTop: 10, marginBottom: 10, marginLeft: 15, alignSelf: 'center'}} path={currentAvatar} />
            <TextInput
              style={{flex: 1, marginLeft: 10, marginRight: 15}}
              onChangeText={(text) => {textRef.current = text}}
              value={desc}
              multiline = {true}
              numberOfLines = {4}
              placeholder="Write a caption..."
            />
          </View>
        </View>

        <View style={{marginTop: 1, flexDirection: 'column', backgroundColor: 'white', alignItems: 'center'}}>
          {/* icon, text and text below */}
          <View style={{width: '100%', flexDirection: 'row', marginTop: 10}}>
            <Icon accessibilityLabel="location icon" style={{marginLeft: 15, alignSelf:"center"}} name="map-marker" size={16} color={"gray"}/>
            <Text accessibilityLabel="current location" style={{alignSelf: 'flex-start'}}>Location &gt;</Text>
          </View>
          <View style={{width: '100%', marginBottom: 10}}>
            <Text style={{marginLeft: 19, alignSelf: 'flex-start'}}>{location}</Text>
          </View>
        </View>

        <View style={{marginTop: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
          <CustomImage style={{marginTop: 15, width: iWidth, height: iWidth}} path={params.uri}/>
        </View>
      </View>
    </ScrollView>
  );
}


export default withScreenBase("explore/camera/upload_photo_preview", ScreenCameraPreview, ScreenBaseType.MAIN);

const styles = StyleSheet.create({
  holder:{ marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},
  heart:{ width: 20, height: 20, backgroundColor: 'transparent',},  
  topHolder: { flex: 1, flexDirection: 'column', alignItems: 'flex-start', width: '100%',},
  btn: { paddingTop: 30,},
  container: { flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F5FCFF',},
  avatarContainer: { borderColor: '#9B9B9B', borderWidth: 1 / PixelRatio.get(), justifyContent: 'center', alignItems: 'center',},
  avatar: { borderRadius: 75, width: 150, height: 150,},
  flatview: { justifyContent: 'center', paddingTop: 30, borderRadius: 2,},
  name: { fontFamily: 'Verdana', fontSize: 18},
  email: { color: 'red'},
  flipButton: { flex: 0.3, height: 40, marginHorizontal: 2, marginBottom: 10, marginTop: 10, borderRadius: 8, borderColor: 'white', borderWidth: 1, padding: 5, alignItems: 'center', justifyContent: 'center',},
  autoFocusBox: { position: 'absolute', height: 64, width: 64, borderRadius: 12, borderWidth: 2, borderColor: 'white', opacity: 0.4,},
  flipText: { color: 'white', fontSize: 15,},
  zoomText: { position: 'absolute', bottom: 70, zIndex: 2, left: 2,},
  picButton: { backgroundColor: 'darkseagreen',},
  facesContainer: { position: 'absolute', bottom: 0, right: 0, left: 0, top: 0,},
  face: { padding: 10, borderWidth: 2, borderRadius: 2, position: 'absolute', borderColor: '#FFD700', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)',},
  landmark: { width: landmarkSize, height: landmarkSize, position: 'absolute', backgroundColor: 'red',},
  faceText: { color: '#FFD700', fontWeight: 'bold', textAlign: 'center', margin: 10, backgroundColor: 'transparent',},
  text: { padding: 10, borderWidth: 2, borderRadius: 2, position: 'absolute', borderColor: '#F00', justifyContent: 'center',},
  textBlock: { color: '#F00', position: 'absolute', textAlign: 'center', backgroundColor: 'transparent',}
});
