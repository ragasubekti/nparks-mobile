import React, { useEffect, useRef, useState, useContext } from 'react';
import { View, Text, StyleSheet, Button as Btn, ImageBackground, FlatList, ScrollView, Image, Platform } from 'react-native';
import { List, ListItem } from "react-native-elements";
import CustomButton from '@components/CustomButton';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import useMapboxLocation from '@hooks/useMapboxLocation';
import Constants from '@helpers/Constants';
import Utils from '@helpers/Utils';

const ScreenInfoPoiDetail = () => { 
  const item = useNavigationParam('item');
  const { renderMapStuff, prepareMarker } = useMapboxLocation();

  useEffect(() => {
    prepareMarker([item.longitude, item.latitude]);
    return function cleanup() { }
  }, []);

  handleOnPress = () => {
    Utils.log('trigger device unlock');
  }

  return (
    <ScrollView bounces={false} style={{flex: 1, backgroundColor: "white"}}>
    <View style={{backgroundColor: 'white', flex: 1}}>
      <View style={styles.cardHolder}>
        <ImageBackground
          resizeMode={'cover'}
          style={{flex:1}}
          source={{uri: item.thumbnail_base_url + '/' + item.thumbnail_path}}
        >
        <View style={styles.alignCentre}>

          <Text style={styles.poiName}>{item.name}</Text>
          <View style={styles.row}>
            <Icon style={{alignSelf: 'center'}} name="map-marker" size={13} color={"white"}/>
            <Text style={styles.poiGroup}>{item.poi_group_name}</Text>
          </View>

        </View>
        </ImageBackground>
      </View>

      <Text style={styles.poiDescription}>{item.description}</Text>
      <View style={styles.poiView}>
        <Image style={styles.image} resizeMode={'contain'} source={require('@assets/img/explore_inactive.png')}/>
        <Text style={styles.poiGN_Grey}>{item.poi_group_name}</Text>
      </View>

      <View style={styles.map}>
        {renderMapStuff()}
      </View>
    </View>
    </ScrollView>
  );

}

navigationOptions = ({navigation}) => ({
  title: 'Points of Interest',
  ...HeaderStyleWithBack
});
 
export default withScreenBase("Information/place_of_interest/detail", ScreenInfoPoiDetail, ScreenBaseType.MAIN, navigationOptions);

var styles = StyleSheet.create({
  holder:{ marginTop: 50, height: 200, justifyContent: 'center', alignItems: 'center',},  
  cardHolder:{ maxWidth: "100%",  height: 175,  backgroundColor: "white"},
  poiName: {color:"white", marginLeft: 15, marginRight: 15, fontSize: 14, fontWeight: "bold"},
  poiGroup: {marginLeft: 5, alignSelf: 'center', fontSize: 12, color: "white"},
  poiGN_Grey: {left: 10, alignSelf: 'center', right: 15, fontSize: 12, color: "#cccccc"},
  poiView: {flexDirection:"row", marginTop:15, left: 15, right: 15, alignContent: 'center'},
  alignCentre: {width: "100%", height: 55, justifyContent:'center', position: "absolute", bottom: 0, backgroundColor:"rgba(153,102,68,0.6)"},
  row: {flexDirection:"row", marginLeft: 15, marginRight: 15, marginTop: 2},
  poiDescription: {marginTop: 20, marginLeft: 15, marginRight: 15, fontSize: 12, color: "black"},
  image: {width:13, height: 13, alignSelf: 'center'},
  map: {marginTop: 20, height: 300, backgroundColor: StyleConstant.bgGray}
});
