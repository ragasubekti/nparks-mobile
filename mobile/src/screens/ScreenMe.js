import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import { withScreenBase, ScreenBaseType } from '@screens/withScreenBase';
import {useNavigation} from 'react-navigation-hooks';
import WebApi from '@helpers/WebApi';
import { Avatar } from 'react-native-paper';
import CustomFlatList from '@components/CustomFlatList3';
import ModelPostLocation from '@models/ModelPostLocation';
import Constants from '@helpers/Constants';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Utils from '@helpers/Utils';
import { Settings, StoreSettings } from '@helpers/Settings';
import { NavigationEvents } from 'react-navigation';
import firebase, { Notification, RemoteMessage } from 'react-native-firebase';
import { StyleConstant } from '@assets/MyStyle';
import CustomImage from '@components/CustomImage';

const ScreenMe = (props) => {
  const { navigate } = useNavigation();
  const [dataProfile, setDataProfile] = useState({});
  const [imageSource, setImageSource] = useState("");
  const dataUrl = Constants.API_SERVER_URL + '/user/list-post-location-me';
  const [hasChanges, setHasChanges] = useState(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
  const r = ModelPostLocation.renderMyPost(navigate, 'myPhotoDetail', 'My Posts');
  const { toggleRefresh, renderCustomFlatList } = CustomFlatList(dataUrl, r, Constants.LAYOUT_OFFSET);
  const [numberOfNotification, setNumberOfNotification] = useState(0);
  const [ inEventMode, setInEventMode] = useState(false);

  useEffect(() => {
    if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
      setInEventMode(true);
    }
    return function cleanup() {
    }
  }, []);

  const setProfile = async() => {
    const avatar_url = await StoreSettings.get(StoreSettings.USER_AVATAR_URL);
    const nickname = await StoreSettings.get(StoreSettings.USER_NICKNAME);
    const credit = await StoreSettings.get(StoreSettings.USER_CREDIT);
    let data = { "avatar_url":avatar_url, "nickname":nickname, "credit":credit }
    setDataProfile(data)
  }

  renderAvatar = ()=>{
    if(dataProfile != null){   
      return(
        <TouchableOpacity accessibilityLabel="enter edit profile page" onPress={handleEditProfile}>
          <CustomImage style={{width: 70, height: 70, borderRadius: 35}} path={dataProfile.avatar_url}/>
          <Image style={{position: 'absolute', width: 16, height: 16, alignSelf: 'flex-end', bottom: 0}} source={require('@assets/img/pen.png')}/>
        </TouchableOpacity>
      );
    }
  }

  renderBadge = ()=>{
    if(numberOfNotification > 0){   
      return(
        <Image style={{position: 'absolute', width: 8, height: 8, alignSelf: 'flex-end'}} source={require('@assets/img/red_dot.png')}/>
      );
    }
  }

  renderProfile = () =>{
    return(
      <View style={{flexDirection:'column', alignSelf:'center', marginLeft: 10}}>
        <Text style={{color: 'white'}}>{dataProfile.nickname}</Text>
        <View style={{flexDirection:'row'}}>
          <Text accessibilityLabel={"you have "+dataProfile.credit+" credits"} style={{color: 'white'}}>{dataProfile.credit}</Text>
          <Image style={{width: 12, height: 12}} resizeMode={'contain'} source={require('@assets/img/flower.png')} alignSelf='center'/>
        </View>
      </View>
    )
  }

  renderXmasString = () =>{
    if(inEventMode){
      return(
        <Image style={{position:'absolute', width: '100%', height: 40}} source={require('@assets/img/xmas_decorative.png')}/>
      );
    }
  }

  addToList = (uri) =>{
    setImageSource([...imageSource, uri]);
    //Utils.log("added to list");
  }

  getNumberOfUnreadNotification = async() =>{
    const res = await WebApi.getNumberOfUnreadNotification();
    if (res.status == WebApi.STATUS_OK) {
      setNumberOfNotification(res.data);
      firebase.notifications().setBadge(res.data)
    }
  }
  
  const handleJourney = () => { navigate('journey', {credit:dataProfile.credit}) };
  const handleNotification = () => { navigate('notification') };
  const handleSetting = () => { navigate('setting') };
  const handleEditProfile = () => { navigate('editProfile') };
 
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#1b5e20'}}>
      <View style={{flex:1}}>
        <NavigationEvents
          onWillFocus={()=>{async function callApi(){
            await setProfile();//set default profile
            await Utils.refreshUserProfile();
            await setProfile();//if have new profile set new profile
            await getNumberOfUnreadNotification();
            if(Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA)){
              setHasChanges(!hasChanges);
              toggleRefresh();
              Settings.store(Settings.HAS_CHANGES_IN_FLATLIST_DATA, !Settings.get(Settings.HAS_CHANGES_IN_FLATLIST_DATA));
            }
          }
          callApi();}}
        />
        <View style={{backgroundColor:"#1b5e20"}}>
          <View style={{margin: 10, flexDirection: 'row'}}>
            {renderAvatar()}
            {renderProfile()}

            <View style={{flexDirection:'row', position: 'absolute', right: 0, width: '33.33%', justifyContent:'space-around'}}>
              <TouchableOpacity accessibilityLabel="enter journey history page" activeOpacity={0.5} onPress={handleJourney} >
                <Icon name="history" size={24} color={"white"}/>
              </TouchableOpacity>

              <TouchableOpacity accessibilityLabel="enter notification page" activeOpacity={0.5} onPress={handleNotification} >
                <Icon name="bell" size={24} color={"white"}/>
                {renderBadge()}
              </TouchableOpacity>

              <TouchableOpacity accessibilityLabel="enter settings page" activeOpacity={0.5} onPress={handleSetting} >
                <Icon name="cog" size={24} color={"white"}/>
              </TouchableOpacity>
            </View>

          </View>
        </View>

        <View style={{backgroundColor: StyleConstant.bgGray, flex: 1}}>
          {renderCustomFlatList()}  
          {renderXmasString()}
        </View>
      </View>
    </SafeAreaView>   
  );  
}

navigationOptions = ({navigation}) => ({ header: null });

export default withScreenBase("me", ScreenMe, ScreenBaseType.MAIN, navigationOptions);

const styles = StyleSheet.create({
  holder:{
    height: 200,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',    
  },  
});


