import Toast from 'react-native-root-toast';
import {Keyboard, TextInput} from 'react-native';
import Constants from '@helpers/Constants';
import { Settings, StoreSettings } from '@helpers/Settings';
import Utils from '@helpers/Utils';
import {fetch} from 'react-native-ssl-pinning';

const POST_METHOD = 'POST';
const GET_METHOD = 'GET';

const STATUS_OK = 200;
const STATUS_BAD_REQUEST = 400;
const STATUS_UNAUTHORIZED = 401;
const STATUS_FORBIDDEN = 403;    
//const STATUS_DATA_VALIDATION_FAILED = 422;
const STATUS_UNPROCESSABLE_ENTITY = 422;

/*
//response format
{
    "status": 200,
    "text": "OK",
    "data": {
        "authorization_code": "5b936156796b42a9e9564d6b61e8eae99c4b640729ae5982f97ab65defd49add",
        "expires_at": 1559740415
    }
}
*/

callApi = async(method, endpoint, data, isMultipart = false) => {
  
  var promise = new Promise(async (resolve, reject) => {            
    const url = Constants.API_SERVER_URL + endpoint;
    const token = await StoreSettings.get(StoreSettings.ACCESS_TOKEN);

    var config = {
      method: method,
			timeoutInterval: 10000,
      headers: {
        'Authorization': "Bearer " + token,
        // 'Content-Type': 'application/json',
        'Accept': "application/json; charset=utf-8",
        "Access-Control-Allow-Origin": "*",
        "e_platform": "mobile",
      },
      body: JSON.stringify(data),
      // your certificates array (needed only in android) ios will pick it automatically
      sslPinning: {
        certs: ["c2c_staging","c2c_prod"] // your certificates name (without extension), for example c2c_staging.cer, c2c_prod.cer
      },
    };
    if(isMultipart){
      // config.headers['Content-Type'] = 'multipart/form-data';
      config.headers.Accept = 'application/json, text/plain, /';
      config.body = {
        formData: data,
      };
    }

    if (method == GET_METHOD) {
      delete config.body;
    }
    
    fetch(url, config)
    .then((res)=>res.json())
    .then((resJson)=>{
      resolve(resJson);
    }).catch((e)=>{
      Utils.log("callApi: " + url);
      var res = JSON.parse(e.bodyString);
      Utils.log(res);
      resolve(res);
    });
        
  });
  return promise;
  
}

callApiWithEncyptedResponse = async(method, endpoint, data) => {
  
  var promise = new Promise(async (resolve, reject) => {            
    const url = Constants.API_SERVER_URL + endpoint;
    const token = await StoreSettings.get(StoreSettings.ACCESS_TOKEN);
    
    var config = {
      method: method,
			timeoutInterval: 10000,
      headers: {
        'Authorization': "Bearer " + token,
        'Accept': "application/json; charset=utf-8",
        "Access-Control-Allow-Origin": "*",
        "e_platform": "mobile",
      },
      body: JSON.stringify(data),
      // your certificates array (needed only in android) ios will pick it automatically
      sslPinning: {
        certs: ["c2c_staging","c2c_prod"] // your certificates name (without extension), for example c2c_staging.cer, c2c_prod.cer
      },
    }
    
    fetch(url, config).then(async(res)=>{
      res.text().then(async(text)=>{
        Utils.decrypt(text).then(async(response)=>{
          resolve(JSON.parse(response))
        });
      });
    }).catch((e)=>{
      Utils.error("callApiWithEncyptedResponse",url,e);
      reject(e);
    });
        
  });
  return promise;
  
}

const WebApi = {
    STATUS_OK: 200,
    STATUS_BAD_REQUEST: 400,
    STATUS_UNAUTHORIZED: 401,
    STATUS_FORBIDDEN: 403,
    STATUS_UNPROCESSABLE_ENTITY: 422,
    authorise: async(email, pwd) => {
      var data = { "email": email, "password": pwd }
      return await callApi(POST_METHOD, '/user/authorize', data);
    },
    register: async(nickname, password, password_confirm, email, age_group) =>{
      var data = { "nickname": nickname, "password": password, "password_confirm": password_confirm, "email": email, "age_group": age_group }
      return await callApi(POST_METHOD, '/user/register', data);
    },
    accessToken: async(code) => {
      var data = { "authorization_code": code }
      return await callApi(POST_METHOD, '/user/access-token', data);
    },
    //me tab
    getMyProfile: async() => {
      return await callApi(GET_METHOD, '/user/me');
    },
    listPostLocationMe: async(page, result) => {
      return await callApi(GET_METHOD, '/user/list-post-location-me?page=' + page + '&result=' + result);
    },
    logout: async() =>{
      return await callApi(GET_METHOD, '/user/logout');
    },
    changePassword: async(currentPassword, password, confirmPassword) =>{
      var data = { "current_password": currentPassword, "password": password, "password_confirm": confirmPassword};
      return await callApi(POST_METHOD, '/user/password-change', data);
    },
    changeNickname: async(nickname) =>{
      var data = { "nickname": nickname };
      return await callApi(POST_METHOD, '/user/nickname-change', data);
    },
    //info tab 
    listTenants: async() => {
      return await callApi(GET_METHOD, '/nparks/list-tenant');
    },
    listPoi: async() => {
      return await callApi(GET_METHOD, '/nparks/list-poi-group');
    },
    listPoiGroup: async(id) => {
      return await callApi(GET_METHOD, '/nparks/list-poi-group-list?poi_group_id=' + id);
    },
    getPoiDetail: async(id) => {
      return await callApi(GET_METHOD, '/nparks/poi?id=' + id);
    },
    getTenantDetail: async(id) => {
      return await callApi(GET_METHOD, '/nparks/tenant?id=' + id);
    },
    //community tab
    listPostLocationTop: async(page, result) => {
      return await callApi(GET_METHOD, '/user/list-post-location-top-post?page=' + page + '&result=' + result);
    },
    listPostLocationNearby: async(page, result) => {
      const lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
      const lng = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);
      return await callApi(GET_METHOD, '/user/list-post-location-nearby?page=' + page + '&result=' + result + '&latitude=' + lat + '&longitude=' + lng);
    },
    listPostLocationRecent: async(page, result) => {
      return await callApi(GET_METHOD, '/user/list-post-location-recent?page=' + page + '&result=' + result);
    },
    listPostLocationCluster: async(page, result, lat, lng) => {
      return await callApi(GET_METHOD, '/user/list-post-location-cluster?page=' + page + '&result=' + result + '&latitude=' + lat + '&longitude=' + lng);
    },
    votePostLocation: async(id_hash) => {
      return await callApi(GET_METHOD, '/user/vote-post-location?id_hash=' + id_hash);
    },
    deletePostLocation: async(id_hash) => {
      return await callApi(GET_METHOD, '/user/delete-post-location?id_hash=' + id_hash);
    },
    reportPostLocation: async(id_hash) => {
      return await callApi(GET_METHOD, '/user/report-post-location?id_hash=' + id_hash);
    },
    //rewards tab
    listActiveRewards: async() => {
      return await callApi(GET_METHOD, '/user/my-rewards');
    },
    listInactiveRewards: async() => {
      return await callApi(GET_METHOD, '/user/my-inactive-rewards');
    },
    doUserJourneyAction: async(action, parameter = "") =>{
      const lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
      const lng = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);
      const device_id = await Settings.get(Settings.DEVICE_ID);
      const device_type = await Settings.get(Settings.DEVICE_PLATFORM);
      const app_version = await Settings.get(Settings.INSTALLED_APP_VERSION);
      var data = { "action": action, "parameter": parameter, "latitude": lat, "longitude": lng, "device_id": device_id, "device_type": device_type, "app_version": app_version }
      return await callApi(POST_METHOD, '/user/journey-action', data);
    },
    getQuestTask: async(id, type) =>{
      return await callApi(GET_METHOD, '/nparks/list-quest-task?quest_id=' + id + '&type=' + type);
    },
    getStationQuest: async(station_id)=>{
      return await callApi(GET_METHOD, '/nparks/quest?id=' + station_id);
    },
    postUserPostLocation: async(image_url, description, location) => {
      //Utils.log(image_url);
      //Utils.log("preview");

      const lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
      const lng = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);
      const user_id = await StoreSettings.get(StoreSettings.USER_ID);

      var formData = new FormData();
      let counter =0;
      image_url.map((img) => {
        counter++;
        var f = {
          uri : img.uri,
          type: 'image/jpeg',
          name: counter+'.jpg',
        }
        formData.append('image_file', f);
      })

      var data = {"user_id": user_id, "description": description, "latitude": lat, "longitude": lng, "location":location}

      formData.append('json',JSON.stringify(data));

      return await callApi(POST_METHOD, '/user/upload-post-location', formData, true);
    },
    uploadAvatar: async(image_url, user_id) =>{
      const lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
      const lng = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);

      var formData = new FormData();
      var f = {
        uri: image_url,
        type: 'image/jpeg',
        name: 'avatar.jpg',
      }
      formData.append('image_file', f);
      var data = {"user_id": user_id, "description": '1', "latitude": lat, "longitude": lng}

      formData.append('json',JSON.stringify(data));
      return await callApi(POST_METHOD, '/user/upload-profile-avatar', formData, true);
    },
    listMyInbox: async() =>{
      return callApi(GET_METHOD, '/user/list-my-inbox');
    },
    readNotification: async(id) =>{
      return callApi(GET_METHOD, '/user/read-notification?id=' + id);
    },
    deleteNotification: async(id) =>{
      return callApi(GET_METHOD, '/user/delete-notification?id=' + id);
    },
    deleteAllNotification: async() =>{
      return callApi(GET_METHOD, '/user/delete-all-notification');
    },
    resendVerifyEmail: async(email) =>{
      var data = { "email": email };
      return await callApi(POST_METHOD, '/user/resend-verify-email', data);
    },
    listServerSettingEncypt: async() =>{
      return callApiWithEncyptedResponse(GET_METHOD, '/sys/list-setting-encrypt');
    },
    getPopupBanner: async() =>{
      return callApi(GET_METHOD, '/sys/app-popup-banner');
    },
    registerFcmToken: async() =>{
      var fcm_token = await StoreSettings.get(StoreSettings.FCM_TOKEN);
      fcm_token = fcm_token == null || fcm_token == undefined || fcm_token == "" ? "-" : fcm_token;
      var data = { "token": fcm_token };
      return callApi(POST_METHOD, '/user/register-fcm-token', data);
    },
    listMyJourneyHistory: async(page, result) =>{
      return callApi(GET_METHOD, '/user/my-journey-history' + page + "&result=" + result);
    },
    listHiddenLocationToday: async() => {
      return await callApi(GET_METHOD, '/nparks/list-hidden-location-today', {});
    },
    

    //map stuffs
    getC2cRouteGeoJson: async() => {
      return await callApi(GET_METHOD, '/nparks/c2c-route-geo-json', {});
    },
    getCheckpointGeoJson: async() => {
      return await callApi(GET_METHOD, '/nparks/list-quest-geo-json', {});
    },
    getPostLocationSummaryGeoJson: async() => {
      return await callApi(GET_METHOD, '/user/list-post-location-summary-geo-json', {});
    },
    getPostLocationBboxGeoJson: async(x0, y0, x1, y1) => {
      return await callApi(GET_METHOD, '/user/list-post-location-bounding-box-geo-json?x0='+x0+'&y0='+y0+'&x1='+x1+'&y1='+y1, {});
    },
    getTenantGeoJson: async() => {
      return await callApi(GET_METHOD, '/nparks/list-tenant-geo-json', {});
    },
    getPoiGeoJson: async() => {
      return await callApi(GET_METHOD, '/nparks/list-poi-geo-json', {});
    },
    getTreeGeoJson: async() => {
      return await callApi(GET_METHOD, '/imaven/list-heritage-tree-geo-json', {});
    },    
    getAmenityJson: async() => {
      return await callApi(GET_METHOD, '/imaven/list-feature-geo-json', {});
    },

    getNumberOfUnreadNotification: async() => {
      return await callApi(GET_METHOD, '/user/get-number-of-unread-notification', {});
    },
    reverseGeolocation: async(lat, long, buffer) =>{
      //return callApi(GET_METHOD, '/sys/app-popup-banner');
      var promise = new Promise(async (resolve, reject) => {
        let token = await Settings.get(Settings.ONE_MAP_TOKEN);            
        const url = 'https://developers.onemap.sg/privateapi/commonsvc/revgeocode?token=' + token + '&location=' + lat + ',' + long + "&buffer=" + buffer; 
        // /const token = await StoreSettings.get(StoreSettings.ACCESS_TOKEN);
        var config = null;

        config = {
          method: GET_METHOD,
          disableAllSecurity: true,
        }      
        fetch(url, config)
        .then((res)=>res.json())
        .then((resJson)=>{
          resolve(resJson);
        }).catch((e)=>{
          Utils.log("callApi",url,e);
          reject(e);
        });
            
      });
      return promise;
    },
    getOneMapRoutePcn: async(start, end ) =>{
      
      var promise = new Promise(async (resolve, reject) => {            
        let token = await Settings.get(Settings.ONE_MAP_TOKEN);
        const url = "https://developers.onemap.sg/privateapi/routingsvc/route?start=" + start + "&end=" + end + "&routeType=pcn&token=" + token;
        var config = null;

        config = {
          method: GET_METHOD,
          disableAllSecurity: true,
        }      
        fetch(url, config)
        .then((res)=>res.json())
        .then((resJson)=>{
          resolve(resJson);
        }).catch((e)=>{
          Utils.log("callApi",url,e);
          reject(e);
        });
            
      });
      return promise;
    },
    getMyXmasEventStatus: async() => {
      return await callApi(GET_METHOD, '/user/get-my-xmas-event-status', {});
    },

};
export default WebApi;
