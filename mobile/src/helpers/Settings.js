import { createContext } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '@helpers/Constants';
import Utils from '@helpers/Utils';
import EncryptedStorage from 'react-native-encrypted-storage';
import { isNumber, isObject, toString } from 'lodash';

const GlobalContext = createContext({});


//Usage:
//Settings.get(Settings.DEBUG_MODE)
//Settings.store(Settings.DEBUG_MODE, true);

//ephemeral settings
class Settings {
    static HAS_CHECKED_SERVER_SYNC = 'has_checked_server_sync';
    static HAS_NOTIFICATION = 'has_notification';
    static DEVICE_ID = 'device_id';
    static DEVICE_PLATFORM = 'device_platform';
    static IS_EMULATOR = 'is_emulator';
    static INSTALLED_APP_VERSION = 'current_app_version';
    static HAD_CHECK_SERVER_SYNC = 'had_check_server_sync';

    // SERVER SETTINGS
    static APP_LAST_SYNC = "app_last_sync";
    static APP_POPUP_STATUS = "app_popup";
    static APP_POPUP_MESSAGE = "app_popup_message";
    static APP_VERSION_ANDROID = "app_version_android";
    static APP_VERSION_IOS = "app_version_ios";
    static APP_POPUP_HAS_LINK = "app_popup_has_link";
    static APP_POPUP_LINK = "app_popup_link";
    static APP_POPUP_HAS_THUMBNAIL = "app_popup_has_thumbnail";
    static ONE_MAP_TOKEN = "one_map_token";
    static ONE_MAP_TOKEN_PCN = "one_map_token_pcn";
    static SYSTEM_UNDER_MAINTENANCE_STATUS = "system_under_maintenance";
    static SYSTEM_UNDER_MAINTENANCE_MESSAGE = "system_under_maintenance_message";
    static DEBUG_MODE = "debug_mode";
    static SPECIAL_EVENT_MODE = "special_event_mode";

    static USER_CREDIT = 'user_credit';

    // LOCATION LOGIC
    static IS_WITHIN_SG = "is_within_sg";
    static IS_WITHIN_C2C = "is_within_c2c";
    static HAD_PROMPT_C2C_MSG = "had_prompt_c2c_msg";
    static HAD_PROMPT_WELCOME_MSG = "had_prompt_welcome_msg";
    static HIDDEN_LOCATION_TODAY_LIST = 'hidden_location_today_list';
    static CURRENT_NEAR_HIDDEN_LOCATION = 'current_near_hidden_location';

    static HAS_CHANGES_IN_FLATLIST_DATA = 'has_changes_in_flatlist_data';

    

    static defaultSettings = [];

    //need to call init() upon app starts
    static init = async() => {
        Utils.log('init Settings');
        this.reset();   
    }

    static reset = () => {
        Utils.log('reset Settings');
        this.defaultSettings = [];
        this.defaultSettings[this.HAD_CHECK_SERVER_SYNC] = false;
        this.defaultSettings[this.HAS_NOTIFICATION] = false;
        // this.defaultSettings[this.DEBUG_MODE] = false; 
        this.defaultSettings[this.DEVICE_ID] = "";
        this.defaultSettings[this.DEVICE_PLATFORM] = "";
        this.defaultSettings[this.IS_EMULATOR] = false;
        this.defaultSettings[this.INSTALLED_APP_VERSION] = "";

        // SERVER SETTINGS
        this.defaultSettings[this.APP_LAST_SYNC] = "";
        this.defaultSettings[this.APP_POPUP_STATUS] = Constants.STATUS_DISABLED;
        this.defaultSettings[this.APP_POPUP_MESSAGE] = "";
        this.defaultSettings[this.APP_VERSION_ANDROID] = "0.0.0";
        this.defaultSettings[this.APP_VERSION_IOS] = "0.0.0";
        this.defaultSettings[this.APP_POPUP_HAS_LINK] = Constants.STATUS_DISABLED;
        this.defaultSettings[this.APP_POPUP_LINK] = "";
        this.defaultSettings[this.APP_POPUP_HAS_THUMBNAIL] = Constants.STATUS_DISABLED;
        this.defaultSettings[this.ONE_MAP_TOKEN] = "";
        this.defaultSettings[this.ONE_MAP_TOKEN_PCN] = "";
        this.defaultSettings[this.SYSTEM_UNDER_MAINTENANCE_STATUS] = Constants.STATUS_DISABLED;
        this.defaultSettings[this.SYSTEM_UNDER_MAINTENANCE_MESSAGE] = "";
        this.defaultSettings[this.DEBUG_MODE] = Constants.STATUS_DISABLED;
        this.defaultSettings[this.SPECIAL_EVENT_MODE] = Constants.STATUS_DISABLED;

        this.defaultSettings[this.USER_CREDIT] = "0";

        // LOCATION LOGIC
        this.defaultSettings[this.IS_WITHIN_SG] = false;
        this.defaultSettings[this.IS_WITHIN_C2C] = false;
        this.defaultSettings[this.HAD_PROMPT_C2C_MSG] = false;
        this.defaultSettings[this.HAD_PROMPT_WELCOME_MSG] = false;
        this.defaultSettings[this.HAD_PROMPT_WELCOME_C2C_MSG] = false;
        this.defaultSettings[this.HIDDEN_LOCATION_TODAY_LIST] = [];
        this.defaultSettings[this.CURRENT_NEAR_HIDDEN_LOCATION] = "";
        //Current near hidden location

        this.defaultSettings[this.HAS_CHANGES_IN_FLATLIST_DATA] = false;        

        global.settings = this.defaultSettings;
    }
    static store = (key, value) => {
        // Utils.log('Settings store:', key, value);
        this.isValidKey(key)
        global.settings[key] = value;
    }
    static get = (key) => {
        this.isValidKey(key)
        return global.settings[key];
    }
    static isValidKey(key) {
        let valid = false;
        Object.keys(this.defaultSettings).forEach(k => {
            if (key == k) {
                valid = true;
            }
        });
        if (!valid) {
            Utils.log('!isValidKey: ' + key);
        }
        return valid;
    }
    static debug = () => {
        //Utils.log(JSON.stringify(global.settings));
        Utils.log(global.settings);
    }
}

//Usage:
//const v = await StoreSettings.get(StoreSettings.FCM_TOKEN);
//await StoreSettings.store(StoreSettings.FCM_TOKEN, 'zxc-zxc-zxc-zcx');
//!!cannot store complicated datatypes...

//persistent settings
class StoreSettings {
    static FIRST_RUN = 'first_run';
    static IS_LOGGED_IN = 'is_logged_in';
    static FCM_TOKEN = 'fcm_token';
    static ACCESS_TOKEN = 'access_token';
    static USER_ID = 'user_id';
    static USER_EMAIL = 'user_email';
    static USER_NICKNAME = 'user_nickname';
    static USER_AVATAR_URL = 'user_avatar_url';
    static USER_CREDIT = 'user_credit';
    static USER_ACCOUNT_STATUS = 'user_account_status';
    static USER_EMAIL_STATUS = 'user_email_status';
    static HIDDEN_LOCATION_TODAY_FOUND_LIST = 'hidden_location_today_found_list';
    static HAD_VISITED_C2C_TODAY = 'had_visited_c2c_today';
    static LAST_DAILY_RESYNC = 'last_daily_resync';
    // USER LOCATION
    static USER_CURRENT_LATITUDE = "user_current_latitude";
    static USER_CURRENT_LONGITUDE = "user_current_longitude";
    
    static defaultSettings = [
        [this.FIRST_RUN, 'true'],
        [this.IS_LOGGED_IN, 'false'],
        [this.FCM_TOKEN, '-'],
        [this.ACCESS_TOKEN, '-'],
        [this.USER_EMAIL, ''],

        [this.USER_ID, '0'],
        [this.USER_EMAIL, ''],
        [this.USER_NICKNAME, ''],
        [this.USER_AVATAR_URL, ''],
        [this.USER_CREDIT, '0'],
        [this.USER_ACCOUNT_STATUS, ''],
        [this.USER_EMAIL_STATUS, ''],

        [this.HIDDEN_LOCATION_TODAY_FOUND_LIST, ''],
        [this.HAD_VISITED_C2C_TODAY, 'false'],
        [this.USER_CURRENT_LATITUDE, ''],
        [this.USER_CURRENT_LONGITUDE, ''],
        [this.LAST_DAILY_RESYNC, ''],
    ];

    //async storage need to call init() upon app starts
    static init = async() => {
        Utils.log('init StoreSettings');
        try {
            const v = await this.get(this.FIRST_RUN);
            Utils.log("init FIRST_RUN: " + v);
            if (v == null) {
                await this.reset();
            }
        } catch (e) {
            Utils.log(e);
            return false;
        }
    }
    static reset = async () => {
        Utils.log('reset StoreSettings');
        try {
            var caches = [];
            caches.push([this.FIRST_RUN, await this.get(this.FIRST_RUN) == null ? "true" : await this.get(this.FIRST_RUN)]);
            caches.push([this.FCM_TOKEN, await this.get(this.FCM_TOKEN) == null ? "-" : await this.get(this.FCM_TOKEN)]);
            caches.push([this.USER_EMAIL, await this.get(this.USER_EMAIL) == null ? "" : await this.get(this.USER_EMAIL)]);
            caches.push([this.HIDDEN_LOCATION_TODAY_FOUND_LIST, await this.get(this.HIDDEN_LOCATION_TODAY_FOUND_LIST) == null ? "" : await this.get(this.HIDDEN_LOCATION_TODAY_FOUND_LIST)]);
            caches.push([this.HAD_VISITED_C2C_TODAY, await this.get(this.HAD_VISITED_C2C_TODAY) == null ? "false" : await this.get(this.HAD_VISITED_C2C_TODAY)]);
            Utils.log("Caches: ", caches);
            
            await EncryptedStorage.clear();

            for (let i = 0; i < caches.length; i++) {
                const cache = caches[i];
                await this.store(cache[0], cache[1]);
            }
            // Congrats! You've just cleared the device storage!
            return true;
        } catch (error) {
            // There was an error on the native side
            Utils.log(error);
            return false;
        }




        // try {
        //     var cache = [];
        //     cache.push([this.FIRST_RUN, await AsyncStorage.getItem(this.FIRST_RUN) == null ? "true" : await AsyncStorage.getItem(this.FIRST_RUN)]);
        //     cache.push([this.FCM_TOKEN, await AsyncStorage.getItem(this.FCM_TOKEN) == null ? "-" : await AsyncStorage.getItem(this.FCM_TOKEN)]);
        //     cache.push([this.USER_EMAIL, await AsyncStorage.getItem(this.USER_EMAIL) == null ? "" : await AsyncStorage.getItem(this.USER_EMAIL)]);
        //     cache.push([this.HIDDEN_LOCATION_TODAY_FOUND_LIST, await AsyncStorage.getItem(this.HIDDEN_LOCATION_TODAY_FOUND_LIST) == null ? "" : await AsyncStorage.getItem(this.HIDDEN_LOCATION_TODAY_FOUND_LIST)]);
        //     cache.push([this.HAD_VISITED_C2C_TODAY, await AsyncStorage.getItem(this.HAD_VISITED_C2C_TODAY) == null ? "false" : await AsyncStorage.getItem(this.HAD_VISITED_C2C_TODAY)]);
        //     // Utils.log("Cache: ", cache);
        //     await AsyncStorage.multiSet(this.defaultSettings);            
        //     await AsyncStorage.multiSet(cache);
        //     // this.debug();

        //     // await AsyncStorage.multiSet(this.defaultSettings);
        //     return true;
        // } catch (e) {
        //     Utils.log(e);
        //     return false;
        // }            
    }
    static store = async (key, value) => {
        try {
            var val = value;
            if(isObject(value)){
                val = JSON.stringify(value);
            } else if(isNumber(value)){
                val = toString(value);
            }
            await EncryptedStorage.setItem(key, val);
            // Congrats! You've just stored your first value!
            return true;
        } catch (error) {
            // There was an error on the native side
            Utils.log(error);
            return false;
        }




        // Utils.log('AsyncStorage store:', key, value);
        // if (value === null) value = '';
        // const valid = this.isValidKey(key);
        // if (!valid) { return null; }
        // try {
        //     await AsyncStorage.setItem(key, value + '');
        //     return true;
        // } catch (e) {
        //     Utils.log(e);
        //     return false;
        // }
    }
    static get = async (key) => {
        try {   
            const session = await EncryptedStorage.getItem(key);
        
            if (session !== undefined) {
                // Congrats! You've just retrieved your first value!
                return session;
            } 
            return null;
        } catch (error) {
            // There was an error on the native side
            Utils.log(error);
            return null;
        }




        // this.isValidKey(key);
        // try {
        //     return await AsyncStorage.getItem(key);
        // } catch (e) {
        //     Utils.log(e);
        //     return null;
        // }
    }
    static isValidKey(key) {
        let valid = false;
        this.defaultSettings.forEach(arr => {
            if (key == arr[0]) {
                valid = true;
            }
        });
        if (!valid) {
            Utils.log('!isValidKey: ' + key);
        }
        return valid;
    }
    static clear = async() => {
        await AsyncStorage.clear();
    }
    static debug = () => {
        AsyncStorage.getAllKeys((err, keys) => {
            AsyncStorage.multiGet(keys, (err, stores) => {
                Utils.log(JSON.stringify(stores));
            });
        });
    }
}


export { Settings, StoreSettings, GlobalContext };