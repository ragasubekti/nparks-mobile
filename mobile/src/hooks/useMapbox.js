import { Text, Button, View, StyleSheet, Image, TouchableOpacity, Linking, Platform } from 'react-native';
import React, { useEffect, useRef, useState, useContext } from 'react';
import Constants from '@helpers/Constants';
import { StyleConstant } from '@assets/MyStyle';
import WebApi from '@helpers/WebApi';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Supercluster from 'supercluster';
import Utils from "@helpers/Utils";
import { useNavigation } from 'react-navigation-hooks';

import imgGridPattern from '@assets/img/grid-pattern.jpg';
import imgRedFlag from '@assets/img/red-flag.png';
import imgPopupTop from '@assets/img/feature-popup-white.png';
import imgPopupClose from '@assets/img/feature-popup-close.png';
import imgPopupBottom_brown from '@assets/img/feature-popup-brown.png';
import imgPopupBottom_orange from '@assets/img/feature-popup-orange.png';
import imgPopupBottom_red from '@assets/img/feature-popup-red.png';
import imgPopupBottom_green from '@assets/img/feature-popup-green.png';
import imgPhotoBg from '@assets/img/user-post-marker-bg.png';
import imgPhotoCircle from '@assets/img/user-post-marker-cicrle.png';
import imgXmasTree from '@assets/img/xmas-tree.png';

import icon_tenant from '@assets/img/icon-tenant.png';
import icon_poi from '@assets/img/icon-poi.png';
import icon_bike_rent from '@assets/img/icon-bike-rent.png';
import icon_carpark from '@assets/img/icon-carpark.png';
import icon_dog_area from '@assets/img/icon-dog-area.png';
import icon_entry from '@assets/img/icon-entry.png';
import icon_fitness from '@assets/img/icon-fitness.png';
import icon_heritage_tree from '@assets/img/icon-heritage-tree.png';
import icon_lookout from '@assets/img/icon-lookout.png';
import icon_picnic from '@assets/img/icon-picnic.png';
import icon_playground from '@assets/img/icon-playground.png';
import icon_pond from '@assets/img/icon-pond.png';
import icon_reflexology from '@assets/img/icon-reflexology.png';
import icon_restaurant from '@assets/img/icon-restaurant.png';
import icon_restroom from '@assets/img/icon-restroom.png';
import icon_sandplay from '@assets/img/icon-sandplay.png';
import icon_shelter from '@assets/img/icon-shelter.png';
import icon_shower from '@assets/img/icon-shower.png';
import icon_tree from '@assets/img/icon-tree.png';
import icon_venues from '@assets/img/icon-venues.png';
import icon_water_point from '@assets/img/icon-water-point.png';
import icon_wheelchair_access from '@assets/img/icon-wheelchair-access.png';


import c2cRouteGeoJson from '@assets/geojson/c2cRoute.json';
import checkpointGeoJson from '@assets/geojson/checkpoint.json';
import userPostLocationGeoJson from '@assets/geojson/userPostLocation.json';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {ImagePath} from '@assets/imagePaths';

export default useMapbox = () => {
    const zoomSummary = 11;
    const zoomBbox = 13;
    const mapViewRef = useRef(null);
    const cameraRef = useRef(null);
    const [selectedItem, setSelectedItem] = useState(null);
    const [followMap, setFollowMap] = useState(false);

    const [featureC2cRoute, setFeatureC2cRoute] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    const [featureC2cBorder, setFeatureC2cBorder] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    const [featurePopup, setFeaturePopup] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    const [featureCheckpoint, setFeatureCheckpoint] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    const [featureDetail, setfeatureDetail] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    const [featureNavigateRoute, setFeatureNavigateRoute] = useState(MapboxGL.geoUtils.makeFeatureCollection());

    const [mapLoaded, setMapLoaded] = useState(false);
    const [showPhotos, setShowPhotos] = useState(true);
    const [imageCheckpoint, setImageCheckpoint] = useState({});
    const [imagePhotoSummary, setImagePhotoSummary] = useState({});
    const [imagePhotoBbox, setImagePhotoBbox] = useState({});
    const [imageDetail, setimageDetail] = useState({}); //for tenant & poi photo
    const imageBottomColor = { imgPopupBottom_brown: imgPopupBottom_brown, imgPopupBottom_orange: imgPopupBottom_orange, imgPopupBottom_red: imgPopupBottom_red, imgPopupBottom_green: imgPopupBottom_green };
    const imageAmenity = { icon_tenant: icon_tenant, icon_poi: icon_poi, icon_bike_rent: icon_bike_rent, icon_carpark: icon_carpark, icon_dog_area: icon_dog_area, icon_entry: icon_entry, icon_fitness: icon_fitness, icon_heritage_tree: icon_heritage_tree, icon_lookout: icon_lookout, icon_picnic: icon_picnic, icon_playground: icon_playground, icon_pond: icon_pond, icon_reflexology: icon_reflexology, icon_restaurant: icon_restaurant, icon_restroom: icon_restroom, icon_sandplay: icon_sandplay, icon_shelter: icon_shelter, icon_shower: icon_shower, icon_tree: icon_tree, icon_venues: icon_venues, icon_water_point: icon_water_point, icon_wheelchair_access: icon_wheelchair_access };

    const defaultMyLocation = {
        timestamp: 1568716336000,
        coords: { accuracy: 0, altitude: 0, heading: 353, latitude: Constants.SINGAPORE_CENTER[1], longitude: Constants.SINGAPORE_CENTER[0], speed: 0 }
    };
    const [myLocation, setMyLocation] = useState(defaultMyLocation);
    const [superClusterPhotoSummary, setSuperClusterPhotoSummary] = useState(null);
    const [superClusterPhotoBbox, setSuperClusterPhotoBbox] = useState(null);

    const [superClusterSummaryCluster, setSuperClusterSummaryCluster] = useState([]);
    const [superClusterBboxCluster, setSuperClusterBboxCluster] = useState([]);
    const [mapboxStyleUrl, setMapboxStyleUrl] = useState({});

    const { navigate, navigationOptions } = useNavigation();
    const [touchedLong, setTouchedLong] = useState("");
    const [touchedLat, setTouchedLat] = useState("");
    const [touchedData, setTouchedData] = useState({});
    const [hasLocationPermission, setHasLocationPermission] = useState(false);
    const { toggleActivityIndicator, showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
    const [ inEventMode, setInEventMode] = useState(false);

    useEffect(() => {
        checkGpsSettings();
        MapboxGL.setAccessToken(Constants.MAPBOX_KEY);
        if(Platform.OS === "ios") {
            setMapboxStyleUrl({styleURL: MapboxGL.StyleURL.Dark});
        }
        if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2020_XMAS){
            setInEventMode(true);
        }
        //call onDidFinishLoadingMap
        //handeleOnDidFinishLoadingMap();
        return function cleanup() { }
    }, []);

    handeleOnDidFinishLoadingMap = async () => {
        setMapLoaded(true);
        Utils.log("handeleOnDidFinishLoadingMap");

        //border
        let polygon = Constants.C2C_BORDER_POLYGON_ARR;
        const fc = { type: "FeatureCollection", features: [{ type: "Feature", geometry:{ type: "Polygon", coordinates:[polygon] }}] }
        setFeatureC2cBorder(fc)

        //TODO::show loading status...
        WebApi.getC2cRouteGeoJson()
            .then((res) => { setFeatureC2cRoute(res); })
            .catch((e) => { Utils.error(e) });

        WebApi.getCheckpointGeoJson()
            .then((res) => { initCheckpoint(res); })
            .catch((e) => { Utils.error(e) });

        WebApi.getPostLocationSummaryGeoJson()
            .then((res) => { initPhoto(res); })
            .catch((e) => { Utils.error(e) });

        try {
            const res1 = await WebApi.getTenantGeoJson();
            const res2 = await WebApi.getPoiGeoJson();
            const res3 = await WebApi.getTreeGeoJson();
            const res4 = await WebApi.getAmenityJson();
            //Utils.log(res1,res2,res3,res4);
            const mergeRes = { type: 'FeatureCollection', features: [...res1.features, ...res2.features, ...res3.features, ...res4.features] };
            initMapDetail(mergeRes);
        } catch (e) {
            Utils.error("Problem init map details", e);
        }

        cameraRef.current.moveTo(Constants.SINGAPORE_CENTER, 100);
        //Utils.log("UserTrackingModes", MapboxGL.UserTrackingModes.FollowWithHeading);
    }
    initCheckpoint = (geojson) => {
        const { photos, featureCollection } = prepareFeatureFromGeoJson(geojson);
        setFeatureCheckpoint(featureCollection);
        setImageCheckpoint(photos);
        //Utils.log("initCheckpoint", geojson, featureCollection);
    }
    initPhoto = (geojson) => {
        const { photos, featureCollection } = prepareFeatureFromGeoJson(geojson);
        setImagePhotoSummary(photos);
        //Utils.log("initPhotoSummary & initPhotoBbox", geojson, featureCollection);

        //ref: https://www.npmjs.com/package/supercluster
        const cluster = new Supercluster({
            radius: 100, maxZoom: 20,
            map: (props) => ({ imgUrlId: props.imgUrlId, posts: props.posts }),
            reduce: (accumulated, props) => { accumulated.imgUrlId = props.imgUrlId; accumulated.posts += props.posts; }
        });
        cluster.load(featureCollection.features);
        setSuperClusterPhotoSummary(cluster);

        const cluster2 = new Supercluster({
            radius: 100, maxZoom: 20,
            map: (props) => ({ imgUrlId: props.imgUrlId }),
            reduce: (accumulated, props) => { accumulated.imgUrlId = props.imgUrlId; }
        });
        setSuperClusterPhotoBbox(cluster2);
    }
    initMapDetail = (geojson) => {
        const { photos, featureCollection } = prepareFeatureFromGeoJson(geojson);
        setfeatureDetail(featureCollection);
        setimageDetail(photos);
    }
    prepareFeatureFromGeoJson = (geojson) => {
        let photos = {}, features = [];
        geojson.features.forEach((obj) => {
            let featureType = obj.properties.featureType;
            featureType = featureType == "HeritageTrees" ? "tree" : featureType; //rename to fit local usage
            featureType = featureType ? featureType : "amenity"; //imaven geojson has no feaureType property

            if (featureType != "tree" || featureType != "amenity") {
                const t = (featureType == "postLocation" || featureType == "postLocationSummary") ? "thumbnail_square" : "thumbnail";
                const url = obj.properties.thumbnail_base_url + "/" + t + "/" + obj.properties.thumbnail_path;
                photos[featureType + "_" + obj.properties.id] = { uri: url };
            }

            const p = prepareFeatureProperty(featureType, obj.properties);
            const feature = {
                "type": "Feature",
                "properties": p,
                "geometry": {
                    "type": "Point",
                    "coordinates": [obj.geometry.coordinates[0], obj.geometry.coordinates[1]]
                }
            }
            features.push(feature);
        });
        const fc = MapboxGL.geoUtils.makeFeatureCollection(features);
        return { photos: photos, featureCollection: fc };
    }
    prepareFeatureProperty = (featureType, properties) => {
        const n = properties.name || "...";
        let label = n.length > 35 ? n.substring(0, 35) + "..." : n;
        const p = {
            "featureType": featureType,
            "id": properties.id,
            "popupLabel": label,
        };

        if (featureType == "checkpoint") {
            p["popupType"] = 1;
            p["popupTitle"] = "Checkpoint " + properties.map_flag_label;
            p["map_flag_label"] = properties.map_flag_label;
            p["imgUrlId"] = "checkpoint_" + properties.id;
            p["color"] = "brown";
        } else if (featureType == "postLocationSummary") {
            p["posts"] = properties.posts ? properties.posts : 1;
            p["imgUrlId"] = "postLocationSummary_" + properties.id;
        } else if (featureType == "postLocation") {
            p["posts"] = properties.posts ? properties.posts : 1;
            p["imgUrlId"] = "postLocation_" + properties.id;
        } else if (featureType == "tenant") {
            p["popupType"] = 2;
            p["popupTitle"] = "Park Tenant";
            p["imgUrlId"] = "tenant_" + properties.id;
            p["icon"] = "icon_tenant";
            p["color"] = "orange";
        } else if (featureType == "poi") {
            p["popupType"] = 2;
            p["popupTitle"] = "Place Of Interest";
            p["imgUrlId"] = "poi_" + properties.id;
            p["icon"] = "icon_poi";
            p["color"] = "red";
        } else if (featureType == "tree") {
            p["popupType"] = 3;
            p["popupTitle"] = "Heritage Tree";
            p["hasPopupPhoto"] = true;
            p["icon"] = "icon_heritage_tree";
            p["color"] = "green";
        } else if (featureType == "amenity") {
            p["popupType"] = 3;
            p["popupTitle"] = "Park Amenity";
            p["hasPopupPhoto"] = true;
            let str = properties.symbol;
            str = str.replace('-', '_');
            p["icon"] = "icon_" + str;
            str = str.replace('_', ' ');
            p["popupLabel"] = Utils.uppercaseFirstChar(str);;
            p["color"] = "green";
            //Utils.log("symbol", str);
        }
        return p;
    }


    handleOnRegionDidChange = (e) => {
        const zoom = e.properties.zoomLevel;
        const bounds = e.properties.visibleBounds;
        const westLng = bounds[1][0], southLat = bounds[1][1], eastLng = bounds[0][0], northLat = bounds[0][1];

        if (zoom >= zoomSummary && zoom < zoomBbox && superClusterPhotoSummary) {
            setSuperClusterSummaryCluster(superClusterPhotoSummary.getClusters([westLng, southLat, eastLng, northLat], Math.round(zoom)));
        }
        if (zoom >= zoomBbox && superClusterPhotoBbox) {
            //Utils.log("showPhotoBbox", showPhotoBbox);
            WebApi.getPostLocationBboxGeoJson(westLng, southLat, eastLng, northLat)
                .then((geojson) => {
                    const { photos, featureCollection } = prepareFeatureFromGeoJson(geojson, "userPostLocation", true);
                    setImagePhotoBbox(arr => { return { ...arr, ...photos }; });
                    //setImagePhotoBbox(photos);
                    //#loynote: recluster supercluster on every region change, cannot append...
                    //ref: https://github.com/mapbox/supercluster/issues/19
                    superClusterPhotoBbox.load(featureCollection.features);
                    setSuperClusterBboxCluster(superClusterPhotoBbox.getClusters([westLng, southLat, eastLng, northLat], Math.round(zoom)));
                })
                .catch((e) => { Utils.error(e); });
        } else {
            setImagePhotoBbox({})
        }

        /**/
        //Utils.log("zoom", zoom);
        //Utils.log("handleOnRegionDidChange",e);

        //const c = e.properties.visibleBounds;
        //restrictBounds(c);
    }
    restrictBounds = (coords) => {
        //to handle maxbounds, move map back when scroll out of sg
        const maxBounds = Constants.SINGAPORE_BOUNDS
        const center = [(coords[0][0] + coords[1][0]) / 2, (coords[0][1] + coords[1][1]) / 2];
        let move = false;
        if (coords[0][0] > maxBounds[0][0]) {
            // NE ->
            move = true;
            center[0] -= coords[0][0] - maxBounds[0][0];
        }
        if (coords[0][1] > maxBounds[0][1]) {
            // NE ^
            move = true;
            center[1] -= coords[0][1] - maxBounds[0][1];
        }
        if (coords[1][0] < maxBounds[1][0]) {
            // SW <-
            move = true;
            center[0] += maxBounds[1][0] - coords[1][0];
        }
        if (coords[1][1] < maxBounds[1][1]) {
            // SW v
            move = true;
            center[1] += maxBounds[1][1] - coords[1][1];
        }
        if (move) {
            //Utils.log(move);
            cameraRef.current.moveTo(center, 100);
        }
    }
    handleOnPress = async (e) => {
        setFollowMap(false);
        /*
        const {screenPointX, screenPointY} = e.properties;
        const featureCollection = await mapViewRef.current.queryRenderedFeaturesAtPoint([screenPointX, screenPointY], null,  ['checkpoint-flag-layer']);
        //Utils.log(featureCollection);
        if (featureCollection.features.length <= 0) {
            //hide popup
            setFeaturePopup(MapboxGL.geoUtils.makeFeatureCollection());
        }
        */
    }
    handleOnPressDetail = async (e) => {
        const feature = e.nativeEvent.payload;
        const fc = MapboxGL.geoUtils.makeFeatureCollection([feature]);
        setFeaturePopup(fc);
        setTouchedLong(feature.geometry.coordinates[0]);
        setTouchedLat(feature.geometry.coordinates[1]);
        setSelectedItem(feature);
        const c = feature.geometry.coordinates
        cameraRef.current.flyTo(c, 200);
        //Utils.log("handleOnPressDetail", feature);
        if(feature.properties.featureType === "poi"){
            var res = await WebApi.getPoiDetail(feature.properties.id);
            setTouchedData(res.data);
        } else{
            var res = await WebApi.getTenantDetail(feature.properties.id);
            setTouchedData(res.data);
        }
    }
    handleOnPressCheckpoint = async (e) => {
        const feature = e.nativeEvent.payload;
        const fc = MapboxGL.geoUtils.makeFeatureCollection([feature]);
        setFeaturePopup(fc);
        setTouchedLong(feature.geometry.coordinates[0]);
        setTouchedLat(feature.geometry.coordinates[1]);
        setSelectedItem(feature);
        const c = feature.geometry.coordinates
        cameraRef.current.flyTo(c, 200);
        //Utils.log("handleOnPressCheckpoint", feature);
    }
    handleOnPressPhotoSummary = async (e) => {
        const feature = e.nativeEvent.payload;
        Utils.log("handleOnPressPhotoSummary", feature);
        const lng = feature.geometry.coordinates[0];
        const lat = feature.geometry.coordinates[1];
        const number_of_posts = feature.properties.posts;
        navigate("exploreCommunityViewAll", { lng: lng, lat: lat, number_of_posts: number_of_posts, title: "Cluster Posts" })
    }
    handleOnPressPhoto = async (e) => {
        const feature = e.nativeEvent.payload;
        Utils.log("handleOnPressPhoto", feature);
        const lng = feature.geometry.coordinates[0];
        const lat = feature.geometry.coordinates[1];
        const number_of_posts = feature.properties.point_count;
        navigate("exploreCommunityViewAll", { lng: lng, lat: lat, number_of_posts: number_of_posts, title: "Cluster Posts" })
    }
    handleOnPressPopup = async (e) => {
        const feature = e.nativeEvent.payload;
        Utils.log("handleOnPressPopup", feature);
    }
    handleOnPressPopupClose = (e) => {
        const fc = MapboxGL.geoUtils.makeFeatureCollection();
        setFeaturePopup(fc);
        setFeatureNavigateRoute(fc);
        setSelectedItem(null);
    }
    handleOnPressMyLocation = (e) => {
        const c = [myLocation.coords.longitude, myLocation.coords.latitude];
        cameraRef.current.flyTo(c, 200);
        setFollowMap(true);
        //cameraRef.current.setZoom(12);
        Utils.log("handleOnPressMyLocation", c);
    }
    handleOnUpdateUserLocation = (e) => {
        // Utils.log(e);
        //Utils.log("handleOnUpdateUserLocation", e);
        if(e == undefined || e == null){
            checkGpsSettings();
        } else {
            if (!myLocation || myLocation.coords.latitude != e.coords.latitude) {
                setMyLocation(e);
                StoreSettings.store(StoreSettings.USER_CURRENT_LATITUDE, e.coords.latitude);
                StoreSettings.store(StoreSettings.USER_CURRENT_LONGITUDE, e.coords.longitude);
            }
        }
    }

    handleNavigateToDetail = async(e) =>{
        const lat = await StoreSettings.get(StoreSettings.USER_CURRENT_LATITUDE);
        const long = await StoreSettings.get(StoreSettings.USER_CURRENT_LONGITUDE);
        let res = await WebApi.getOneMapRoutePcn( lat + "," + long , touchedLat + "," + touchedLong );
        let beforeRoutePoints, routePoints, afterRoutePoints = [];

        if (res.beforeRoute && res.beforeRoute.route_itineraries) {
            let beforeRoute = res.beforeRoute.route_itineraries[0].legs;
            for (var i = 0; i < beforeRoute.length; i++) {
                var geometry = beforeRoute[i].legGeometry.points;
                beforeRoutePoints = Utils.polylineDecode(geometry);
            }
        }

        routePoints = Utils.polylineDecode(res.route.route_geometry);

        if (res.afterRoute && res.afterRoute.route_itineraries) {
            let afterRoute = res.afterRoute.route_itineraries[0].legs;
            for (var i = 0; i < afterRoute.length; i++) {
                var geometry = afterRoute[i].legGeometry.points;
                afterRoutePoints = Utils.polylineDecode(geometry);
            }
        }
        const fc = {
            "type": "FeatureCollection",
            "features": [
              {
                "type": "Feature",
                "properties": {},
                "geometry": {
                  "type": "LineString",
                  "coordinates": [...beforeRoutePoints, ...routePoints, ...afterRoutePoints]
                }
              }
            ]
          }
          setFeatureNavigateRoute(fc);
    }

    undrawPolyLine = () =>{
        const fc = MapboxGL.geoUtils.makeFeatureCollection();
        setFeatureNavigateRoute(fc);
    }
    //loynote: null styleURL will cause symbols to not render
    //styleURL={null}

    checkGpsSettings = async() =>{
        let result;
        if (Platform.OS === "android") {
            let fineResult = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
            let coarseResult = await check(PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION);
            result = fineResult == RESULTS.GRANTED || coarseResult == RESULTS.GRANTED;
        } else {
            let always = await check(PERMISSIONS.IOS.LOCATION_ALWAYS);
            let when_in_use = await check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            if(always == RESULTS.BLOCKED || when_in_use == RESULTS.BLOCKED){
                openAppSettings=()=>{
                  Linking.openURL('app-settings:');
                }
                showCustomDialog({body:"Coast-to-Coast would like access to your Location", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{openAppSettings}, hasCancelButton:true});
                return;
            }
            result = always == RESULTS.GRANTED || when_in_use == RESULTS.GRANTED;
        }
        
        if(result){
            // request user to enable gps settings
            // if(location == disabled){
                // if (Platform.OS === "android") {
                    // TODO:: yarn add react-native-android-location-enabler
                // } else {
                    // Linking.openURL('app-settings:');
                // }
            // }
            setHasLocationPermission(true);
        } else {
            showCustomDialog({body:"Coast-to-Coast would like access to your Location", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{requestForLocationPermissions}, hasCancelButton:true});
        }
    }
    
    requestForLocationPermissions = ()=>{
        requestForPermissions4()
        .then((status)=>{
            Utils.log("useMapBox - hasRequiredPermissions: " + status);
            if(status){
                setHasLocationPermission(true);
            }
        })
    }
    requestForPermissions4 = async() =>{
        var result = false;
        try{
          if (Platform.OS === "android") {
            var fineResult = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
            var coarseResult = await request(PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION);
            result = fineResult == RESULTS.GRANTED || coarseResult == RESULTS.GRANTED;
          } else {
            var alwaysResult = await request(PERMISSIONS.IOS.LOCATION_ALWAYS);
            var inUseResult = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            result = alwaysResult == RESULTS.GRANTED || inUseResult == RESULTS.GRANTED;
          }
          
        } catch (e) {
          Utils.log(e);
        }
        return result;
    }
    navigateToExplore = () =>{
        navigate("explore");
    }
    
    renderUserLocation = ()=>{
        if(hasLocationPermission){
            return (
                <MapboxGL.UserLocation visible={true} onUpdate={handleOnUpdateUserLocation} />
            )
        }
    }

    renderPhotoLayers = ()=>{
        if(showPhotos){
            return(
                <View>
                    { /*Photo Summary*/}
                    <MapboxGL.Images images={imagePhotoSummary} />
                    <MapboxGL.ShapeSource id="photo-sumamry-source" cluster={false} onPress={handleOnPressPhotoSummary}
                        shape={{ type: 'FeatureCollection', features: superClusterSummaryCluster }}  >
                        <MapboxGL.SymbolLayer minZoomLevel={zoomSummary} maxZoomLevel={zoomBbox} id="photo-bg-layer" style={styles.photoBg} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomSummary} maxZoomLevel={zoomBbox} id="photo-layer" style={styles.photo} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomSummary} maxZoomLevel={zoomBbox} id="photo-circle-layer" filter={['>', 'posts', 1]} style={styles.photoCircle} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomSummary} maxZoomLevel={zoomBbox} id="photo-text-layer" filter={['>', 'posts', 1]} style={styles.photoText} />
                    </MapboxGL.ShapeSource>
    
                    {/* Photo Bbox */}
                    <MapboxGL.Images images={imagePhotoBbox} />
                    <MapboxGL.ShapeSource id="photo-bbox-source" cluster={false} onPress={handleOnPressPhoto}
                        shape={{ type: 'FeatureCollection', features: superClusterBboxCluster }} >
                        <MapboxGL.SymbolLayer minZoomLevel={zoomBbox} id="photo-bbox-bg-layer" style={styles.photoBg} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomBbox} id="photo-bbox-layer" style={styles.photo} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomBbox} id="photo-bbox-circle-layer" filter={['>', 'point_count', 1]} style={styles.photoCircle} />
                        <MapboxGL.SymbolLayer minZoomLevel={zoomBbox} id="photo-bbox-text-bbox-layer" filter={['>', 'point_count', 1]} style={styles.photoBboxText} />
                    </MapboxGL.ShapeSource>
                </View>
            );
        }
    }

    renderMapStuff = () => {
        return (
            <View style={{ flex: 1, opacity: mapLoaded? 100 : 0  }}>

                <MapboxGL.MapView ref={mapViewRef} {...mapboxStyleUrl} style={{ flex: 1 }} onDidFinishLoadingMap={handeleOnDidFinishLoadingMap} onPress={handleOnPress} onTouchStart={handleOnPress}
                    onRegionDidChange={handleOnRegionDidChange} logoEnabled={true} attributionEnabled={false} >

                    {/* followUserLocation={true} followUserMode={MapboxGL.Camera.UserTrackingModes.FollowWithHeading} */}
                    <MapboxGL.Camera ref={cameraRef} zoomLevel={11} minZoomLevel={10.5} maxZoomLevel={20} />

                    <MapboxGL.RasterSource id="onemap-source" tileSize={256} url={"https://maps-a.onemap.sg/v3/Grey_HD/{z}/{x}/{y}.png"} >
                        <MapboxGL.BackgroundLayer id="bg-layer" style={styles.background} />
                        <MapboxGL.RasterLayer id="onemap-layer" sourceLayerID='onemap-source' />
                    </MapboxGL.RasterSource>

                    {/* C2C border layer */}
                    <MapboxGL.ShapeSource id="c2c-border-source" shape={featureC2cBorder}>
                        <MapboxGL.FillLayer id="c2c-border-layer" style={styles.c2cBorder} />
                    </MapboxGL.ShapeSource>

                    {renderUserLocation()}

                    {/* C2C route layer */}
                    <MapboxGL.ShapeSource id="c2c-route-source" shape={featureC2cRoute}>
                        <MapboxGL.LineLayer id="c2c-route-layer" style={styles.c2cRoute} />
                    </MapboxGL.ShapeSource>                    

                    {/* Nav route layer */}
                    <MapboxGL.ShapeSource id="navigate-route-source" shape={featureNavigateRoute}>
                        <MapboxGL.LineLayer id="navigate-route-bg-layer" style={styles.navigateRouteBg} />
                        <MapboxGL.LineLayer id="navigate-route-layer" style={styles.navigateRoute} />
                    </MapboxGL.ShapeSource>

                    { /*Map Details - Tenant, Poi, Heritage Tree, Amenity */ }
                    <MapboxGL.Images images={imageDetail} />
                    <MapboxGL.Images images={{ icon_tenant: icon_tenant, icon_poi: icon_poi, icon_tree: icon_heritage_tree, ...imageAmenity }} />
                    <MapboxGL.ShapeSource id="detail-source" shape={featureDetail} onPress={handleOnPressDetail}>
                        <MapboxGL.SymbolLayer id="detail-layer" style={styles.detail} />
                    </MapboxGL.ShapeSource>

                    {/* to put a state variable passed in from screenExplore to not render this 2 layers */}
                    { renderPhotoLayers() }

                    { /*Detail Focus*/}
                    <MapboxGL.ShapeSource id="detail-focus-source" shape={featurePopup} onPress={handleOnPressDetail}>
                        <MapboxGL.SymbolLayer id="detail-focus-layer" style={styles.detailFocus} />
                    </MapboxGL.ShapeSource>

                    { /*Checkpoint*/}
                    <MapboxGL.Images images={imageCheckpoint} />
                    <MapboxGL.ShapeSource id="checkpoint-source" shape={featureCheckpoint} onPress={handleOnPressCheckpoint}>
                        <MapboxGL.SymbolLayer id="checkpoint-flag-layer" style={inEventMode?styles.xmasTree : styles.redFlag} />
                        <MapboxGL.SymbolLayer id="checkpoint-text-layer" style={inEventMode?styles.xmasTreeText : styles.redFlagText} />
                    </MapboxGL.ShapeSource>

                    { /*Popup - display 3 types of popup using filter... */}
                    <MapboxGL.Images images={imageBottomColor} />
                    <MapboxGL.ShapeSource id="popup-source" shape={featurePopup} onPress={handleOnPressPopupClose}>
                        <MapboxGL.SymbolLayer id="popup-image-layer1" style={inEventMode? styles.popupImageXmas : styles.popupImage1} filter={['==', "popupType", 1]} />
                        <MapboxGL.SymbolLayer id="popup-close-layer1" style={inEventMode? styles.popupCloseXmas : styles.popupClose1} filter={['==', "popupType", 1]} />
                        <MapboxGL.SymbolLayer id="popup-top-layer1" style={inEventMode? styles.popupTopXmas : styles.popupTop1} filter={['==', "popupType", 1]} />
                        <MapboxGL.SymbolLayer id="popup-bottom-layer1" style={inEventMode? styles.popupBottomXmas : styles.popupBottom1} filter={['==', "popupType", 1]} />
                        <MapboxGL.SymbolLayer id="popup-title1" style={inEventMode? styles.popupTitleXmas : styles.popupTitle1} filter={['==', "popupType", 1]} />
                        <MapboxGL.SymbolLayer id="popup-label-layer1" style={inEventMode? styles.popupLabelXmas : styles.popupLabel1} filter={['==', "popupType", 1]} />

                        <MapboxGL.SymbolLayer id="popup-image-layer2" style={styles.popupImage2} filter={['==', "popupType", 2]} />
                        <MapboxGL.SymbolLayer id="popup-close-layer2" style={styles.popupClose2} filter={['==', "popupType", 2]} />
                        <MapboxGL.SymbolLayer id="popup-top-layer2" style={styles.popupTop2} filter={['==', "popupType", 2]} />
                        <MapboxGL.SymbolLayer id="popup-bottom-layer2" style={styles.popupBottom2} filter={['==', "popupType", 2]} />
                        <MapboxGL.SymbolLayer id="popup-title2" style={styles.popupTitle2} filter={['==', "popupType", 2]} />
                        <MapboxGL.SymbolLayer id="popup-label-layer2" style={styles.popupLabel2} filter={['==', "popupType", 2]} />

                        <MapboxGL.SymbolLayer id="popup-close-layer3" style={styles.popupClose3} filter={['==', "popupType", 3]} />
                        <MapboxGL.SymbolLayer id="popup-top-layer3" style={styles.popupTop3} filter={['==', "popupType", 3]} />
                        <MapboxGL.SymbolLayer id="popup-bottom-layer3" style={styles.popupBottom3} filter={['==', "popupType", 3]} />
                        <MapboxGL.SymbolLayer id="popup-title3" style={styles.popupTitle3} filter={['==', "popupType", 3]} />
                        <MapboxGL.SymbolLayer id="popup-label-layer3" style={styles.popupLabel3} filter={['==', "popupType", 3]} />
                    </MapboxGL.ShapeSource>

                </MapboxGL.MapView>
                {/* <TouchableOpacity onPress={handleOnPressMyLocation}>
                    <View style={{width: 40, height: 40, borderRadius: 20, backgroundColor: 'white'}}>
                        <Icon name="crosshairs" size={20} color={"#00ffff"}/>
                    </View>
                </TouchableOpacity> */}
                {/* <Button title="center" onPress={handleOnPressMyLocation} /> */}
            </View>
        );
    }
    //child view in symbol layer not working??
    //ref:https://github.com/react-native-mapbox-gl/maps/issues/248
    //ref:https://github.com/react-native-mapbox-gl/maps/issues/266


    return {
        renderMapStuff, cameraRef, myLocation, handleOnPressMyLocation, handleNavigateToDetail, selectedItem, touchedData, followMap, undrawPolyLine, showPhotos, setShowPhotos
    }
}

//ref: https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions
//ref: https://github.com/nitaliano/react-native-mapbox-gl/issues/706

/*
//mapbox runtime styling not working for icon offset...
const casePopupBottom = 
["case", 
    [ "==", ["get","popupType"], 1], [80, -185],
    [ "==", ["get","popupType"], 2], [0, -50],
    [ "==", ["get","popupType"], 3], [0, -100],
    //default:
    [0, -150]
];
*/

const popupImage = { iconImage: ['get', 'imgUrlId'], iconAllowOverlap: true, iconSize: 0.208, iconKeepUpright: true, iconAnchor: "bottom" };
const popupTop = { iconImage: imgPopupTop, iconAllowOverlap: true, iconSize: 0.35, iconAnchor: "bottom" };
const popupClose = { iconImage: imgPopupClose, iconAllowOverlap: true, iconSize: 0.35, iconAnchor: "bottom" };
const popupTitle = { textField: ["get", "popupTitle"], textAllowOverlap: true, textSize: 14, textColor: StyleConstant.primaryColor, textAnchor: 'center', textSize: 13 };
const popupLabel = { textField: ["get", "popupLabel"], textAllowOverlap: true, textSize: 12, textColor: "#ffffff" };
const popupBottom = { iconImage: ["concat", "imgPopupBottom_", ['get', 'color']], iconAllowOverlap: true, iconSize: 0.35, iconAnchor: "bottom" };
const detailCommon = { iconImage: ['get', 'icon'], iconSize: 0.5, iconAnchor: "center" };

const styles = {
    background: { backgroundColor: "black", backgroundPattern: imgGridPattern },
    c2cBorder: { fillColor: 'rgba(233, 222, 193, 0.6)' },
    c2cRoute: { lineColor: 'rgba(0, 63, 19, 0.8)', lineWidth: 5 },
    navigateRouteBg: { lineColor: 'rgba(255, 255, 255, 0.5)', lineWidth: 5 },
    navigateRoute: { lineColor: 'rgba(121, 85, 72, 0.8)', lineWidth: 5, lineDasharray: [1, 0.5], lineJoin: 'round', lineCap: 'butt' },

    detail: { ...detailCommon, iconAllowOverlap: false },
    detailFocus: { ...detailCommon, iconAllowOverlap: true },
    redFlag: { iconImage: imgRedFlag, iconAllowOverlap: true, iconSize: 0.4, iconAnchor: "bottom-left", iconOffset: [0, 0] },
    xmasTree: { iconImage:  imgXmasTree, iconAllowOverlap: true, iconSize: 0.5, iconAnchor: "bottom-left", iconOffset: [-61, -16] },
    redFlagText: { textField: '{map_flag_label}', textAllowOverlap: true, textSize: 21, textColor: '#ffffff', textOffset: [1.2, -2.3] },
    xmasTreeText: { textField: '{map_flag_label}', textAllowOverlap: true, textSize: 16, textColor: "#ffffff", textOffset: [0.06, -2.65], textFont:["Open Sans Bold"] },

    photoBg: { iconImage: imgPhotoBg, iconAllowOverlap: true, iconSize: 0.4, iconAnchor: "center", iconOffset: [90, -100] },
    photo: { iconImage: ['get', 'imgUrlId'], iconAllowOverlap: true, iconSize: 0.4, iconAnchor: "center", iconOffset: [90, -127] },
    photoCircle: { iconImage: imgPhotoCircle, iconAllowOverlap: true, iconSize: 0.35, iconAnchor: "center", iconOffset: [180, -225] },
    photoText: { textField: '+{posts}', textAllowOverlap: true, textSize: 10, textColor: '#333333', textOffset: [6.2, -7.9] },
    photoBboxText: { textField: '+{point_count}', textAllowOverlap: true, textSize: 10, textColor: '#333333', textOffset: [6.2, -7.9] },

    popupImage1: { ...popupImage, iconOffset: [135, -390] },
    popupImageXmas: { ...popupImage, iconOffset: [17, -557] },
    popupTop1: { ...popupTop, iconOffset: [80, -460] },
    popupTopXmas: { ...popupTop, iconOffset: [10, -568] },
    popupClose1: { ...popupClose, iconOffset: [290, -460] },
    popupCloseXmas: { ...popupClose, iconOffset: [213, -568] },
    popupTitle1: { ...popupTitle, textOffset: [2.2, -13.3] },
    popupTitleXmas: { ...popupTitle, textOffset: [0.39, -16.2] },
    popupLabel1: { ...popupLabel, textOffset: [2.2, -7.4] },
    popupLabelXmas: { ...popupLabel, textOffset: [0.39, -8.5] },
    popupBottom1: { ...popupBottom, iconOffset: [80, -185] },
    popupBottomXmas: { ...popupBottom, iconOffset: [10, -225] },

    popupImage2: { ...popupImage, iconOffset: [0, -245] },
    popupTop2: { ...popupTop, iconOffset: [0, -320] },
    popupClose2: { ...popupClose, iconOffset: [205, -320] },
    popupTitle2: { ...popupTitle, textOffset: [0, -9.5] },
    popupLabel2: { ...popupLabel, textOffset: [0, -3] },
    popupBottom2: { ...popupBottom, iconOffset: [0, -40] },

    popupTop3: { ...popupTop, iconOffset: [0, -145] },
    popupClose3: { ...popupClose, iconOffset: [205, -146] },
    popupTitle3: { ...popupTitle, textOffset: [0, -4.75] },
    popupLabel3: { ...popupLabel, textOffset: [0, -3] },
    popupBottom3: { ...popupBottom, iconOffset: [0, -40] },

};




