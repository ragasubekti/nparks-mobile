import React, {useState, useEffect, useRef} from 'react';
import { TouchableOpacity } from 'react-native';
import Menu, {MenuItem, MenuDivider, Position} from 'react-native-enhanced-popup-menu';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from 'react-navigation-hooks';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Utils from '@helpers/Utils';

//loynote: lame scoping problem, cannot put inside functional component...
//ref: https://github.com/likern/react-native-enhanced-popup-menu
let menuRef = null;

export default useDebug = (flag) => {
    const { navigate } = useNavigation();
    const [visible, setVisible] = useState(flag);
    const [currentFruit, setCurrentFruit] = useState(null);
    let iconRef = React.createRef(null);
    const setMenuRef = ref => menuRef = ref;

    useEffect(() => {
        if (flag) { Utils.log("Debug menu is on"); }
        setCurrentFruit(Settings.get(Settings.CURRENT_NEAR_HIDDEN_LOCATION));
        return function cleanup() {  } 
    }, []);
    
    const showMenu = () => menuRef.show(iconRef.current, stickTo = Position.BOTTOM_LEFT);
    const hideMenu = () => menuRef.hide();  
    
    renderDebugMenu = () => {
        if (!visible) { return; }
        //Utils.log("renderDebugMenu #########", menuRef);
        return (
          <Menu ref={setMenuRef}>
            <MenuItem onPress={() => { hideMenu(); navigate('hiddenFruit', {data: currentFruit}); }}>Hidden Fruit</MenuItem>
            <MenuItem onPress={() => { hideMenu(); navigate('quiz',{station :'station01'}); }}>Quiz</MenuItem>
            <MenuItem onPress={() => { hideMenu(); navigate('camera'); }}>Camera</MenuItem>
            <MenuItem onPress={() => { hideMenu(); navigate('tutorial'); }}>Tutorial</MenuItem>
            <MenuItem onPress={() => { hideMenu(); navigate('wikitude'); }}>Wikitude</MenuItem>
            <MenuItem onPress={() => { hideMenu(); Settings.debug(); }}>Console log all saved settings</MenuItem>
            <MenuItem onPress={() => { hideMenu(); StoreSettings.debug(); }}>Console log all store settings</MenuItem>
          </Menu>
        )
    }

    renderDebugIcon = () => {
        if (!visible) { return; }
        return (
            <TouchableOpacity style={{marginRight: 10}} activeOpacity={0.5} ref={iconRef} onPress={showMenu}>
                <Icon name="layers" size={24} color={"white"}/>
            </TouchableOpacity>
        )
    }  

    return { renderDebugMenu, renderDebugIcon }
}

