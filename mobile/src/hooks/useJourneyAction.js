import React, {useState, useEffect, useContext} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import * as Progress from 'react-native-progress';
import Modal from "react-native-modal";
import { StyleConstant } from '@assets/MyStyle';
import WebApi from '@helpers/WebApi';
import Constants from '@helpers/Constants';
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import Utils from "@helpers/Utils";
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';
import {ImagePath} from '@assets/imagePaths';
//to put showcustomdialog in immediate screen
export default useJourneyAction = () => { 
  const id = "journey-action"; //provide key to supress error, shd use uuid instead?
  const { showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
  const { navigate } = useNavigation();
  
  const doJourneyAction = async(action, param = null) =>{
    let res = await WebApi.doUserJourneyAction(action, param);
    let success = false;
    // console.log(res);
    if (res.status == WebApi.STATUS_OK) {
      if(action != Constants.ACTION_REDEEM_REWARD){
        handleAddCreditSuccess(action, param, res.data);
      }
      success = true;
    } else if (res.status == WebApi.STATUS_UNPROCESSABLE_ENTITY) {
      const err = res.data[0].message
      Utils.showToast(err);
    } else {
      const key = res.data.message_key
      success = handleAddCreditError(key, action, param);
    }
    return success;
  }

  const handleAddCreditSuccess = (action, param, data) => {
    switch(action){
      case Constants.ACTION_QUEST_COMPLETED:
        break;

      case Constants.ACTION_HIDDEN_FRUIT_FOUND:
      
      case Constants.ACTION_HIDDEN_FRUIT_FOUND_RARE:
        showCustomDialog({title:"Fantastic!", body:"You've earned " + data.credit, okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.CELEBREATE, hasFlowerImage:true});
        break;
      
      case Constants.ACTION_EVENT_XMAS_2019:
        if(data.hasReward){
          // collected 12 set for the 1st time
          showCustomDialog({title:"Congratulations!", body:"You have collected all 12 Gingerbread men,\n an extra 1000 flowers are rewarded to you!", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.GINGERBREAD_MAN_PAIR, customButtonColor:StyleConstant.xmasRed});
        } else if(param.includes(Constants.TYPE_2019_XMAS_SANTA)) {
          showCustomDialog({title:"MERRY CHRISTMAS!\nYOU'VE WON A 13" + '" MACBOOK PRO!', body:"Now, take a screenshot of me!\n\nPost it on your social media & hashtag #NParksC2C. Check your Inbox for collection details.", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.SANTA_GIFT, customButtonColor:StyleConstant.xmasRed, santa: true});
        } else {
          showCustomDialog({title:"Fantastic!", body:"You've earned " + data.credit, okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.CHRISTMAS, hasFlowerImage:true, customButtonColor:StyleConstant.xmasRed});
        }
        break;
      
      default:
        showDropdownAlert(data.message, 'Earned ' + data.credit + ' flowers');
        break;
    }
  }

  const handleAddCreditError = (message_key, action, param) =>{
    switch (message_key) {
      case Constants.VISIT_DUPLICATE:
        // do nothing
        return true;

      case Constants.AR_SCAN_DUPLICATE:
        // do nothing
        return true;

      case Constants.AR_SCAN_EARLY:
        showCustomDialog({title:"Uh-Oh!", body:"You're too fast!\nPlease allow at least 20 minutes\nfrom your last checkpoint to access this checkpoint.", okButtonText:"OK", okCallback:{navigateToExplore}, imagePath:ImagePath.TOO_FAST});
        return false;

      case Constants.AR_SCAN_COMPLETED_ALL_CHECKPOINTS:
        // do nothing
        return true;

      case Constants.UPLOAD_POST_LOCATION_DUPLICATE:
        showCustomDialog({title:"Error Uploading!", body:"Please try again later...", okButtonText:"OK", imagePath:ImagePath.LOGOUT});
        return false;

      case Constants.UPLOAD_POST_LOCATION_OVER_MAX_TIMES:
        // do nothing
        return true;

      case Constants.QUEST_COMPLETED_DUPLICATE:

      case Constants.QUEST_COMPLETED_OVER_MAX_TIMES:
        if (param.includes("quiz")) {
          showCustomDialog({title:"Good Job!", body:"You've already done the quiz for this station today.", okButtonText:"OK", okCallback:{navigateToExplore}, imagePath:ImagePath.LOGOUT});
        }
        return false;

      case Constants.HIDDEN_FRUIT_FOUND_DUPLICATE:

      case Constants.HIDDEN_FRUIT_FOUND_OVER_MAX_TIMES:
        showCustomDialog({title:"Oops!", body:"You've already scanned this location.", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.LOGOUT});
        return true;

      case Constants.JOURNEY_ACTION_INCORRECT_LOCATION:
        switch (action) {
          case Constants.ACTION_AR_SCAN:
            showCustomDialog({title:"Uh-Oh!", body:"Seems like you're in the wrong location.\nHead to the correct checkpoint location\nto scan and earn credits.", okButtonText:"OK", imagePath:ImagePath.LOGOUT});
            return false;

          case Constants.ACTION_QUEST_COMPLETED:
            showCustomDialog({title:"Uh-Oh!", body:"Seems like you're in the wrong location.\nHead to the correct checkpoint location\nand try again.", okButtonText:"OK", imagePath:ImagePath.LOGOUT});
            return false;

          case Constants.ACTION_HIDDEN_FRUIT_FOUND:

          case Constants.ACTION_HIDDEN_FRUIT_FOUND_RARE:
            showCustomDialog({title:"Oops!", body:"You are out of the location range of this object.", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.LOGOUT});
            return false;

          case Constants.ACTION_EVENT_XMAS_2019:
            showCustomDialog({title:"Oops!", body:"You are out of the location range of this object.", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.LOGOUT});
            return false;

        }
        break;

      case Constants.TOO_MANY_REQUEST_KEY:
        showDropdownAlert("Error", "Flowers not awarded this time.\nPlease try again in 5 seconds.")
        return false;

      case Constants.KEY_2019_XMAS_SANTA_FOUND:
        showCustomDialog({title:"The Santa Hunt is over!", body:"You may continue to hunt for the Gingerbread Man till 22 Dec - 11:59PM", okButtonText:"OK", okCallback:{navigateBackToExplore}, imagePath:ImagePath.SANTA_NO_GIFT, santa: true});
        return true;

      default:
        if(action != Constants.ACTION_REDEEM_REWARD){
          showDropdownAlert("Error", "Unable to award credits for " + action)
        }
        break;
    }
    return false;
  }

  const navigateBackToExplore = ()=>{
    navigate('explore',{resetCurrentNearHiddenLocation : true})
  }
  const navigateToExplore = () =>{
    navigate("explore");
  }

  return { doJourneyAction }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    backgroundColor: '#000000aa',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  txt: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: 'white',
    paddingTop: 10
  }  
});
