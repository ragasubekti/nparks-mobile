import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import Constants from '@helpers/Constants';
import WebApi from '@helpers/WebApi';
import Utils from '@helpers/Utils';
import { Settings, StoreSettings } from '@helpers/Settings';

export default useCustomFlatListFunc2 = (dataUrl, callback=null, result) => { 
    const [data2, setData] = useState([]);
    const [loading2, setLoading] = useState(true);
    const [refreshing2, setRefreshing] = useState(true);
    const [page2, setPage] = useState(0);
    const [error, setError] = useState(null);
    const [isLastPage, setIsLastPage] = useState(false);
    //const url = dataUrl + '?_format=json&page=${page}&results=10';
    const url = dataUrl + '?&page=' + page2 + '&result=' + result;
    
    useEffect(() => {
        if(!isLastPage){
            makeRemoteRequest2();
        }
    }, [page2]);    

    makeRemoteRequest2 = async() => {
        // Utils.log('makeRemoteRequest2: ' + url);
        setLoading(true);
        const token = await StoreSettings.get(StoreSettings.ACCESS_TOKEN);
        
        var config = {
          method: WebApi.GET_METHOD,
          headers: {
            'Authorization': "Bearer " + token
          }
        }
        
        fetch(url, config)
        .then((res)=>res.json())
        .then((resJson)=>{
            // add check status if == 200 (OK)
            if(resJson.status == WebApi.STATUS_OK){
                if(resJson.data.length < result){
                    setIsLastPage(true);
                    setPage(0)
                } 
                const d =  (page2 === 0) ? resJson.data : [...data2, ...resJson.data];
                // Utils.log(resJson);
                setData(d);
                if (callback) { setTimeout(callback,1000); }
            } else{
                // Utils.showToast("Unable to load data")
            }
        })
        .catch(err => {
            setError(err)
            //Utils.log(err);
        })
        .finally(()=>{
            setLoading(false);
            setRefreshing(false);
        });
    };
    handleRefresh = () => {
        setPage(0)
        setIsLastPage(false);
        setRefreshing(true);
        this.makeRemoteRequest();
    };
    renderSeparator = () => { return ( <View style={styles.separator}/> );  };
    renderFooter = () => {
        if (refreshing2 || !loading2) return null;
        return ( <View style={styles.bottomSeparator}><ActivityIndicator /></View> );
    };
    renderItemDefault = ({item}) => { return (<Text>{Math.random()}</Text>); };
    handleLoadMore = () => { setPage(page2 + 1); };

    return {
        page2, data2, setData, refreshing2, handleLoadMore, handleRefresh, renderSeparator, renderFooter
    }
}

const styles = StyleSheet.create({
    separator: {
      height: 1,
      width: "100%",
      backgroundColor: "#CED0CE",
    }, 
    bottomSeparator: {
      paddingVertical: 20,
      borderTopWidth: 1,
      borderColor: "#CED0CE"
    }, 
});
