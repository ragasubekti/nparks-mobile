import { Text, Button, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import React, {useState, useEffect, useRef} from 'react';
import Constants from '@helpers/Constants';
import { StyleConstant } from '@assets/MyStyle';
import WebApi from '@helpers/WebApi';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Supercluster from 'supercluster';
import Utils from "@helpers/Utils";

import imgGridPattern from '@assets/img/grid-pattern.jpg';
import imgMarker from '@assets/img/marker-icon-2x.png';


export default useMapbox = () => {
    const mapViewRef = useRef(null);
    const cameraRef = useRef(null);
    const [featureMarker, setFeatureMarker] = useState(MapboxGL.geoUtils.makeFeatureCollection());
    
    const [position, setPosition] = useState(Constants.SINGAPORE_CENTER);
    const [mapLoaded, setMapLoaded] = useState(false);

    useEffect(() => {
        MapboxGL.setAccessToken(Constants.MAPBOX_KEY);
        //call onDidFinishLoadingMap
        //handeleOnDidFinishLoadingMap();
        return function cleanup() { } 
    }, []);


    // useEffect(() => {
    //     cameraRef.current.moveTo(Constants.SINGAPORE_CENTER, 100);
    // }, [position]);

    prepareMarker = ([long, lat]) => {
        setPosition([long, lat]);
        const feature = {
            "type": "Feature",
            "properties": {
                "id": "marker_id",
                "name": "marker",
            },
            "geometry": {
                "type": "Point",
                "coordinates": [long, lat]
            }
        }
        const fc = { type: 'FeatureCollection', features: [feature] };
        setFeatureMarker(fc);
        Utils.log(fc);
    }

    handeleOnDidFinishLoadingMap = async () => {
        setMapLoaded(true);
        if(position != Constants.SINGAPORE_CENTER){
            cameraRef.current.moveTo(position, 100);
        }
        //find out whether map loads finish 1st or prepareMarker, then set camRef moveTo the passed in location
    }

    renderMapStuff = () => {
        return (
            <View style={{flex:1, opacity: mapLoaded? 100 : 0}}>

                <MapboxGL.MapView ref={mapViewRef} style={{flex: 1}} onDidFinishLoadingMap={handeleOnDidFinishLoadingMap}
                    onRegionDidChange={handleOnRegionDidChange} logoEnabled={false} attributionEnabled={false} >
                    
                    <MapboxGL.Camera ref={cameraRef} zoomLevel={15} minZoomLevel={10.5} maxZoomLevel={20}/>
                    
                    {/*followUserLocation={true} followUserMode="normal"*/}
                    <MapboxGL.RasterSource id="onemap-source" tileSize={256} url={"https://maps-a.onemap.sg/v3/Grey_HD/{z}/{x}/{y}.png"} >
                        <MapboxGL.BackgroundLayer id="bg-layer" style={styles.background} />
                        <MapboxGL.RasterLayer id="onemap-layer" sourceLayerID='onemap-source' />
                    </MapboxGL.RasterSource>

                    { /*Detail Focus*/ }
                    <MapboxGL.ShapeSource id="detail-focus-source" shape={featureMarker}>
                        <MapboxGL.SymbolLayer id="detail-focus-layer" style={styles.marker}/>                        
                    </MapboxGL.ShapeSource>

                   
            
                </MapboxGL.MapView>
            </View>
        );
    }


    return {
        renderMapStuff, cameraRef, prepareMarker
    }
}


const styles = {
    background: { backgroundColor: "black", backgroundPattern: imgGridPattern },
    marker: { iconImage:imgMarker, iconAllowOverlap:true, iconSize:0.4, iconAnchor:"bottom-left", iconOffset:[0, 0] },

};




