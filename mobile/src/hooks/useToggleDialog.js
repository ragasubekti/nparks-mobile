import React, {useState} from 'react';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { View, Text, StyleSheet, Image, Linking, TouchableOpacity, Dimensions, Switch } from 'react-native';
import * as Progress from 'react-native-progress';
import Modal from "react-native-modal";
import ModelDialog from '@models/ModelDialog';
import Constants from '@helpers/Constants';
import {Images} from '@assets/imagePaths';
import Utils from "@helpers/Utils";
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default useToggleDialog = () => { 
  //causing error
  //const id = "single-modal-id"; //provide same id to all modal to prevent triggering multiple modal
  const id = "toggle-dialog"; //provide key to supress error, shd use uuid instead?
  
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState(null);
  const [okButtonText, setOkButtonText] = useState(null);
  const [okCallback, setOkCallback] = useState(null);

  const [amenitiesOn, setAmenitiesOn] = useState(true);
  const [treesOn, setTreesOn] = useState(true);
  const [poiOn, setPoiOn] = useState(true);
  const [servicesOn, setServicesOn] = useState(true);
  const [photosOn, setPhotosOn] = useState(true);

  const dialogWidth = (Dimensions.get('window').width) * 0.9;
  
  const showToggleDialog = (title = "Map Details", okButtonText = "APPLY", okCallback = null) => {
    if(!visible){
      setVisible(true);
      setOkButtonText(okButtonText);
      setTitle(title);
    }
  }

  
  handleOkBtn = () =>{
    if(okCallback != null){
      let callback = okCallback[Object.keys(okCallback)[0]];
      callback();
    }
    setVisible(false);
  }

  renderOkBtn = () => {
    if(okButtonText != null){
      return (
      <Button 
        containerStyle={{width: '100%', height: 40, borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}
        title={okButtonText}
        onPress={handleOkBtn}
        buttonStyle={{backgroundColor: '#1b5e20'}}
      />);
    }
  }
  
  renderTitle = () => {
    if(title != null){
      return (<Text style={styles.title}>{title}</Text>);
    }
  } 

  toggleSwitchAmenities = ()=>{ setAmenitiesOn(!amenitiesOn); }
  toggleSwitchTrees = ()=>{ setTreesOn(!treesOn); }
  toggleSwitchPoi = ()=>{ setPoiOn(!poiOn); }
  toggleSwitchServices = ()=>{ setServicesOn(!servicesOn); }
  toggleSwitchPhotos = ()=>{ setPhotosOn(!photosOn); }

  render = () => {
    return (
      <Modal key={id} isVisible={visible} backdropColor='#111111' animationType='fade' style={styles.container}>
        <View style={{width: dialogWidth, backgroundColor: 'white', borderRadius: 20, alignItems: 'stretch', justifyContent: 'space-evenly', overflow: 'hidden'}}>
          <Icon style={{alignSelf:"center"}} name={'layers'} size={50} color={"#1b5e20"}/>
          { renderTitle() }
          <View style={{flexDirection: 'row'}}>
            <Image style={{width: 24, height: 24, margin: 10}} source={require('@assets/img/ic_amenity.png')} />
            <Text style={{alignSelf: 'center'}}>Amenities</Text>
            <Switch
              style={{position: 'absolute', right: 10, alignSelf: 'center'}}
              onValueChange = {toggleSwitchAmenities}
              value={amenitiesOn} thumbColor={"#1b5e20"}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image style={{width: 24, height: 24, margin: 10}} source={require('@assets/img/ic_heritage_tree.png')} />
            <Text style={{alignSelf: 'center'}}>Heritage Trees</Text>
            <Switch
              style={{position: 'absolute', right: 10, alignSelf: 'center'}}
              onValueChange = {toggleSwitchTrees}
              value={treesOn} thumbColor={"#1b5e20"}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image style={{width: 24, height: 24, margin: 10}} source={require('@assets/img/ic_poi.png')} />
            <Text style={{alignSelf: 'center'}}>Points of Interest</Text>
            <Switch
              style={{position: 'absolute', right: 10, alignSelf: 'center'}}
              onValueChange = {toggleSwitchPoi}
              value={poiOn} thumbColor={"#1b5e20"} 
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image style={{width: 24, height: 24, margin: 10}} source={require('@assets/img/ic_tenant.png')} />
            <Text style={{alignSelf: 'center'}}>Services</Text>
            <Switch
              style={{position: 'absolute', right: 10, alignSelf: 'center'}}
              onValueChange = {toggleSwitchServices}
              value={servicesOn} thumbColor={"#1b5e20"}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image style={{width: 24, height: 24, margin: 10}} source={require('@assets/img/ic_post_location.png')} />
            <Text style={{alignSelf: 'center'}}>Photos</Text>
            <Switch
              style={{position: 'absolute', right: 10, alignSelf: 'center'}}
              onValueChange = {toggleSwitchPhotos}
              value={photosOn} thumbColor={"#1b5e20"}
            />
          </View>
          { renderOkBtn() }
        </View>
      </Modal>
    );
  }

  return { showToggleDialog, renderToggleDialog:render, amenitiesOn }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    content: {
      backgroundColor: '#ffffff',
      padding: 22,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 20,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    title: {
      textAlign: 'center', textAlignVertical: 'center',
      color: 'black', fontSize: 18,
      fontWeight: 'bold', margin: 10
    },
    body: {
      textAlign: 'center',
      textAlignVertical: 'center',
      color: '#666666',
      margin: 10
    }  
});
