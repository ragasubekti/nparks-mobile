import React, {useState} from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import * as Progress from 'react-native-progress';
import Modal from "react-native-modal";
import ModelDialog from '@models/ModelDialog';
import Constants from '@helpers/Constants';
import DropdownAlert from 'react-native-dropdownalert';

export default useDropdownAlert = () => { 
  const id = "dropdown-alert" + Math.floor(Math.random() * 100) + 1 ; //provide key to supress error, shd use uuid instead?
  
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState(false);
  const [body, setBody] = useState(false);
  var popupRef = null;
  
  const showDropdownAlert = (title, body) => {
    setVisible(true);
    setTitle(title);
    setBody(body);
    setTimeout(() => {
      setVisible(false);
    }, 3000);
    

    
  }

  render = () => {
    if(visible){
      return (
        <View style={styles.content}>
          <View style={{width:'90%', minHeight: 75, alignSelf:'center', backgroundColor: 'white', borderRadius: 20, elevation: 10, flexDirection: 'row', alignItems: 'center', paddingLeft: 5, paddingRight: 5, paddingTop: 10, paddingBottom: 10}}>
            <Image style={{width: 40, height: 40, marginLeft: 5}} source={require('@assets/img/ic_launcher.png')} />
            <View style={{flex: 1, flexDirection: 'column', marginLeft: 5}}>
              <Text style={{flex: 1, fontSize: 18, color:'black', fontWeight:'bold'}}>{title}</Text>
              <Text style={{flex: 1, fontSize: 15, color:'black'}}>{body}</Text>
            </View>
            
          </View>
        </View>
      );
    }
  }

  return { showDropdownAlert, renderDropdownAlert:render }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 10
    },
    // content: {
    //   backgroundColor: '#000000aa',
    //   padding: 22,
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //   borderRadius: 20,
    //   borderColor: 'rgba(0, 0, 0, 0.1)',
    // },
    content: {
      flex: 1,
      backgroundColor: '#00000000',
      position: 'absolute',
      padding: 5,
      width: '100%',
    },
    txt: {
      textAlign: 'center',
      textAlignVertical: 'center',
      color: 'white',
      paddingTop: 10
    }  
});

// const styles = StyleSheet.create({
//   content: {
//     flex: 1,
//     backgroundColor: StyleConstant.warnColor,
//     position: 'absolute',
//     padding: 5,
//     width: '100%',
//   },
//   txt: {
//     flex: 1,
//     textAlign: 'center',
//     textAlignVertical: 'center',
//     color: '#333333',
//   }  
// });
