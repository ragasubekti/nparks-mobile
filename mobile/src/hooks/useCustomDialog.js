import React, {useState} from 'react';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';
import { View, Text, StyleSheet, Image, Linking, TouchableOpacity, Dimensions, Button } from 'react-native';
import * as Progress from 'react-native-progress';
import Modal from "react-native-modal";
import ModelDialog from '@models/ModelDialog';
import Constants from '@helpers/Constants';
import {Images} from '@assets/imagePaths';
import Utils from "@helpers/Utils";
// import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';
import CustomImage from '@components/CustomImage';

export default useCustomDialog = () => { 
  //causing error
  //const id = "single-modal-id"; //provide same id to all modal to prevent triggering multiple modal
  // const id = "custom-dialog"  + Math.floor(Math.random() * 100) + 1 ; //provide key to supress error, shd use uuid instead?
  const id = "custom-dialog"; //provide key to supress error, shd use uuid instead?
  
  const [data, setData] = useState({});
  const [visible, setVisible] = useState(false);

  const dialogWidth = (Dimensions.get('window').width) * 0.9;
  const imageWidth = (Dimensions.get('window').width) * 0.5;
  
  const showCustomDialog = (data) => {
    // Utils.log(data);
    setData(data);

    setVisible(true);
  }

  handleOk = () =>{
    if(data.hasOwnProperty("okCallback")){
      if(data.okCallback != null){
        let callback = data.okCallback[Object.keys(data.okCallback)[0]];
        callback();
      }
    }
    setVisible(false);
  }

  handleCancel = () =>{
    if(data.hasOwnProperty("cancelCallback")){
      if(data.cancelCallback != null){
        let callback = data.cancelCallback[Object.keys(data.cancelCallback)[0]];
        callback();
      }
    }
    setVisible(false);
  }

  renderTitle = () => {
    if(data.hasOwnProperty("title")){
      return (<Text style={[styles.title, data.hasOwnProperty("santa")? {fontWeight: 'normal', fontFamily: 'Sudden Desires'} : {fontWeight: 'bold', fontFamily: 'system font'}]}>{data.title}</Text>);
    }
  }

  renderBody = () => {
    if(data.hasOwnProperty("body")){
      if(data.hasOwnProperty("hasGlobeImage")){
        return (<Text style={styles.body}>{data.body}<Image style={{width:16, height: 16, alignSelf: 'flex-end'}} source={require('@assets/img/xmas_globe.png')}/></Text>);
      }
      return(<Text style={styles.body}>{data.body}</Text>);
    }
  }

  renderOkButton = () => {
    if(data.hasOwnProperty("okButtonText")){
      return (
          <TouchableOpacity style={{width: '100%', marginBottom: -2, alignItems:'center', justifyContent:'center', height: 55, backgroundColor: data.hasOwnProperty("customButtonColor") ? data.customButtonColor : '#1b5e20'}} onPress={handleOk}>
            <Text style={{color: 'white', fontSize: 21}}>{data.okButtonText}</Text>
          </TouchableOpacity>
      );
    }
  }

  renderCloseButton = () => {
    if(data.hasOwnProperty("hasCancelButton")){
      return (
        <TouchableOpacity accessibilityLabel='close button' style={{position: 'absolute', right:0, padding: 12}} onPress={handleCancel}>
          <Icon name={'close-circle-outline'} size={32} color={data.hasOwnProperty("customButtonColor") ? data.customButtonColor : "#1b5e20"}/>
        </TouchableOpacity>
      );
    }
  }

  renderImage = () => {
    // read up https://willowtreeapps.com/ideas/react-native-tips-and-tricks-2-0-managing-static-assets-with-absolute-paths
    if(data.hasOwnProperty("imagePath")){
      if(data.imagePath != null){
        if(data.hasOwnProperty("isImagePathUrl")){
          //example: require('@assets/img/flower.png')
          return (<CustomImage style={{width: imageWidth, height: imageWidth, marginTop: 10, alignSelf:'center', marginBottom: 10}} resizeMode={'contain'} path={data.imagePath}/>);
        } else {
          //example: 'https://facebook.github.io/react-native/docs/assets/favicon.png'
          return (<FastImage style={{width: imageWidth, height: imageWidth, marginTop: 10, alignSelf:'center', marginBottom: 10}} resizeMode={'contain'} source={Images[data.imagePath]}/>);
        }
      }
    }
  }

  renderHyperlink  = () => {
    if(data.hasOwnProperty("hyperlink")){
      if(data.hyperlink != null){
        return (
          <TouchableOpacity onPress={()=>{Linking.openURL(Utils.reformHyperlink(data.hyperlink))}}>
            <Text style={{alignSelf: 'center', color: StyleConstant.primaryColor, margin: 20, textDecorationLine: 'underline'}}>Tap here to find out more!</Text>
          </TouchableOpacity>
        );
      }
    }
  }

  renderFlowerImage = () => {
    if(data.hasOwnProperty("hasFlowerImage")){
      return (<Image style={{width:12, height: 12, alignSelf: 'center'}} source={require('@assets/img/flower.png')}/>);
    }
  }

  render = () => {
    return (
      <Modal accessibilityLabel={"pop up dialog"} key={id} isVisible={visible} backdropColor='#00000080' animationType='fade' style={styles.container} hardwareAccelerated={true}>
        <View style={{width: dialogWidth, backgroundColor: 'white', borderRadius: 20, alignItems: 'center', justifyContent: 'space-between', overflow: 'hidden'}}>
          {renderCloseButton()}
          {renderTitle()}
          {renderImage()}
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            {renderBody()}
            {renderFlowerImage()}
          </View>
          {renderHyperlink()}
          {renderOkButton()}
        </View>
      </Modal>
    );
  }

  return { showCustomDialog, renderCustomDialog:render }

}

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center', padding: 0, margin: 0 },
    content: { backgroundColor: '#ffffff', padding: 22, justifyContent: 'center', alignItems: 'center', borderRadius: 20, borderColor: 'rgba(0, 0, 0, 0.1)'},
    title: { textAlign: 'center', textAlignVertical: 'center', color: 'black', fontSize: 18, margin: 10 },
    body: { textAlign: 'center', textAlignVertical: 'center', color: '#666666', margin: 10 }  
});
