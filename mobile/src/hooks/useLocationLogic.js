import { Text, Button, View, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';
import React, {useState, useEffect, useContext} from 'react';
import Constants from '@helpers/Constants';
import { StyleConstant } from '@assets/MyStyle';
import WebApi from '@helpers/WebApi';
import Utils from "@helpers/Utils";
import { Settings, StoreSettings, GlobalContext } from '@helpers/Settings';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {ImagePath} from '@assets/imagePaths';
import useJourneyAction from '@hooks/useJourneyAction';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';


/* //myLocation is state variable from useMapbox
{
    timestamp: 1568716336000,
    coords: {
        accuracy: 170
        altitude: 15.04444408
        heading: 353
        latitude: 1.2873945687
        longitude: 103.7849863076
        speed: 0
    }
}
*/  

export default useLocationLogic = (myLocation, isInMap) => {
    const { showDropdownAlert, showCustomDialog } = useContext(GlobalContext);
    const [withinC2cBorder, setWithinC2cBorder] = useState(false);
    const [currentNearHiddenLocation, setCurrentNearHiddenLocation] = useState(null);
    const { doJourneyAction } = useJourneyAction();
    const { navigate, navigationOptions } = useNavigation();

    useEffect(() => {
        return function cleanup() { } 
    }, []);

    useEffect(() => {
        if(Settings.get(Settings.CURRENT_NEAR_HIDDEN_LOCATION) == ""){
            setCurrentNearHiddenLocation(null);
        }
    }, [Settings.get(Settings.CURRENT_NEAR_HIDDEN_LOCATION)]);

    useEffect(() => {
        if(isInMap){
            Utils.log('in map');
            onPositionChange();
        } else{Utils.log('out map'); }
    }, [myLocation]);
    
    checkWithinSgBoundary = (px, py) => {
        let flag = false;
        var tlx = Constants.SINGAPORE_TOP_LEFT[0];
        var tly = Constants.SINGAPORE_TOP_LEFT[1];
        var brx = Constants.SINGAPORE_BOTTOM_RIGHT[0];
        var bry = Constants.SINGAPORE_BOTTOM_RIGHT[1];
        if (px > tlx && px < brx && py > bry && py < tly) {
            flag = true;
        }
        //Utils.log("checkWithinSgBoundary", flag);
        return flag;
    }
    checkWithinC2cBoundary = (px, py) => {
        let polygon = Constants.C2C_BORDER_POLYGON_ARR;
        let flag = false;
        let j = polygon.length - 1;
        for (let i = 0; i < polygon.length; i++) {
            if (polygon[i][1] < py && polygon[j][1] >= py || polygon[j][1] < py && polygon[i][1] >= py) {
                if (polygon[i][0] + (py - polygon[i][1]) / (polygon[j][1] - polygon[i][1]) * (polygon[j][0] - polygon[i][0]) < px) {
                    flag = !flag;
                }
            }
            j = i;
        }
        return flag;
    }
    showWelcomeDialogOnce = (type) => {
        if (!Settings.get(Settings.HAD_PROMPT_WELCOME_MSG)) {
            Settings.store(Settings.HAD_PROMPT_WELCOME_MSG, true);
            if(type == "DIALOG_OUT_OF_C2C"){
                showCustomDialog({title:"Welcome!", body:"Almost there!\nVisit the nearest checkpoint along the\nCoast-to-Coast Trail to start your adventure!", okButtonText:"OK", imagePath:ImagePath.CELEBREATE});
            } else if(type == "DIALOG_OUT_OF_SG"){
                showCustomDialog({title:"Uh-Oh!", body:"Seems like you're outside of Singapore.\nThe Coast-to-Coast Trail\nis only available in Singapore.", okButtonText:"OK", imagePath:ImagePath.NO_GPS});
            }
        }  
    }
    onEnterC2c = () => { setWithinC2cBorder(true); }
    onExitC2c = () => { setWithinC2cBorder(false); }
    doVisitC2c = async() => { 
        if (!Settings.get(Settings.HAD_PROMPT_C2C_MSG)) {
            Settings.store(Settings.HAD_PROMPT_C2C_MSG, true);
            
            if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
                showCustomDialog({title:"Christmas Event!", body:"To join the christmas event lucky draw\nClick on the Candy Cane Snowball", okButtonText:"OK", imagePath:ImagePath.CHRISTMAS_WITH_GLOBE, customButtonColor:StyleConstant.xmasRed, hasGlobeImage: true});
            } else {
                showCustomDialog({title:"Hello!", body:"You've just entered the Coast-to-Coast Trail!\nHit all 10 checkpoints if you can!", okButtonText:"OK", imagePath:ImagePath.WELCOME});
            }
        }        
        const visited = await StoreSettings.get(StoreSettings.HAD_VISITED_C2C_TODAY);
        if (visited == "false") {
            await StoreSettings.store(StoreSettings.HAD_VISITED_C2C_TODAY, "true");
            doJourneyAction(Constants.ACTION_VISIT);
        } 

    
    }

    onPositionChange = async () => {
        if (myLocation == null) return;
        //Utils.log("onPositionChange");

        const x = myLocation.coords.longitude;
        const y = myLocation.coords.latitude;
        const isWithinSg = checkWithinSgBoundary(x, y);
        const isWithinC2c = checkWithinC2cBoundary(x, y);
        
        //welcome dialog logic
        //firstPosChange might not be need in react?;
        let firstPosChange =  true;
        if (firstPosChange || Settings.get(Settings.IS_WITHIN_C2C) != isWithinC2c || Settings.get(Settings.DEBUG_MODE) == Constants.STATUS_ENABLED) {
            if (isWithinC2c || Settings.get(Settings.IS_WITHIN_C2C) || Settings.get(Settings.DEBUG_MODE) == Constants.STATUS_ENABLED) onEnterC2c();
            else onExitC2c();
        }
        firstPosChange = false;

        Settings.store(Settings.IS_WITHIN_SG , isWithinSg);
        Settings.store(Settings.IS_WITHIN_C2C, isWithinC2c);

        if (!Settings.get(Settings.HAD_PROMPT_C2C_MSG )) {
            if (isWithinSg) {
                if (isWithinC2c) {
                    doVisitC2c();
                } else {
                    showWelcomeDialogOnce("DIALOG_OUT_OF_C2C");
                }
            } else {
                showWelcomeDialogOnce("DIALOG_OUT_OF_SG");
            }
        }

        //hidden fruits logic
        let h = await checkIsNearHiddenLocationWithinKmAndNotFound(y, x, 0.1);
        if (h != null) {
            let flag = false;
            //when not near any hidden fruit 
            if (currentNearHiddenLocation == null) {
                flag = true;
            }
            //when near a hidden fruit & approach another hidden fruit
            else if (currentNearHiddenLocation.id != h.id) {
                flag = true;
            }
            // show dialog once every hidden fruit change
            if (flag) {
                setCurrentNearHiddenLocation(h);
                var url = h.thumbnail_base_url + "/" + h.thumbnail_path;
                if(Settings.get(Settings.SPECIAL_EVENT_MODE) == Constants.MODE_2019_XMAS_PHRASE2){
                    var body = "Find it and earn Flower Points!";
                    var title = "Psst!\nA Gingerbread Man is nearby!";
                    if(h.object_name == Constants.TYPE_2019_XMAS_SANTA){
                        body = "Find him and win the grand prize,\na 13-inch MacBook Pro!";
                        title = "HO! HO! HO!\nWHERE AM I ?";
                    }
                    showCustomDialog({title:title, body:body, okButtonText:"Let's Go!", imagePath:url, okCallback:{checkForPermission}, hasCancelButton:true, isImagePathUrl:true, customButtonColor:StyleConstant.xmasRed, santa: true});
                } else {
                    let body = h.status_rare == "true" ? "There's hidden rare fruit nearby.\nLook for them to earn flowers!" : "There's hidden fruit nearby.\nLook for them to earn flowers!";
                    showCustomDialog({title:"Psssst!", body:body, okButtonText:"Let's Go!", imagePath:url, okCallback:{checkForPermission}, hasCancelButton:true, isImagePathUrl:true});
                }
            } 
        }
        if (currentNearHiddenLocation != null) {
            let hasExited = checkHasExitHiddenLocationWithinKm(currentNearHiddenLocation, y, x, 0.3);
            if (hasExited) {
                setCurrentNearHiddenLocation(null);
            }
        }
        
    }
    
    checkForPermission = async() =>{
    let result;
    if (Platform.OS === "android") {
        let cameraResult = await check(PERMISSIONS.ANDROID.CAMERA);
        result = cameraResult == RESULTS.GRANTED;
    } else {
        let camera = await check(PERMISSIONS.IOS.CAMERA);
        let motion = await check(PERMISSIONS.IOS.MOTION);
        if(camera == RESULTS.BLOCKED || motion == RESULTS.BLOCKED){
        openAppSettings=()=>{
            Linking.openURL('app-settings:');
        }
        showCustomDialog({body:"Coast-to-Coast would like access to your Camera to find the hidden object", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{openAppSettings}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
        return;
        }
        result = camera == RESULTS.GRANTED && motion == RESULTS.GRANTED;
    }

    if(result){
        hiddenFruitDialogOnOkAction();
    } else {
        showCustomDialog({body:"Coast-to-Coast would like access to your Camera to find the hidden object", okButtonText:"OK", imagePath:ImagePath.WELCOME, okCallback:{requestForCameraMotionPermissions}, hasCancelButton:true, cancelCallback:{navigateToExplore}});
    }
    }

    navigateToExplore = () =>{
    navigate("explore");
    }

    requestForCameraMotionPermissions = ()=>{
    requestForPermissions2()
    .then((status)=>{
        Utils.log("ScreenCameraHiddenFruit - hasRequiredPermissions: " + status);
        if(status){
        hiddenFruitDialogOnOkAction();
        } else {
        navigateToExplore();
        }
    })
    }

    requestForPermissions2 = async() =>{
    var result = false;
    try{
        if (Platform.OS === "android") {
        var cameraResult = await request(PERMISSIONS.ANDROID.CAMERA);
        result = cameraResult == RESULTS.GRANTED;
        } else {
        var cameraResult = await request(PERMISSIONS.IOS.CAMERA);
        var motionResult = await request(PERMISSIONS.IOS.MOTION);
        result = cameraResult == RESULTS.GRANTED && motionResult == RESULTS.GRANTED;
        }
        
    } catch (e) {
        Utils.log(e);
    }
    return result;
    }

    hiddenFruitDialogOnOkAction = () => {
        navigate("hiddenFruit" , {data: currentNearHiddenLocation});
    }

    
    checkIsNearHiddenLocationWithinKmAndNotFound = async (latitude, longitude, km) => {
        var list = Settings.get(Settings.HIDDEN_LOCATION_TODAY_LIST);
        let nearestDis = km;
        let loc = null;

        for (let i = 0; i < list.length; i++) {
            let h = list[i];
            var d = Utils.calculateDistance(latitude, longitude, h.latitude, h.longitude);
            if (d < km) {
                const found = await hadFoundHiddenLocation(h.id);
                if (d < nearestDis && !found) {
                    nearestDis = d;
                    loc = h;
                }
            }
        }
        //return type: ModelHiddenLocationToday
        return loc;
    }

    checkHasExitHiddenLocationWithinKm = (h, latitude, longitude, km) => {
        if (h != null) {
            var d = Utils.calculateDistance(latitude, longitude, h.latitude, h.longitude);
            if (d > km) {                
                return true;
            }
        }
        return false;
    }    
    
    /* Shifted to Utils
    recordHiddenLocationFound = async(id) => {
        const str = await StoreSettings.get(StoreSettings.HIDDEN_LOCATION_TODAY_FOUND_LIST);
        await StoreSettings.store(StoreSettings.HIDDEN_LOCATION_TODAY_FOUND_LIST, id + "," + str);
    }
    */

    hadFoundHiddenLocation = async (id) => {
        const str = await StoreSettings.get(StoreSettings.HIDDEN_LOCATION_TODAY_FOUND_LIST);
        if (str) {
            const arr = str.split(',');
            for (let i = 0; i < arr.length; i++) {
                let f = arr[i];
                //Utils.log("compare", f, "vs", id);
                if (f == id) {
                    return true;
                }
            }
        }
        return false;
    }


    


    return { withinC2cBorder, currentNearHiddenLocation, setCurrentNearHiddenLocation }
}


