import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import { DefaultTheme } from 'react-native-paper';
import DeviceInfo from 'react-native-device-info';

const StyleConstant = {
  primaryColor: "#1b5e20",
  primaryColorLight: "#e2f1f8",
  primaryColorDark: "#808e95",
  primaryTextColor: "#000000",

  secondaryColor: "#212121",
  secondaryColorLight: "#484848",
  secondaryColorDark: "#000000",  
  secondaryTextColor: "#ffffff",
  mutedTextColor: "#cccccc",

  warnColor: "#ffee75",
  bgGray: "#eeeeee",
  tabGray: "#999",
  xmasRed: "#af0005",

  titleFontSize: 28,
}

const s = StyleConstant;

const MyTheme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: s.primary,
    accent: s.secondary,
  }
};

const heightAccordingToIosModel = () =>{
  if( DeviceInfo.getDeviceId().includes('iPhone11') ){
    return 68;
  } else if( DeviceInfo.getDeviceId().includes('iPhone12') ){
    return 68;
  } else if( DeviceInfo.getDeviceId().includes('iPhone13,1') ){
    return 55;
  } else if( DeviceInfo.getDeviceId().includes('iPhone13') ){
    return 88;
  } else {
    return 68;
  }
}
const paddingTopAccordingToIosModel = () =>{
  if( DeviceInfo.getDeviceId().includes('iPhone11') ){
    return 37;
  } else if( DeviceInfo.getDeviceId().includes('iPhone12') ){
    return 37;
  } else if( DeviceInfo.getDeviceId().includes('iPhone13,1') ){
    return 31;
  } else if( DeviceInfo.getDeviceId().includes('iPhone13') ){
    return 42;
  } else {
    return 25;
  }
}

//ref: https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
const HeaderStyle = {
  headerStyle : {backgroundColor: s.primaryColor,    height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0,},
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white'
};

const HeaderStyleWithBackDroid = {
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white',
  headerStyle: { backgroundColor: s.primaryColor,    height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0, },
  headerTitleContainerStyle: {
    left: 0
  }
};
const HeaderStyleWithBackIos = {
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white',
  headerStyle: { backgroundColor: s.primaryColor,     height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0, },
  headerTitleContainerStyle: {}
};
//different style required for droid and ios
const HeaderStyleWithBack = Platform.OS === 'android'? HeaderStyleWithBackDroid : HeaderStyleWithBackIos;

const HeaderStyleWithRight = {
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white',
  headerStyle: { backgroundColor: s.primaryColor,     height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0, },
  headerTitleContainerStyle: {
    alignItems: "center"
  }
};

const HeaderStyleWithRightOnlyDroid = {
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white',
  headerStyle: { backgroundColor: s.primaryColor,     height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0, },
  headerTitleContainerStyle: {
    right: 0
  }
};
const HeaderStyleWithRightOnlyIos = {
  headerTitleStyle: { color: "white", alignSelf: "center", marginLeft: "auto", marginRight: "auto", fontSize: 16, fontWeight: "normal"},
  headerTintColor: 'white',
  headerStyle: { backgroundColor: s.primaryColor,    height: Platform.OS == 'ios' ? heightAccordingToIosModel() : 56, 
  paddingTop: Platform.OS == 'ios' ? paddingTopAccordingToIosModel() : 0, },
  headerTitleContainerStyle: {}
};
//different style required for droid and ios
const HeaderStyleWithRightOnly = Platform.OS === 'android'? HeaderStyleWithRightOnlyDroid : HeaderStyleWithRightOnlyIos;

const ShadowStyle = {
  shadowColor: 'rgba(0,0,0,0.4)', shadowOffset: {height: 1, width: 1}, shadowOpacity: 1, shadowRadius: 1, elevation: 2
}

const ShareStyle = StyleSheet.create({
  titleTxt: {
    fontSize: StyleConstant.titleFontSize,
    lineHeight:StyleConstant.titleFontSize,
    //fontWeight: 'bold',
    color: StyleConstant.primaryColorLight, 
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  flex1: {
    flex: 1,
    //backgroundColor: '#ffffff11',
    //borderWidth: 1,
  },
  flex2: {
    flex: 2,
  },
  flex3: {
    flex: 3,
  }

});
/*
const MyThemeReactPaper = {
  ...DefaultTheme,
  roundness: 12,
  colors: {
    ...DefaultTheme.colors,
    primary: StyleConstant.primaryColor,
    accent: StyleConstant.primaryColorLight,
  },
};
*/

export { StyleConstant, ShareStyle, MyTheme, HeaderStyle, HeaderStyleWithBack, HeaderStyleWithBackIos, HeaderStyleWithRight, HeaderStyleWithRightOnly, HeaderStyleWithRightOnlyIos, ShadowStyle };