// reason why this file is created:
// https://willowtreeapps.com/ideas/react-native-tips-and-tricks-2-0-managing-static-assets-with-absolute-paths
const Images = {
    celebrate: require('@assets/img/Celebrate.png'),
    no_gps: require('@assets/img/phil_no_gps.png'),
    welcome: require('@assets/img/phil_welcome.png'),
    too_fast: require('@assets/img/phil_too_fast.png'),
    logout: require('@assets/img/Logout.png'),
    completed_quiz: require('@assets/img/completed_quiz.png'),
    christmas: require('@assets/img/phil_xmas.png'),
    christmas_with_globe: require('@assets/img/phil_with_globe.png'),
    gingerbread_man_pair: require('@assets/img/ginger_pair.png'),
    santa_gift: require('@assets/img/santa_gift.png'),
    santa_no_gift: require('@assets/img/santa_no_gift.png'),
};

const ImagePath = {
    CELEBREATE:"celebrate",
    NO_GPS:"no_gps",
    WELCOME:"welcome",
    TOO_FAST:"too_fast",
    LOGOUT:"logout",
    COMPLETED_QUIZ:"completed_quiz",
    CHRISTMAS:"christmas",
    CHRISTMAS_WITH_GLOBE:"christmas_with_globe",
    GINGERBREAD_MAN_PAIR:"gingerbread_man_pair",
    SANTA_GIFT:"santa_gift",
    SANTA_NO_GIFT:"santa_no_gift",
};

export {Images,ImagePath};