import React from 'react';
import { FlatList, View, Text } from 'react-native';
import useCustomFlatListFunc2 from '@hooks/useCustomFlatListFunc2';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';

export default CustomFlatList2 = ({dataUrl, renderItem, result}) => {
    
    const { data2, setData, refreshing2, handleLoadMore, handleRefresh, renderSeparator, renderFooter } = useCustomFlatListFunc2(dataUrl, null, result);

    // using form inactive reward list, as cannot use identical hook function name in same screen
    return (
        <FlatList
            ListEmptyComponent={
                <View style={{flex: 1, height: 400, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
                </View>
            }
            data={data2}
            refreshing={refreshing2}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={handleRefresh}
            onEndReached={handleLoadMore}          
            onEndReachedThreshold={0.1}
            ItemSeparatorComponent={renderSeparator}
            ListFooterComponent={renderFooter}
        />
    );
};
