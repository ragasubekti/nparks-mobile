/*
import {requireNativeComponent} from 'react-native';
module.exports = requireNativeComponent('ArchitectView');
*/
//ref: https://teabreak.e-spres-oh.com/swift-in-react-native-the-ultimate-guide-part-2-ui-components-907767123d9e


// ArchitectView.js
import React, { Component } from "react";
import PropTypes from "prop-types";
import { requireNativeComponent, UIManager, findNodeHandle} from "react-native";
const COMPONENT_NAME = "HiddenLocationView";
const RNView = requireNativeComponent(COMPONENT_NAME);

export default class HiddenLocationView extends Component {
  static propTypes = {
    //count: PropTypes.number,
    //path: PropTypes.string,
    //serial: PropTypes.string,
    onArEvent: PropTypes.func
  };
  _onTouchHiddenFruit = event => {
    if (this.props.onTouchHiddenFruit) {
      this.props.onTouchHiddenFruit(event.nativeEvent);
    }
  };
  render() {
    const { style } = this.props;
    return (
      <RNView
        style={style}
        onTouchHiddenFruit={this._onTouchHiddenFruit}
        ref={ref => this.ref = ref}
      />
    );
  }
  initFromReact = (...args) => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.ref),
      UIManager.getViewManagerConfig(COMPONENT_NAME).Commands.initFromReact,
      [...args]
    );
  };

}