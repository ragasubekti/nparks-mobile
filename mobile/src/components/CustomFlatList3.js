import React from 'react';
import { FlatList, View, Text } from 'react-native';
import useCustomFlatListFunc from '@hooks/useCustomFlatListFunc';
import { ShareStyle, StyleConstant } from '@assets/MyStyle';

export default CustomFlatList = (dataUrl, renderItem, result) => {

    const { data, setData, refreshing, handleLoadMore, handleRefresh, renderSeparator, renderFooter } = useCustomFlatListFunc(dataUrl, null, result);
    
    // using in ScreenMe and ScreenNotification to trigger handleRefresh programmatically
    toggleRefresh = () =>{
        handleRefresh();
    }
    render = () =>{
        return <FlatList
            ListEmptyComponent={
                <View style={{flex: 1, height: 400, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
                </View>
            }
            data={data}
            refreshing={refreshing}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            numColumns={1}
            onRefresh={handleRefresh}
            onEndReached={handleLoadMore}          
            onEndReachedThreshold={0.1}
            ItemSeparatorComponent={renderSeparator}
            ListFooterComponent={renderFooter}
        />
    }
    return { toggleRefresh, renderCustomFlatList:render }
};
