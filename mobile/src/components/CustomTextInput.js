import React from 'react';
import { View, StyleSheet,  TextInput } from 'react-native';

const CustomTextInputType = { 
    TEXT: 'text',
    PASSWORD: 'password'  
  }

const CustomTextInput = ({type, placeholder, value, setValue}) => {

    const onChangeText = (text) => {
        if (setValue) setValue(text);
    }

    let txt = 
        <TextInput style={styles.txt}
            placeholder={placeholder}
            placeholderTextColor="#cccccc"
            returnKeyType="done"
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            defaultValue={value}
            onChangeText={onChangeText}
        />
    let pwd = 
        <TextInput style={styles.txt}
            placeholder={placeholder}
            placeholderTextColor="#cccccc"
            returnKeyType="done"
            secureTextEntry
            defaultValue={value}
            onChangeText={onChangeText}
        />

    return (
        <View style={[styles.container]}>
            {type == CustomTextInputType.TEXT ? txt : pwd}
        </View>
    );
}

CustomTextInput.defaultProps = {
    type: CustomTextInputType.TEXT,
    placeholder: 'Placerholder Text'
  };

export { CustomTextInput, CustomTextInputType }

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    txt: {
        width: '80%',
        height: 40,
        borderRadius: 100,
        backgroundColor: '#ffffff90',
        color: '#000000',
        //elevation: 2,
        borderWidth: 1,
        borderColor: "#eeeeee",
        fontSize: 16,
        textAlign: 'center',
        textAlignVertical: 'center',        
    },
});
