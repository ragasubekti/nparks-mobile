import React from 'react';
import { StyleSheet } from 'react-native';
import { StyleConstant, ShareStyle } from '@assets/MyStyle';
import { Button, DefaultTheme } from 'react-native-paper';


export default function CustomButton(props) {
  return <Button mode="contained" uppercase={false} theme={theme} {...props} style={[styles.elem, props.style]} />;
}

const theme = {
    ...DefaultTheme,
    roundness: 40,
    colors: {
      ...DefaultTheme.colors,
      primary: StyleConstant.primaryColor,
      accent: StyleConstant.secondaryColor,
    },
  };

const styles = StyleSheet.create({
    elem: {
        //color: ShareStyle.primaryText,
        height: 40,
        width: '70%',
        justifyContent: 'center',
        alignSelf: 'center',
        margin: 5
    },
});
