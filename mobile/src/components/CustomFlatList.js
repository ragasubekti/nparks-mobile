import React from 'react';
import { FlatList, View, Text } from 'react-native';
import useCustomFlatListFunc from '@hooks/useCustomFlatListFunc';
import { ShareStyle, HeaderStyleWithBack, StyleConstant } from '@assets/MyStyle';

export default CustomFlatList = ({dataUrl, renderItem, result, numColumns=1, seperator=true}) => {
    
    const { data, setData, refreshing, handleLoadMore, handleRefresh, renderSeparator, renderFooter } = useCustomFlatListFunc(dataUrl, null, result, seperator);
    //todo : change height 400 to full height
    return (
        <FlatList
            ListEmptyComponent={
                <View style={{flex: 1, height: 400, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: StyleConstant.mutedTextColor}}>There is nothing to show at the moment</Text>
                </View>
            }
            data={data}
            refreshing={refreshing}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            numColumns={numColumns}
            onRefresh={handleRefresh}
            onEndReached={handleLoadMore}          
            onEndReachedThreshold={0.1}
            ItemSeparatorComponent={renderSeparator}
            ListFooterComponent={renderFooter}
        />
    );
};
